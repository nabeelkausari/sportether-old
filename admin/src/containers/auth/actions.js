import axios from 'axios';

import { HOST } from "../../constants";
import {AUTH_ADMIN, AUTH_ADMIN_ERROR, AUTH_ADMIN_SUCCESS, LOGOUT_ADMIN} from "./types"

export const signinAdmin = ({ email, password, redirect }) => dispatch => {
  dispatch({ type: AUTH_ADMIN })
  axios.post(`${HOST}/api/admin/signIn`, { email, password })
    .then(res => doLogin(dispatch, res, redirect))
    .catch(err => dispatch(authError(err.response && err.response.data)))
}

const doLogin = (dispatch, res, redirect) => {
  dispatch({ type: AUTH_ADMIN_SUCCESS, payload: res.data.user })
  localStorage.setItem('token', res.data.jwt)
  redirect(res.data.user);
}

export const logoutAdmin = ({ redirect }) => {
  localStorage.removeItem('token');
  redirect()
  return { type: LOGOUT_ADMIN }
}

export const isAuthenticated = ({ redirect }) => dispatch => {
  dispatch({ type: AUTH_ADMIN })
  let jwt = localStorage.getItem('token');
  axios.post(`${HOST}/api/getMe`, { jwt })
    .then(res => {
      if (!res.data.user) {
        dispatch(logoutAdmin({ redirect }))
      } else {
        dispatch({ type: AUTH_ADMIN_SUCCESS, payload: res.data.user })
      }
    })
    .catch(() => dispatch(authError(redirect)))
}

const authError = (redirect) => {
  redirect()
  return { type: AUTH_ADMIN_ERROR, payload: null };
}
