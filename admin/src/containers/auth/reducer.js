import {AUTH_ADMIN, AUTH_ADMIN_ERROR, AUTH_ADMIN_SUCCESS, LOGOUT_ADMIN} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case AUTH_ADMIN:
      return { ...state, authLoading: true, error: null };
    case AUTH_ADMIN_SUCCESS:
      return { ...state, authLoading: false, authenticated: true, user: payload };
    case AUTH_ADMIN_ERROR:
      return { ...state, authLoading: false, authenticated: false, error: payload };

    case LOGOUT_ADMIN:
      return { ...state, authenticated: false };
    default:
      return state;
  }
}
