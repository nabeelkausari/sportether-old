import { connect } from 'react-redux';

import {logoutAdmin} from './actions';

export default connect(null, { logoutAdmin })
