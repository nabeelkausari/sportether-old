import { connect } from 'react-redux';
import { logoutAdmin, signinAdmin, isAuthenticated } from './actions';

function mapStateToProps(state) {
  return { ...state.auth }
}

export default connect(mapStateToProps, { logoutAdmin, signinAdmin, isAuthenticated })
