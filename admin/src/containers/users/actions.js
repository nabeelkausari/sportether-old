import axios from 'axios';

import { HOST, getAuthHeaders } from "../../constants";
import {
  GET_USERS, GET_USERS_ERROR, GET_USERS_SUCCESS,
  GET_CLAIMS, GET_CLAIMS_ERROR, GET_CLAIMS_SUCCESS,
  CONFIRM_BONUS_CLAIM, CONFIRM_BONUS_CLAIM_ERROR, CONFIRM_BONUS_CLAIM_SUCCESS
} from "./types";


export const getUsers = () => dispatch => {
  dispatch({ type: GET_USERS })
  axios.get(`${HOST}/api/admin/getUsers`, getAuthHeaders())
    .then(res => dispatch({ type: GET_USERS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_USERS_ERROR, payload: err}))
}

export const getClaims = () => dispatch => {
  dispatch({ type: GET_CLAIMS });
  axios.get(`${HOST}/api/claims`, getAuthHeaders())
    .then(res => dispatch({ type: GET_CLAIMS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_CLAIMS_ERROR, payload: err }))
}

export const confirmBonus = ({ userId }) => dispatch => {
  dispatch({ type: CONFIRM_BONUS_CLAIM });
  axios.post(`${HOST}/api/confirmBonus`, { userId }, getAuthHeaders())
    .then(res => {
      dispatch({ type: CONFIRM_BONUS_CLAIM_SUCCESS, payload: res.data })
      dispatch(getClaims())
    })
    .catch(err => dispatch({ type: CONFIRM_BONUS_CLAIM_ERROR, payload: err }))
}
