import { connect } from 'react-redux';
import { getUsers } from './actions';

function mapStateToProps(state) {
  return { ...state.users }
}

export default connect(mapStateToProps, { getUsers })
