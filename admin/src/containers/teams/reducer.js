import {GET_TEAMS, GET_TEAMS_ERROR, GET_TEAMS_SUCCESS, GET_TEAM, GET_TEAM_ERROR, GET_TEAM_SUCCESS } from "./types";

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_TEAMS:
      return { ...state, getTeamsLoading: true, teamsErr: null };
    case GET_TEAMS_SUCCESS:
      return { ...state, getTeamsLoading: false, teams: payload };
    case GET_TEAMS_ERROR:
      return { ...state, getTeamsLoading: false, teamsErr: payload.data };

    case GET_TEAM:
      return { ...state, getTeamLoading: true, teamErr: null };
    case GET_TEAM_SUCCESS:
      return { ...state, getTeamLoading: false, team: payload };
    case GET_TEAM_ERROR:
      return { ...state, getTeamLoading: false, teamErr: payload.data };

    default:
      return state;
  }
}
