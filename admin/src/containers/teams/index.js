import { connect } from 'react-redux';
import { getTeams } from './actions';

function mapStateToProps(state) {
  return { ...state.teams }
}

export default connect(mapStateToProps, { getTeams })
