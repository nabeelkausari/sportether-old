import axios from 'axios';

import { HOST, getAuthHeaders } from "../../constants";
import {GET_TEAMS, GET_TEAMS_ERROR, GET_TEAMS_SUCCESS, GET_TEAM, GET_TEAM_ERROR, GET_TEAM_SUCCESS } from "./types";

export const getTeams = sport => dispatch => {
  dispatch({ type: GET_TEAMS })
  axios.get(`${HOST}/api/teams?sport=${sport}`, getAuthHeaders())
    .then(res => dispatch({ type: GET_TEAMS_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_TEAMS_ERROR, payload: err}))
}

export const getTeam = teamId => dispatch => {
  dispatch({ type: GET_TEAM })
  axios.get(`${HOST}/api/team/${teamId}`, getAuthHeaders())
    .then(res => dispatch({ type: GET_TEAM_SUCCESS, payload: res.data}))
    .catch(err => dispatch({ type: GET_TEAM_ERROR, payload: err}))
}
