import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {  getTeam } from './actions';

const validate = formProps => {
  const errors = {};

  if (!formProps.symbol) errors.symbol = 'Please enter a symbol'
  if (!formProps.name) errors.name = 'Please enter a name'
  if (!formProps.sport) errors.sport = 'Please select a sport'
  return errors;
}

function mapStateToProps(state) {
  return { initialValues: state.teams.team }
}

export default Comp => {
  Comp = reduxForm({
    form: 'teamForm',
    validate
  })(Comp);

  return connect(mapStateToProps, { getTeam })(Comp)
}
