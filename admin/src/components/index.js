import Dashboard from './Dashboard';
import Users from './Users';
import Claims from './Claims';
import Teams from './Teams';
import TeamEdit from './Teams/edit';

export {
  Dashboard,
  Users,
  Claims,
  Teams,
  TeamEdit
};

