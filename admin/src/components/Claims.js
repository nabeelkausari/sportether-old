import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import moment from 'moment';

import Container from '../containers/users/claims';

class Claims extends Component {

  componentWillMount() {
    this.props.getClaims();
  }

  render() {
    const { claims } = this.props;
    if (!claims) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                User Claims
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>Date</th>
                    <th>Email</th>
                    <th>Facebook</th>
                    <th>Twitter</th>
                    <th>Telegram</th>
                    <th>Confirm</th>
                  </tr>
                  </thead>
                  <tbody>
                  {!claims.length && <tr>
                    <td className="team-result__date">No users had claimed for bonuses</td>
                  </tr>}
                  {claims.length > 0 && claims.map((claim, i) => <ClaimRow key={i} confirmBonus={this.props.confirmBonus} claim={claim}/>)}
                  </tbody>
                </Table>
                {/*<Pagination>*/}
                {/*<PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem active>*/}
                {/*<PaginationLink tag="button">1</PaginationLink>*/}
                {/*</PaginationItem>*/}
                {/*<PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>*/}
                {/*</Pagination>*/}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

class ClaimRow extends Component {
  state = {
    approvalStart: null
  }
  handleApprove = userId => {
    this.setState({ approvalStart: true });
    this.props.confirmBonus({ userId });
  }

  render() {
    const { claim } = this.props
    const { approvalStart } = this.state;
    return (
      <tr>
        <td>{moment(claim.createdAt).format("ll LT")}</td>
        <td>{claim.userId.email}</td>
        <td>{claim.facebook}</td>
        <td>{claim.twitter}</td>
        <td>{claim.telegram}</td>
        <td>
          {!claim.userId.bonusApproved && <button disabled={approvalStart} onClick={() => this.handleApprove(claim.userId._id)} className="btn btn-primary">Approve</button>}
          {claim.userId.bonusApproved && <button disabled>Approved</button>}
        </td>
      </tr>
    )
  }
}

export default Container(Claims);
