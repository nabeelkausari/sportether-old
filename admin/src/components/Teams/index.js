import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import { Link } from 'react-router-dom'
import Container from '../../containers/teams';
import { weiToEth } from "../../utils"

class Users extends Component {

  componentWillMount() {
    this.props.getTeams(this.props.match.params.sport);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.sport !== this.props.match.params.sport) {
      this.props.getTeams(nextProps.match.params.sport);
    }
  }

  render() {
    const { teams } = this.props;
    const { sport } = this.props.match.params;
    if (!teams) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                {sport} Teams
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>Symbol</th>
                    <th>Name</th>
                    <th>Balance</th>
                    <th>Current Price</th>
                    <th>Edit</th>
                    {/*<th>Status</th>*/}
                  </tr>
                  </thead>
                  <tbody>
                  {teams.length === 0 && <tr><td colSpan={4}>No Teams</td></tr>}
                  {teams.length > 0 && teams.map(team => <TeamRow key={team.symbol} team={team}/>)}
                  </tbody>
                </Table>
                {/*<Pagination>*/}
                {/*<PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem active>*/}
                {/*<PaginationLink tag="button">1</PaginationLink>*/}
                {/*</PaginationItem>*/}
                {/*<PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>*/}
                {/*<PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>*/}
                {/*</Pagination>*/}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

const TeamRow = ({ team }) => {
  return (
    <tr>
      <td>{team.symbol}</td>
      <td>SportEther {team.name}</td>
      <td>{team.balance}</td>
      <td>{weiToEth(team.price)} ETH</td>
      <td><Link to={`/teams/${team.sportId.name.toLowerCase()}/${team._id}`} className="btn btn-warning">Edit</Link></td>
      {/*<td>{user.bonusClaimed ? 'Yes': 'No'}</td>*/}
      {/*<td>*/}
        {/*{user.confirmed ? <Badge color="success">Active</Badge> : <Badge color="warning">In Active</Badge>}*/}
      {/*</td>*/}
    </tr>
  )
}

export default Container(Users);
