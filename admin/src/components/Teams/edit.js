import React, { Component } from 'react';
import { FormGroup, Label, Input, Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import { Field } from 'redux-form';

import FormContainer from '../../containers/teams/form';
import { weiToEth } from "../../utils"

class TeamsEdit extends Component {

  componentWillMount() {
    this.props.getTeam(this.props.match.params.teamId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.teamId !== this.props.match.params.teamId) {
      this.props.getTeam(nextProps.match.params.teamId);
    }
  }

  render() {
    const { team } = this.props;
    if (!team) return <div/>;
    let options = ['Cricket','Football']
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                 Team { team && team.symbol }
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="4">
                    <Field label="Team Name" name="name" component={renderField} type="text" placeholder="Enter team name..." />
                  </Col>
                  <Col xs="4">
                    <Field label="Team Symbol" name="symbol" component={renderField} type="text" placeholder="Enter team symbol..." />
                  </Col>
                  <Col md="4">
                    <Field label="Select Sport" name="sport" component={renderSelectField} type="select" options={options} placeholder="Select a sport..." />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <button className="btn btn-success">Submit</button>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

const TeamRow = ({ team }) => {
  return (
    <tr>
      <td>{team.symbol}</td>
      <td>SportEther {team.name}</td>
      <td>{team.balance}</td>
      <td>{weiToEth(team.price)} ETH</td>
      <td><button className="btn btn-warning">Edit</button></td>
      {/*<td>{user.bonusClaimed ? 'Yes': 'No'}</td>*/}
      {/*<td>*/}
      {/*{user.confirmed ? <Badge color="success">Active</Badge> : <Badge color="warning">In Active</Badge>}*/}
      {/*</td>*/}
    </tr>
  )
}

const renderField = ({input, label, placeholder, disabled, type, meta: { touched, error, warning }}) => {
  return (
    <FormGroup>
      <Label htmlFor={input.name}>{label}</Label>
      <Input {...input} disabled={disabled} placeholder={placeholder} type={type} />
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </FormGroup>
  )
}

const renderSelectField = ({input, label, placeholder, disabled, type, options, meta: { touched, error, warning }}) => {
  return (
    <FormGroup>
      <Label htmlFor={input.name}>{label}</Label>
      <Input {...input} disabled={disabled} type={type}>
        <option value="">{placeholder}</option>
        {options.map(option => <option key={option}>{option}</option>)}
      </Input>
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </FormGroup>
  )
}

export default FormContainer(TeamsEdit);
