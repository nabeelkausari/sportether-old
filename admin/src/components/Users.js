import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';

import Container from '../containers/users';

class Users extends Component {

  componentWillMount() {
    this.props.getUsers();
  }

  render() {
    const { users } = this.props;
    if (!users) return <div/>
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Registered Users
              </CardHeader>
              <CardBody>
                <Table responsive>
                  <thead>
                  <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Date registered</th>
                    <th>Bonus Claimed</th>
                    <th>Bonus Approved</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  {users.length === 0 && <tr><td colSpan={4}>No Users</td></tr>}
                  {users.length > 0 && users.map(user => <UserRow key={user.username} user={user}/>)}
                  </tbody>
                </Table>
                {/*<Pagination>*/}
                  {/*<PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>*/}
                  {/*<PaginationItem active>*/}
                    {/*<PaginationLink tag="button">1</PaginationLink>*/}
                  {/*</PaginationItem>*/}
                  {/*<PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>*/}
                  {/*<PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>*/}
                  {/*<PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>*/}
                  {/*<PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>*/}
                {/*</Pagination>*/}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

const UserRow = ({ user }) => {
  return (
    <tr>
      <td>{user.username}</td>
      <td>{user.email}</td>
      <td>{user.createdAt}</td>
      <td>{user.bonusClaimed ? 'Yes': 'No'}</td>
      <td>{user.bonusApproved ? 'Yes': 'No'}</td>
      <td>
        {user.confirmed ? <Badge color="success">Active</Badge> : <Badge color="warning">In Active</Badge>}
      </td>
    </tr>
  )
}

export default Container(Users);
