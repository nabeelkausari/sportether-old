import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './containers/auth/reducer';
import usersReducer from './containers/users/reducer';
import teamsReducer from './containers/teams/reducer';

export default combineReducers({
  form: formReducer,
  auth: authReducer,
  users: usersReducer,
  teams: teamsReducer
});
