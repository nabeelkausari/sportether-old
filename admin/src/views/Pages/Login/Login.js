import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Row } from 'reactstrap';
import { Field } from 'redux-form';

import SigninContainer from '../../../containers/auth/signinAdmin'

class Login extends Component {

  handleFormSubmit = ({ email, password }) => {
    this.props.signinAdmin({
      email,
      password,
      redirect: () => this.props.history.push('/dashboard')
    });
  }

  renderAlert = () => {
    if (this.props.signinError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.signinError}
        </div>
      )
    }
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1 className="text-center">Login</h1>
                    <p className="text-muted text-center">Sign In to your account</p>
                    <form className={'modal-form ' + this.props.className } onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
                      <fieldset className="form-group">
                        <Field label="Email" name="email" component={renderField} type="email" placeholder="Enter your email address..." />
                      </fieldset>
                      <fieldset className="form-group">
                        <Field label="Password" name="password" component={renderField} type="password" placeholder="Enter your password..." />
                      </fieldset>
                      {this.renderAlert()}
                      <Row>
                        <Col className="text-center">
                          <Button action="submit" color="primary" className="px-4">Login</Button>
                        </Col>
                        {/*<Col xs="6" className="text-right">*/}
                          {/*<Button color="link" className="px-0">Forgot password?</Button>*/}
                        {/*</Col>*/}
                      </Row>
                    </form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const renderField = ({input, label, placeholder, disabled, type, meta: { touched, error, warning }}) => (
  <div className="row" style={{ alignItems: "center" }}>
    <div className="col-md-4"><h6 style={{ marginBottom: 0, textAlign: 'right' }}>{label}</h6></div>
    <div className="col-md-8">
      <input {...input} disabled={disabled} placeholder={placeholder} type={type} className="form-control" />
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </div>
  </div>
)

export default SigninContainer(Login);
