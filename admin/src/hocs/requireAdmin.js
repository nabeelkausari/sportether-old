import React, { Component } from 'react';
import { connect } from 'react-redux';

export default ComposedComponent => {
  class Authentication extends Component {

    componentWillMount() {
      if ((this.props.authLoading === false && this.props.authenticated === false) ||
        (this.props.user && this.props.user.isAdmin === false)
      ) {
        this.props.history.push('/#/login');
      }
    }

    componentWillUpdate(nextProps) {
      if((!this.props.authLoading === false && nextProps.authenticated === false) ||
        (!this.props.authLoading === false && nextProps.user && nextProps.user.isAdmin === false)
      ) {
        this.props.history.push('/#/login');
      }
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {...state.auth}
  }

  return connect(mapStateToProps)(Authentication)
}

