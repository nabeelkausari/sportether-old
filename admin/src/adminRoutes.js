import {
  Dashboard,
  Users,
  Claims,
  Teams,
  TeamEdit
} from './components';
import DefaultLayout from './containers/DefaultLayout';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/users', name: 'Users', component: Users },
  { path: '/claims', name: 'Claims', component: Claims },
  { path: '/teams/:sport', exact: true, name: 'Teams', component: Teams },
  { path: '/teams/:sport/:teamId', exact: true, name: 'TeamsEdit', component: TeamEdit },
];

export default routes;
