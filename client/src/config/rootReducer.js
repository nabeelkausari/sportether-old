import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import homeReducer from '../containers/home/reducer';
import authReducer from '../containers/auth/reducer';
import walletReducer from '../containers/wallet/reducer';
import sportsReducer from '../containers/sports/reducer';
import matchesReducer from '../containers/matches/reducer';
import teamsReducer from '../containers/teams/reducer';
import groundsReducer from '../containers/grounds/reducer';
import bonusReducer from "../containers/bonus/reducer"
import referralReducer from "../containers/referral/reducer"

export default combineReducers({
  router: routerReducer,
  form: formReducer,
  home: homeReducer,
  auth: authReducer,
  wallet: walletReducer,
  sports: sportsReducer,
  matches: matchesReducer,
  teams: teamsReducer,
  grounds: groundsReducer,
  bonus: bonusReducer,
  referral: referralReducer
});
