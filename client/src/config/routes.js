import React from 'react';
import { Route } from 'react-router-dom';

import requireAuth from '../containers/hoc/requireAuth';
import requireAdmin from '../containers/hoc/requireAdmin';
import SignUp from '../components/pages/signup';
import SignIn from '../components/pages/signin';
import Home from '../components/home';
import Wallet from '../components/wallet';
import Matches from '../components/matches';
import Teams from '../components/teams';
import Marketplace from '../components/teams/Marketplace';
import Grounds from '../components/grounds';
import GroundDetail from '../components/grounds/Detail';

import GroundsPage from '../components/pages/grounds';
import TeamsPage from '../components/pages/teamToken';
import AboutPage from '../components/pages/aboutPage';
import TermsPage from '../components/pages/terms';
import PrivacyPage from '../components/pages/privacy';
import EmailConfirmed from '../components/pages/email/confirmed';
import EmailError from '../components/pages/email/error';
import HowItWorks from '../components/pages/howItWorks';
import FAQPage from '../components/pages/faqPage';
import SPOPage from '../components/pages/SpoPage';
import Legal from '../components/pages/legal';
import Referral from '../components/pages/referral';
import Bonus from '../components/bonus';
import Claims from '../components/bonus/Claims';
import ReferralTracker from '../components/referral';
import SetReferral from '../components/auth/SetReferral';

export default () => (
  <main>
    <Route exact path="/" component={Home} />
    <Route exact path="/team-tokens" component={TeamsPage} />
    <Route exact path="/virtual-tournament-grounds" component={GroundsPage} />
    <Route exact path="/about" component={AboutPage} />
    <Route exact path="/how-it-works" component={HowItWorks} />
    <Route exact path="/faq" component={FAQPage} />
    <Route exact path="/terms-conditions" component={TermsPage} />
    <Route exact path="/privacy-policy" component={PrivacyPage} />
    <Route exact path="/disclaimer-legal" component={Legal} />
    <Route exact path="/spo-coin" component={SPOPage} />
    <Route exact path="/referral-program" component={Referral} />
    <Route path="/ref" component={SetReferral} />
    <Route path="/ref/:refUsername" component={SetReferral} />

    <Route exact path="/email-confirmed" component={EmailConfirmed} />
    <Route exact path="/email-error" component={EmailError} />
    <Route exact path="/sign-up" component={SignUp} />
    <Route exact path="/sign-in" component={SignIn} />
    <Route exact path="/matches/:sport" component={Matches} />
    <Route exact path="/matches/:sport/:match" component={Grounds} />
    <Route exact path="/matches/:sport/:match/:ground" component={GroundDetail} />
    <Route exact path="/marketplace" component={Marketplace} />
    <Route exact path="/marketplace/:sport" component={Teams} />
    <Route exact path="/account/bonus" component={requireAuth(Bonus)} />
    <Route exact path="/account/wallet" component={requireAuth(Wallet)} />
    <Route exact path="/account/referral-tracker" component={requireAuth(ReferralTracker)} />
    <Route exact path="/account/claims" component={requireAdmin(Claims)} />
  </main>
)
