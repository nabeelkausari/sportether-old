export const devUrl = 'http://localhost:5000';
export const isProd = process.env.NODE_ENV === "production";
export const isDev = process.env.NODE_ENV === "development";

export const HOST = isProd ? '' :  devUrl;
export const getAuthHeaders = () => {
  let jwt = localStorage.getItem('token');
  return { headers: { authorization: jwt }}
}

export const isMobile = window.innerWidth < 992;
