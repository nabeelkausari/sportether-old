export const gwei = 1000000000;
export const wei = 1000000000000000000;

export const ethToGwei = etherValue => etherValue * gwei;
export const gweiToEth = gweiValue => gweiValue / gwei;
export const ethToWei = etherValue => etherValue * wei;
export const weiToEth = weiValue => weiValue / wei;

export const setReferralId = () => {
  if (getReferralId() !== null) return;

  let key = encodeURI('ref');
  let value;

  let queries = document.location.search.substr(1).split('&');

  let i=queries.length; let x; while(i--) {
    x = queries[i].split('=');

    if (x[0]===key) {
      value = encodeURI(x[1]);
      break;
    }
  }
  if (value) {
    window.localStorage.setItem('referralId', value);
  }
}

export const setProvidedReferralId = refUsername => {
  if (getReferralId() !== null) return;
  if (refUsername) {
    window.localStorage.setItem('referralId', refUsername);
  }
}

export const getReferralId = () => {
  return window.localStorage.getItem('referralId');
}
