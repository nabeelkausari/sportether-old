import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, CardBody, CardTitle } from 'reactstrap';
import {HOST} from "../../config/constants"

export default ({ completedMatches, completedMatchesLoading, match: { url } }) => {
  if (completedMatchesLoading) return <h3>Loading....</h3>
  if (!completedMatches) return <div></div>;
  return (
    <Row>{completedMatches.map(match => (
      <Col key={match._id} md={3}>
        <Card className="text-center">
          <div className="match-logos">
            <div className="match-logo"><img src={`${HOST}/api/logos/${match.teamA.token.logo}`}/></div>
            <div className="match-logo"><img src={`${HOST}/api/logos/${match.teamB.token.logo}`}/></div>
          </div>
          <CardBody>
            <CardTitle>{match.teamA.token.symbol} <span>vs</span> {match.teamB.token.symbol}</CardTitle>
            {/*<CardSubtitle>Card subtitle</CardSubtitle>*/}
            {/*<CardText>{match.description}</CardText>*/}
            <Link className="btn btn-secondary" to={`${url}/${match._id}`}>
              { !match.complete ? 'Participate' : 'Details' }
            </Link>
          </CardBody>
        </Card>
      </Col>
    ))}</Row>
  )
}

