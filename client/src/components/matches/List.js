import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Card, CardBody, CardTitle } from 'reactstrap';
import {HOST} from "../../config/constants"

export default ({ activeMatches, activeMatchesLoading, match: { url } }) => {
  if (activeMatchesLoading) return <h3>Loading....</h3>
  if (!activeMatches) return <div></div>;
  return (
    <Row>{activeMatches.map(match => (
      <Col key={match._id} md={3}>
        <Card className="text-center">
          <div className="match-logos">
            <div className="match-logo"><img src={`${HOST}/api/logos/${match.teamA.token.logo}`}/></div>
            <div className="match-logo"><img src={`${HOST}/api/logos/${match.teamB.token.logo}`}/></div>
          </div>
          <CardBody>
            <CardTitle>{match.teamA.token.symbol} <span>vs</span> {match.teamB.token.symbol}</CardTitle>
            {/*<CardSubtitle>Card subtitle</CardSubtitle>*/}
            {/*<CardText>{match.description}</CardText>*/}
            {!match.complete && <Link className="btn btn-secondary" to={`${url}/${match._id}`}>Participate</Link>}
          </CardBody>
        </Card>
      </Col>
    ))}</Row>
  )
}

