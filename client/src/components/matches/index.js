import React, { Component } from 'react';

import MatchesContainer from '../../containers/matches';
import Layout from '../layouts';
import MatchesList from './List';
import Completed from './Completed';

class Matches extends Component {

  componentWillMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.sport !== prevProps.match.params.sport) {
      this.loadData();
    }
  }

  loadData = () => {
    let { sport } = this.props.match.params;
    this.props.getActiveMatches(sport);
    this.props.getCompletedMatches(sport);
  }

  render() {
    const { activeMatches, completedMatches } = this.props;
    return (
      <Layout {...this.props}>
        <h1>Matches page {this.props.match.params.sport}</h1>
        {activeMatches && activeMatches.length > 0 && <div>
          <h3>Active Matches</h3>
          <MatchesList {...this.props} />
        </div>}
        {completedMatches && completedMatches.length > 0 && <div>
          <h3>Completed Matches</h3>
          <Completed {...this.props} />
        </div>}
      </Layout>
    )
  }
}

export default MatchesContainer(Matches)
