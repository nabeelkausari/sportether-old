import React from 'react';
import { Link } from 'react-router-dom';

import WithAuthState from '../../containers/hoc/withAuthState';
import logo from '../../images/logo.png';
import logo2x from '../../images/logo2x.png';

export default WithAuthState(props => {
  const isLoggedIn = props.authLoading === false && props.authenticated === true;
  return (
    <footer id="footer" className="footer">
      {/* Footer Widgets */}
      <div className="footer-widgets">
        <div className="footer-widgets__inner">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 col-md-3">
                <div className="footer-col-inner">
                  {/* Footer Logo */}
                  <div className="footer-logo">
                    <Link to="/"><img src={logo} alt="SportEther" srcSet={`${logo2x} 2x`} className="footer-logo__img" /></Link>
                  </div>
                  {/* Footer Logo / End */}
                </div>
              </div>
              {/*<div className="col-sm-4 col-md-3">*/}
              {/*<div className="footer-col-inner">*/}
              {/*<div className="widget widget--footer widget-contact-info">*/}
              {/*<h4 className="widget__title">Activity Online</h4>*/}
              {/*<div className="widget__content">*/}
              {/*/!*<div className="widget-contact-info__desc">*!/*/}
              {/*/!*<p>Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>*!/*/}
              {/*/!*</div>*!/*/}
              {/*<div className="widget-contact-info__body info-block">*/}
              {/*<div className="info-block__item">*/}
              {/*<h4 className="info-block__heading">Players Online</h4>*/}
              {/*<h5 className="info-block__highlight">4 players</h5>*/}
              {/*</div>*/}
              {/*<div className="info-block__item">*/}
              {/*<h4 className="info-block__heading">Live Matches</h4>*/}
              {/*<h5 className="info-block__highlight">3 matches</h5>*/}
              {/*</div>*/}
              {/*<div className="info-block__item">*/}
              {/*<h4 className="info-block__heading">Active Grounds</h4>*/}
              {/*<h5 className="info-block__highlight">170 grounds</h5>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</div>*/}
              <div className="col-sm-4 col-md-3">
                <div className="footer-col-inner">
                  <div className="widget widget--footer widget-popular-posts">
                    <h4 className="widget__title">Other links</h4>

                    <div className="widget__content">
                      <ul className="footer-nav">
                        {!isLoggedIn && <li className="footer-nav__list-item"><Link to="/sign-in">Sign in</Link></li>}
                        {!isLoggedIn && <li className="footer-nav__list-item"><Link to="/sign-up">Sign up</Link></li>}
                        {isLoggedIn && <li className="footer-nav__list-item"><Link to="/account/wallet">My Account</Link></li>}
                        <li className="footer-nav__list-item"><Link to="/how-it-works">How it works?</Link></li>
                        <li className="footer-nav__list-item"><Link to="/faq">FAQ</Link></li>
                      </ul>
                    </div>
                  </div>
                </div>

              </div>
              <div className="col-sm-4 col-md-3">
                <div className="footer-col-inner">
                  <div className="widget widget--footer widget-popular-posts">
                    <h4 className="widget__title">Further Information</h4>

                    <div className="widget__content">
                      <ul className="footer-nav">
                        <li className="footer-nav__list-item"><Link to="/referral-program">Referral Program</Link></li>
                        <li className="footer-nav__list-item"><Link to="/terms-conditions">Terms and Conditions</Link></li>
                        <li className="footer-nav__list-item"><Link to="/privacy-policy">Privacy Policy</Link></li>
                        <li className="footer-nav__list-item"><Link to="/disclaimer-legal">Copyright and Legal</Link></li>
                        {/*<li className="footer-nav__list-item"><Link to="/contact-us">Contact Us</Link></li>*/}
                      </ul>
                    </div>
                  </div>
                </div>

              </div>
              <div className="col-sm-4 col-md-3">
                <div className="footer-col-inner">
                  <div className="widget widget--footer widget-contact-info">
                    <h4 className="widget__title">Contact Info</h4>
                    <div className="widget__content">
                      <div className="widget-contact-info__desc">
                        <h5 className="widget-contact-info__company">SportEther.com</h5>
                        <p>Sportiex Ltd, Kemp House, 160 City Road, London, EC1V2NX, United Kingdom</p>
                        <p><a href="mailto:contact@sportether.com">contact@sportether.com</a></p>
                      </div>
                      <div className="info-block__item info-block__item--nopadding">
                        <ul className="social-links">
                          <li className="social-links__item">
                            <a href="https://www.facebook.com/sportether" className="social-links__link"><i className="fa fa-facebook" /> Facebook</a>
                          </li>
                          <li className="social-links__item">
                            <a href="https://twitter.com/sportether" className="social-links__link"><i className="fa fa-twitter" /> Twitter</a>
                          </li>
                          <li className="social-links__item">
                            <a href="https://t.me/sportether" className="social-links__link"><i className="fa fa-telegram" /> Telegram</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      {/* Footer Widgets / End */}
      {/* Footer Secondary */}
      <div className="footer-secondary footer-secondary--has-decor">
        <div className="container">
          <div className="footer-secondary__inner">
            <div className="row">
              <div className="col-md-12">
                <ul className="footer-nav">
                  <li className="footer-nav__item"><Link to="/">Home</Link></li>
                  <li className="footer-nav__item"><Link to="/about">About</Link></li>
                  <li className="footer-nav__item"><Link to="/team-tokens">Team Tokens</Link></li>
                  <li className="footer-nav__item"><Link to="/virtual-tournament-grounds">Virtual Tournament Grounds (VTG)</Link></li>
                  <li className="footer-nav__item"><Link to="/spo-coin">SPO Coin</Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Footer Secondary / End */}
    </footer>
  )
})
