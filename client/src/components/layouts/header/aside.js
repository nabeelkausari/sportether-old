import React from 'react';

export default () => (
  <aside className="pushy-panel">
    <div className="pushy-panel__inner">
      <header className="pushy-panel__header">
        <div className="pushy-panel__logo">
          <a href="index.html"><img src="./images/logo.png" srcSet="./images/logo@2x.png 2x" alt="Alchemists" /></a>
        </div>
      </header>
      <div className="pushy-panel__content">
        {/* Widget: Posts */}
        <aside className="widget widget--side-panel">
          <div className="widget__content">
            <ul className="posts posts--simple-list posts--simple-list--lg">
              <li className="posts__item posts__item--category-1">
                <div className="posts__inner">
                  <div className="posts__cat">
                    <span className="label posts__cat-label">The Team</span>
                  </div>
                  <h6 className="posts__title"><a href="#">The new eco friendly stadium won a Leafy Award in 2016</a></h6>
                  <time dateTime="2017-08-23" className="posts__date">August 23rd, 2017</time>
                  <div className="posts__excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud en derum sum laborem.
                  </div>
                </div>
                <footer className="posts__footer card__footer">
                  <div className="post-author">
                    <figure className="post-author__avatar">
                      <img src="./images/samples/avatar-1.jpg" alt="Post Author Avatar" />
                    </figure>
                    <div className="post-author__info">
                      <h4 className="post-author__name">James Spiegel</h4>
                    </div>
                  </div>
                  <ul className="post__meta meta">
                    <li className="meta__item meta__item--likes"><a href="#"><i className="meta-like meta-like--active icon-heart" /> 530</a></li>
                    <li className="meta__item meta__item--comments"><a href="#">18</a></li>
                  </ul>
                </footer>
              </li>
              <li className="posts__item posts__item--category-2">
                <div className="posts__inner">
                  <div className="posts__cat">
                    <span className="label posts__cat-label">Injuries</span>
                  </div>
                  <h6 className="posts__title"><a href="#">Mark Johnson has a Tibia Fracture and is gonna be out</a></h6>
                  <time dateTime="2017-08-23" className="posts__date">August 23rd, 2017</time>
                  <div className="posts__excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipisi nel elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini veniam, quis nostrud en derum sum laborem.
                  </div>
                </div>
                <footer className="posts__footer card__footer">
                  <div className="post-author">
                    <figure className="post-author__avatar">
                      <img src="./images/samples/avatar-2.jpg" alt="Post Author Avatar" />
                    </figure>
                    <div className="post-author__info">
                      <h4 className="post-author__name">Jessica Hoops</h4>
                    </div>
                  </div>
                  <ul className="post__meta meta">
                    <li className="meta__item meta__item--likes"><a href="#"><i className="meta-like icon-heart" /> 530</a></li>
                    <li className="meta__item meta__item--comments"><a href="#">18</a></li>
                  </ul>
                </footer>
              </li>
            </ul>
          </div>
        </aside>
        {/* Widget: Posts / End */}
        {/* Widget: Tag Cloud */}
        <aside className="widget widget--side-panel widget-tagcloud">
          <div className="widget__title">
            <h4>Tag Cloud</h4>
          </div>
          <div className="widget__content">
            <div className="tagcloud">
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">PLAYOFFS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">ALCHEMISTS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">INJURIES</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">TEAM</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">INCORPORATIONS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">UNIFORMS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">CHAMPIONS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">PROFESSIONAL</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">COACH</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">STADIUM</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">NEWS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">PLAYERS</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">WOMEN DIVISION</a>
              <a href="#" className="btn btn-primary btn-xs btn-outline btn-sm">AWARDS</a>
            </div>
          </div>
        </aside>
        {/* Widget: Tag Cloud / End */}
        {/* Widget: Banner */}
        <aside className="widget widget--side-panel widget-banner">
          <div className="widget__content">
            <figure className="widget-banner__img">
              <a href="#"><img src="./images/samples/banner.jpg" alt="Banner" /></a>
            </figure>
          </div>
        </aside>
        {/* Widget: Banner / End */}
      </div>
      <a href="#" className="pushy-panel__back-btn" />
    </div>
  </aside>
)
