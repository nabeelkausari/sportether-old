import React, { Component } from 'react';
import { Sticky } from 'react-sticky';
import { Link as ScrollLink } from 'react-scroll'
import { Link } from 'react-router-dom';
import { isMobile } from "../../../config/constants"
import AnimateHeight from 'react-animate-height';

import WithSports from '../../../containers/hoc/withSports';
import LinkItem from './LinkItem';
import logo from '../../../images/logo.png';
import logo2x from '../../../images/logo2x.png';

const linkTo = (to, name) => {
  return (
    <ScrollLink
      style={{ cursor: 'pointer' }}
      activeClass="active"
      to={to} spy={true} smooth={true}
      offset={-62} duration={500}
    >
      {name}
    </ScrollLink>
  )
}

const renderHomeLinks = location => {
  if (location && location.pathname.split('/')[1] === "") {
    return (
      <ul className="main-nav__sub">
        <li>{linkTo('about', 'About')}</li>
        <li>{linkTo('opportunity', 'Opportunity')}</li>
        <li>{linkTo('benefits', 'Benefits')}</li>
        <li>{linkTo('play', 'How it works')}</li>
        <li>{linkTo('spo', 'Spo Coins')}</li>
        <li>{linkTo('sale', 'Sale Process')}</li>
        <li>{linkTo('faq', 'FAQs')}</li>
      </ul>
    )
  }
}

const getLinkClass = (path, location) => {
  if (location) {
    return location.pathname.split('/')[1] === path ? 'active' : '';
  }
}

class AccountLinks extends Component {
  state = {
    openToggle: false,
    menuHeight: 0
  }

  handleToggle = () => {
    this.setState({
      openToggle: !this.state.openToggle,
      menuHeight: !this.state.openToggle ? 'auto' : 0
    })
  }

  renderLinks = ({ user, logoutUser, redirect }) => {
    return (
      <ul className="main-nav__sub">
        {!user.bonusClaimed && !user.isAdmin && <li>
          <Link to="/account/bonus">
            Claim Your Bonus
          </Link>
        </li>}
        {user.isAdmin && <li>
          <Link to="/account/claims">
            Bonus Claims
          </Link>
        </li>}
        <li>
          <Link to="/account/wallet">
            Wallet
          </Link>
        </li>
        <li>
          <Link to="/account/referral-tracker">
            Referral Tracker
          </Link>
        </li>
        <li>
          <a className="btn" onClick={() => logoutUser({ redirect })}>Logout</a>
        </li>
      </ul>
    )
  }

  render() {
    let { openToggle, menuHeight } = this.state;
    let { user, logoutUser, redirect, location } = this.props;
    if (isMobile) {
      return (
        <li className={getLinkClass('account', location)}>
          <span onClick={this.handleToggle} className={"main-nav__toggle " + (openToggle ? "main-nav__toggle--rotate" : "")}/>
          <a onClick={this.handleToggle}>{user.username}</a>
          <AnimateHeight
            duration={ 500 }
            height={ menuHeight }>
            {this.renderLinks({ user, logoutUser, redirect })}
          </AnimateHeight>
        </li>
      )
    } else {
      return (
        <li className={getLinkClass('account', location)}>
          <a>{user.username}</a>
          {this.renderLinks({ user, logoutUser, redirect })}
        </li>
      )
    }

  }
}

export default WithSports(props => {
  const { className, location, isMobile, handleNavToggle, sports } = props;
  const isLoggedIn = props.authLoading === false && props.authenticated === true;
  const marketplaceList = sports ? sports.map(sport => ({ title: sport.name, url: `/marketplace/${sport.slug}` })) : []
  return (
    <Sticky>
      {
        ({ style }) => {
          return (
            <header className={ className + ' header'} style={ !isMobile ? { ...style, zIndex: 4 } : {}}>
              <div className="header__primary">
                <div className="container">
                  <div className="header__primary-inner">
                    <div className="header-logo">
                      <Link to="/"><img src={logo} alt="SportEther" srcSet={`${logo2x} 2x`} className="header-logo__img" /></Link>
                    </div>
                    <nav className="main-nav clearfix">
                      <ul className="main-nav__list">
                        <div className="header-mobile__logo side-logo"><span onClick={handleNavToggle} className="main-nav__back" />
                          <Link to="/"><img src={logo} alt="SportEther" srcSet={`${logo2x} 2x`} className="header-logo__img" /></Link>
                        </div>
                        <li className={getLinkClass('', location)}><Link to="/">Home</Link>
                          {renderHomeLinks(location)}
                        </li>
                        <LinkItem rootLink location={location} list={marketplaceList} title="Marketplace" activePath="marketplace" />
                        {/*<LinkItem location={location} list={matchesList} title="Matches" activePath="matches" />*/}
                        <li className={getLinkClass('referral-program', location)}>
                          <Link to="/referral-program">Referral Program</Link>
                        </li>
                        <li className={getLinkClass('virtual-tournament-grounds', location)}>
                          <Link to="/virtual-tournament-grounds">Virtual Tournament Grounds</Link>
                        </li>
                        { isLoggedIn
                          ? <AccountLinks {...props} />
                          : <li className={getLinkClass('sign-in', location)}>
                            <Link to="/sign-in">Login</Link>
                          </li>
                        }
                      </ul>

                      {/* Pushy Panel Toggle */}
                      {/*<a className="pushy-panel__toggle">*/}
                      {/*<span className="pushy-panel__line" />*/}
                      {/*</a>*/}
                      {/* Pushy Panel Toggle / Eng */}
                    </nav>
                    {/* Main Navigation / End */}
                  </div>
                </div>
              </div>
              {/* Header Primary / End */}
            </header>
          )
        }
      }
    </Sticky>
  )
})
