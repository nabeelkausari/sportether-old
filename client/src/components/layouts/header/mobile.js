import React from 'react';
import { Sticky } from 'react-sticky';
import { Link } from 'react-router-dom';

import logo from '../../../images/logo.png';
import logo2x from '../../../images/logo2x.png';

export default ({ className, handleNavToggle }) => (
  <div className={ className + ' header-mobile clearfix'} id="header-mobile">
    <div className="header-mobile__inner">
      <a onClick={handleNavToggle}  className="burger-menu-icon">
        <span className="burger-menu-icon__line" />
      </a>
      <div  className="header-mobile__logo">
        <Link to="/"><img src={logo} alt="SportEther" srcSet={`${logo2x} 2x`} className="header-mobile__logo-img" /></Link>
      </div>
    </div>
  </div>
)
