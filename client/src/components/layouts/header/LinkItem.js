import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AnimateHeight from 'react-animate-height';

import { isMobile } from "../../../config/constants"

class LinkItem extends Component {
  state = {
    openToggle: false,
    menuHeight: 0
  }

  handleToggle = () => {
    this.setState({
      openToggle: !this.state.openToggle,
      menuHeight: !this.state.openToggle ? 'auto' : 0
    });
  }

  getLinkClass = (path, location) => {
    if (location) {
      return location.pathname.split('/')[1] === path ? 'active' : '';
    }
  }

  render() {
    let { openToggle, menuHeight } = this.state;
    let { list, title, activePath, location, rootLink } = this.props;
    if (isMobile) {
      return (
        <li className={this.getLinkClass(activePath, location)}>
          <a onClick={this.handleToggle}>{title}
            <span
                  className={"main-nav__toggle " + (openToggle ? "main-nav__toggle--rotate" : "")}
            />
          </a>

          <AnimateHeight
            duration={ 500 }
            height={ menuHeight }>
            <ul className="main-nav__sub">
              <li key={title}>
                <Link to={`/${activePath}`}>
                  {title} Home
                </Link>
              </li>
              {list.map(item => (
                <li key={item.title}>
                  <Link to={item.url}>
                    {item.title}
                  </Link>
                </li>
              ))}
            </ul>
          </AnimateHeight>
        </li>

      )
    } else {
      return (
        <li className={this.getLinkClass(activePath, location)}>
          {
            rootLink ? <Link to={`/${activePath}`}>{title}</Link> : <a>{title}</a>
          }
          <ul className="main-nav__sub">
            {list.map(item => (
              <li key={item.title}>
                <Link to={item.url}>
                  {item.title}
                </Link>
              </li>
            ))}
          </ul>
        </li>
      )
    }
  }
}

export default LinkItem
