import React from 'react';

export default () => <div className="ripple-holder">
  <div className="lds-ripple"><div/><div/></div>
</div>

