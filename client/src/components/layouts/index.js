import React, { Component } from 'react';
import { StickyContainer } from 'react-sticky';
import {NotificationContainer} from 'react-notifications';

import LayoutContainer from '../../containers/layouts';
import MobileHeader from './header/mobile';
import DesktopHeader from './header/desktop';
import Footer from './Footer';
import { setReferralId } from "../../config/utils";
import { isMobile } from "../../config/constants"

class Layout extends Component {
  state = {
    showNav: false,
    isMobile,
    showMobileNav: false
  }

  componentWillMount() {
    this.props.isAuthenticated({ redirect: this.props.redirect })
    setReferralId();
  }

  componentDidMount() {
    this.props.requireSync()
  }

  toggleMobileNav = () => {
    this.setState({ showMobileNav: !this.state.showMobileNav })
  }

  getWrapperClassName = () => {
    return this.state.showMobileNav === true
      ? 'site-wrapper clearfix site-wrapper--has-overlay'
      : 'site-wrapper clearfix';
  }


  render() {
    return (
      <StickyContainer className="template-basketball" >
        <div className={this.getWrapperClassName()}>
          <div className="site-overlay" onClick={this.toggleMobileNav}/>
          <MobileHeader {...this.props} handleNavToggle={this.toggleMobileNav} />
          <DesktopHeader {...this.props} handleNavToggle={this.toggleMobileNav} isMobile={this.state.isMobile} />
          <NotificationContainer/>
          {this.props.children}
          <Footer />
        </div>
      </StickyContainer>
    )
  }
}

export default LayoutContainer(Layout)
