import React, { Component } from 'react';
import { StickyContainer } from 'react-sticky';
import { Link, DirectLink, Element , Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import {NotificationContainer} from 'react-notifications';

import LayoutContainer from '../../containers/layouts';
import MobileHeader from './header/mobile';
import DesktopHeader from './header/desktop';
import Footer from './Footer';
import {setReferralId} from "../../config/utils"

class Layout extends Component {
  state = {
    showNav: false,
    isMobile: window.innerWidth < 992,
    showMobileNav: false
  }
  componentWillMount() {
    this.props.isAuthenticated({ redirect: this.props.redirect })
    setReferralId();
  }

  canShowNav = () => {
    let showNav = window.scrollY > 150
    this.setState({ showNav });
  }

  handleWindowSizeChange = () => {
    this.setState({ isMobile: window.innerWidth < 992 });
  };

  toggleMobileNav = () => {
    this.setState({ showMobileNav: !this.state.showMobileNav })
  }

  componentDidMount() {
    window.addEventListener('scroll',this.canShowNav);
    window.addEventListener('resize', this.handleWindowSizeChange);
    this.props.requireSync()
  }

  componentWillUnmount(){
    window.removeEventListener('scroll',this.canShowNav);
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  getWrapperClassName = () => {
    return this.state.showMobileNav === true
      ? 'site-wrapper clearfix site-wrapper--has-overlay'
      : 'site-wrapper clearfix';
  }

  render() {
    const componentClasses = ['header-component'];
    const { isMobile, showNav } = this.state;
    if (showNav) { componentClasses.push('show'); }

    return (
      <StickyContainer className="template-basketball" >
        <div className={this.getWrapperClassName()} >
          <div className="site-overlay" onClick={this.toggleMobileNav}/>
          {isMobile && <MobileHeader
            className={componentClasses.join(' ') + ' '}
            {...this.props}
            handleNavToggle={this.toggleMobileNav}
          />}
          <DesktopHeader
            className={componentClasses.join(' ') + ' '}
            { ...this.props }
            isMobile={isMobile}
            handleNavToggle={this.toggleMobileNav}
          />
          <NotificationContainer/>
          {this.props.children}
          <Footer />
        </div>
      </StickyContainer>
    )
  }
}

export default LayoutContainer(Layout)
