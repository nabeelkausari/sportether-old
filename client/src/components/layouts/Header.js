import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
import { NavLink, Link } from 'react-router-dom';

import WithSports from '../../containers/hoc/withSports';

class HeaderComponent extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleLogout = () => {
    this.props.logoutUser({ redirect: this.props.redirect });
  }

  renderAuthLinks = () => {
    return !this.props.authenticated && [
        <NavItem key={1}><Link className="nav-link" to="/sign-in">Sign In</Link></NavItem>,
        <NavItem key={2}><Link className="nav-link" to="/sign-up">Sign Up</Link></NavItem>
      ]
  }

  render() {
    let { authenticated, user, sports } = this.props;
    return (
      <div>
        <Navbar color="faded" light expand="md">
          <NavbarBrand href="/">SportEther</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink exact className="nav-link" to="/">Home</NavLink>
              </NavItem>
              {dropDown(sports, 'Teams')}
              {dropDown(sports, 'Matches')}
              {this.renderAuthLinks()}
              {authenticated && <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  {user && user.username }
                </DropdownToggle>
                <DropdownMenu >
                  <DropdownItem>
                    Profile
                  </DropdownItem>
                  <DropdownItem>
                    <Link className="nav-link" to="/wallet">Wallet</Link>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem onClick={this.handleLogout}>
                    Logout
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const dropDown = (sports, navHeader) => {
  if (!sports) return;
  return (
    <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav caret>
        { navHeader }
      </DropdownToggle>
      <DropdownMenu >
        {sports.map(sport => (
          <DropdownItem key={sport._id}>
            <NavLink
              className="nav-link"
              to={`/${navHeader.toLowerCase()}/${sport.slug}`}
            >{sport.name}</NavLink>
          </DropdownItem>
        ))}
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default WithSports(HeaderComponent)
