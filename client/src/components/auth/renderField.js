import React from 'react';

export default ({input, label, placeholder, disabled, type, meta: { touched, error, warning }}) => (
  <div className="row" style={{ alignItems: "center" }}>
    <div className="col-md-4"><h6 style={{ marginBottom: 0, textAlign: 'right' }}>{label}</h6></div>
    <div className="col-md-8">
      <input {...input} disabled={disabled} placeholder={placeholder} type={type} className="form-control" />
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </div>
  </div>
)
