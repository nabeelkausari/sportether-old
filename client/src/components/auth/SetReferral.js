import React, { Component } from 'react';
import { setProvidedReferralId } from "../../config/utils";

import Loader from '../../components/layouts/Loader'

export default class SetReferral extends Component {
  componentWillMount() {
    if (this.props.match.params && this.props.match.params.refUsername) {
      setProvidedReferralId(this.props.match.params.refUsername)
    }
    this.props.history.push('/sign-up')
  }

  render() {
    return <Loader/>
  }
}
