import React, { Component } from 'react';
import { Field } from 'redux-form';

import renderField from '../renderField';
import Container from '../../../containers/auth/signin';

class Signin extends Component {
  handleFormSubmit = ({ email, password }) => {
    this.props.signinUser({ email, password, redirect: user => {
       if (!user.bonusClaimed && !user.isAdmin) {
         this.props.redirect('/account/bonus')
       } else {
         this.props.redirect('/marketplace')
       }
    }});
  }

  renderAlert = () => {
    if (this.props.signinError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.signinError}
        </div>
      )
    }
  }

  render() {
    return (
      <form className={'modal-form ' + this.props.className } onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
        <h5>Login to your account</h5>
        <fieldset className="form-group">
          <Field label="Email" name="email" component={renderField} type="email" placeholder="Enter your email address..." />
        </fieldset>
        <fieldset className="form-group">
          <Field label="Password" name="password" component={renderField} type="password" placeholder="Enter your password..." />
        </fieldset>
        {this.renderAlert()}
        <div className="form-group form-group--submit">
          <button action="submit" className="btn btn-primary-inverse btn-block">Enter to your account</button>
        </div>
        {/*<div className="modal-form--social">*/}
        {/*<h6>or Login with your social profile:</h6>*/}
        {/*<ul className="social-links social-links--btn text-center">*/}
        {/*<li className="social-links__item">*/}
        {/*<a href="#" className="social-links__link social-links__link--lg social-links__link--fb"><i className="fa fa-facebook" /></a>*/}
        {/*</li>*/}
        {/*<li className="social-links__item">*/}
        {/*<a href="#" className="social-links__link social-links__link--lg social-links__link--twitter"><i className="fa fa-twitter" /></a>*/}
        {/*</li>*/}
        {/*<li className="social-links__item">*/}
        {/*<a href="#" className="social-links__link social-links__link--lg social-links__link--gplus"><i className="fa fa-google-plus" /></a>*/}
        {/*</li>*/}
        {/*</ul>*/}
        {/*</div>*/}
      </form>
    )
  }
}

export default Container(Signin)
