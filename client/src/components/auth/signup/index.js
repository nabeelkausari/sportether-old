import React, { Component } from 'react';
import { Field } from 'redux-form';
import {NotificationManager} from 'react-notifications';

import renderField from '../renderField';
import Container from '../../../containers/auth/signup'
import { getReferralId } from "../../../config/utils";
import NotifyModal from '../../home/NotifyModal';

class Signup extends Component {
  state = {
    modal: false,
    modalTitle: '',
    modalMessage: '',
    modalLoading: true
  }
  handleFormSubmit = ({ email, password, username, referralId }) => {
    this.props.signupUser({
      email, password, username,
      referralUsername: referralId,
      preRedirect: this.confirmRedirection.bind(this) })
  }

  confirmRedirection = () => {
    this.setState({ modal: true })
  }

  renderAlert = () => {
    if (this.props.signupError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.signupError}
        </div>
      )
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.signupLoading !== this.props.signupLoading) {
      if (nextProps.signupLoading) {
        this.setState({ modalTitle: 'Creating', modalMessage: 'Hold on, your account is being created', modalLoading: true })
      } else if (nextProps.signupSuccess) {
        this.setState({ modalTitle: 'One more step!', modalMessage: 'You’ll receive a confirmation email in your inbox with a link to activate your account', modalLoading: false })
      } else {
        this.setState({ modalTitle: 'Oops!', modalMessage: 'Something went wrong, try again or contact our customer support', modalLoading: false })
      }
    }
  }

  render() {
    const hasReferralId = getReferralId() !== null;
    const { modal, modalTitle, modalMessage, modalLoading } = this.state;
    return (
      <form className={'modal-form ' + this.props.className } onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
        <h5>Register Now!</h5>
        <fieldset className="form-group">
          <Field label="Email" name="email" component={renderField} type="email" placeholder="Enter your email address..." />
        </fieldset>
        <fieldset className="form-group">
          <Field label="Username" name="username" component={renderField} type="text" placeholder="Enter your username..." />
        </fieldset>
        <fieldset className="form-group">
          <Field label="Password" name="password" component={renderField} type="password" placeholder="Enter your password..." />
        </fieldset>
        <fieldset className="form-group">
          <Field label="Confirm Password" name="confirmPassword" component={renderField} type="password" placeholder="Repeat your password..." />
        </fieldset>
        <fieldset className="form-group">
          <Field label="Referral ID" disabled={hasReferralId} name="referralId" component={renderField} type="text" placeholder="Enter you Referral's ID..." />
        </fieldset>
        <fieldset className="form-group form-group--submit">
          <button action="submit" className="btn btn-primary-inverse btn-block">Create Your Account</button>
        </fieldset>
        {this.renderAlert()}
        <div className="modal-form--note">You’ll receive a confirmation email in your inbox with a link to activate your account.</div>
        <NotifyModal
          onClose={() => {
            this.props.redirect('/marketplace');
            setTimeout(() => NotificationManager.success("", "Confirmation Email Sent", 5000), 1000)
          }}
          modal={modal}
          title={modalTitle}
          message={modalMessage}
          showConfirmBtn
          loading={modalLoading}
        />
      </form>
    )
  }
}

export default Container(Signup);
