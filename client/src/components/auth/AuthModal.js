import React from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import SignupForm from './signup';
import SigninForm from './signin';
import Container from '../../containers/auth';

class AuthModal extends React.Component {
  state = {
    modal: false,
    showLogin: this.props.authType === 'signin'
  };

  toggle = () => {
    if (this.state.modal) {
      this.props.clearAuthErrors();
    }
    this.setState({
      modal: !this.state.modal
    });
  }

  isLoggedIn = () => {
    return this.props.authLoading === false && this.props.authenticated === true;
  }

  handleLogout = () => {
    this.props.logoutUser({ redirect: this.props.redirect });
  }

  handleRedirect = () => {
    this.toggle();
    this.props.redirect();
  }

  toggleShowLogin = () => {
    this.setState({ showLogin: !this.state.showLogin})
  }

  render() {
    const { btnClass, btnText } = this.props;
    const { showLogin } = this.state;
    return (
      <div style={{ margin: 10, display: "inline-block" }}>
        { this.isLoggedIn()
            ? <Button onClick={this.handleLogout}>Logout</Button>
            : <Button onClick={this.toggle} className={btnClass}>{btnText}</Button>
        }
        <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal--login modal-lg">
          <ModalHeader toggle={this.toggle} />
          <ModalBody>
            <div className="modal-account-holder">
              <div className="modal-account__item">
                {showLogin && <Button className={this.props.className + ' btn-primary btn-block'} onClick={this.toggleShowLogin}>Register</Button>}
                <SignupForm className={showLogin ? 'hide-auth' : 'show-auth'} handleRedirect={this.handleRedirect}/>
              </div>
              <div className="modal-account__item">
                {!showLogin && <Button className={this.props.className + ' btn-primary btn-block'} onClick={this.toggleShowLogin}>Login</Button>}
                <SigninForm className={!showLogin ? 'hide-auth' : 'show-auth'} handleRedirect={this.handleRedirect} />
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default Container(AuthModal);
