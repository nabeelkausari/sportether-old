import React, { Component } from 'react';
import { Card, CardBody, CardTitle, Row, Col } from 'reactstrap'
import {NotificationManager} from 'react-notifications';
import { Field } from 'redux-form';
import FBLike from 'react-fb-like';
import { Follow, Share } from 'react-twitter-widgets'


import WithAuthState from '../../containers/hoc/withAuthState';
import Container from '../../containers/bonus';
import Layout from '../layouts';

class Bonus extends Component {
  handleFormSubmit = ({ facebook, twitter, telegram }) => {
    this.props.claimBonus({ facebook, twitter, telegram,
      redirect: () => {
        this.props.redirect('/marketplace')
        setTimeout(() => NotificationManager.success("Kindly wait till the admin confirms", "Bonus Claimed", 5000), 1000)
      }
    });
  }

  renderAlert = () => {
    if (this.props.claimBonusError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.claimBonusError}
        </div>
      )
    }
  }

  render() {
    const { user } = this.props;
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <Row>
              <Col style={{ marginBottom: 10 }}>
                <h2>Claim your bonus!</h2>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                {/*<h4>Join in our community and earn 300 SPO coins!</h4>*/}
                <Card>
                  <CardBody>
                    <CardTitle><h4>Join our social community and earn 300 SPO coins!</h4></CardTitle>
                    <div className="col-md-8" style={{ marginTop: 50 }}>
                      {user && user.bonusClaimed &&
                      <p className="desc">You have already claimed for bonus. Your bonus will be processed within 2 working days. For further information you can contact us at <a href="mailto:contact@sportether.com">contact@sportether.com</a></p>
                      }
                      {user && !user.bonusClaimed &&
                        <form onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
                          <fieldset className="form-group">
                            <Field
                              social={<div className="social-follow">
                                <FBLike appId="719852925070311" version="v2.12" href="https://www.facebook.com/sportether" />
                              </div>}
                              label="Facebook Username" name="facebook" component={renderFieldSocial} type="text" placeholder="Enter the your facebook username..." />
                          </fieldset>
                          <fieldset className="form-group">
                            <Field
                              social={<div className="social-follow">
                                <Follow username="sportether"/>
                                <Share url={`https://sportether.com?ref=${this.props.user.username}`}/>
                              </div>}
                              label="Twitter Username" name="twitter" component={renderFieldSocial} type="text" placeholder="Enter the your twitter username..." />
                          </fieldset>
                          <fieldset className="form-group">
                            <Field
                              social={<div className="social-follow">
                                <a className="telegram-btn" href="https://t.me/sportether" target="_blank">
                                  <i className="fa fa-telegram" /> Join Telegram
                                </a>
                              </div>}
                              label="Telegram Username" name="telegram" component={renderFieldSocial} type="text" placeholder="Enter the your telegram username..." />

                          </fieldset>
                          {this.renderAlert()}
                          <div className="text-center">
                            <button action="submit" className="btn btn-primary">
                              Submit
                            </button>
                          </div>
                        </form>
                      }
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    )
  }
}

const renderFieldSocial = ({input, label, placeholder, disabled, type, social, meta: { touched, error, warning }}) => (
  <div className="row" style={{ alignItems: "center" }}>
    <div className="col-md-4"><h6 style={{ marginBottom: 0, textAlign: 'right' }}>{label}</h6></div>
    <div className="col-md-4">
      <input {...input} disabled={disabled} placeholder={placeholder} type={type} className="form-control" />
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </div>
    <div className="col-md-4">{social}</div>
  </div>
)


export default WithAuthState(Container(Bonus));
