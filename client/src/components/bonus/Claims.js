import React, { Component } from 'react';
import moment from 'moment';
import { Row, Col } from 'reactstrap'


import WithAuthState from '../../containers/hoc/withAuthState';
import Container from '../../containers/bonus';
import Layout from '../layouts';

class Bonus extends Component {
  state = {
    approvalStart: false
  }
  componentWillMount() {
    this.props.getClaims();
  }

  handleApprove = userId => {
    this.setState({ approvalStart: true });
    this.props.confirmBonus({ userId })
  }
  render() {
    const { claims } = this.props;
    const { approvalStart } = this.state;
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <Row>
              <Col style={{ marginBottom: 10 }}>
                <h2>User claims</h2>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="card__content">
                  <div className="table-responsive">
                    <table className="table team-result claim-result">
                      <thead>
                      <tr>
                        <th className="team-result__date">Date</th>
                        <th className="team-result__score">Email</th>
                        <th className="team-result__score">Facebook</th>
                        <th className="team-result__points">Twitter</th>
                        <th className="team-result__rebounds">Telegram</th>
                        <th className="team-result__rebounds">Confirm</th>
                      </tr>
                      </thead>
                      <tbody>
                      {!claims || !claims.length && <tr>
                        <td className="team-result__date">No users had claimed for bonuses</td>
                      </tr>}
                      {claims && claims.map(claim => (
                        <tr>
                          <td className="team-result__date">{moment(claim.createdAt).format("ll LT")}</td>
                          <td className="team-result__status">{claim.userId.email}</td>
                          <td className="team-result__score">{claim.facebook}</td>
                          <td className="team-result__score">{claim.twitter}</td>
                          <td className="team-result__score">{claim.telegram}</td>
                          <td className="team-result__rebounds">
                            {!claim.userId.bonusApproved && <button disabled={approvalStart} onClick={() => this.handleApprove(claim.userId._id)} className="btn btn-primary">Approve</button>}
                            {claim.userId.bonusApproved && <button disabled>Approved</button>}
                          </td>
                        </tr>
                      ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    )
  }
}

const renderFieldSocial = ({input, label, placeholder, disabled, type, social, meta: { touched, error, warning }}) => (
  <div className="row" style={{ alignItems: "center" }}>
    <div className="col-md-4"><h6 style={{ marginBottom: 0, textAlign: 'right' }}>{label}</h6></div>
    <div className="col-md-4">
      <input {...input} disabled={disabled} placeholder={placeholder} type={type} className="form-control" />
      {touched &&
      ((error && <div className="error">{error}</div>) ||
        (warning && <span>{warning}</span>))}
    </div>
    <div className="col-md-4">{social}</div>
  </div>
)


export default WithAuthState(Container(Bonus));
