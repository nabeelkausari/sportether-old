import React from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import {CopyToClipboard} from 'react-copy-to-clipboard';

import WithAuthState from '../../containers/hoc/withAuthState';
import SocialShare from './SocialShare';

class InviteModal extends React.Component {
  state = {
    modal: false,
    copied: false
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
      copied: false
    });
  }

  render() {
    const { user } = this.props;
    if (!user) return <div/>
    const link = `https://sportether.com/ref/${ user.username }`;
    return (
      <div style={{ margin: 5, display: "inline-block" }}>
        <Button onClick={this.toggle} className="btn btn-primary highlight">Invite Friends</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className="modal--login">
          <ModalHeader toggle={this.toggle} />
          <ModalBody>
            <div className="modal-account-holder">
              <div className="modal-account__item" style={{ flexBasis: "100%", textAlign: 'center'}}>
                <h3>Referral Link</h3>
                <div style={{ textAlign: 'left', fontSize: 16 }} className={this.state.copied ? 'alert alert-success' : 'alert'}>{link}</div>
                <CopyToClipboard text={link}
                                 onCopy={() => this.setState({copied: true})}>
                  <Button className="text-center">Copy to clipboard</Button>
                </CopyToClipboard>
                <SocialShare link={link}/>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default WithAuthState(InviteModal);
