import React from 'react';
import { Container, Row, Col, Card, CardTitle, CardSubtitle, CardText, CardBody } from 'reactstrap';

import WithWallet from '../../containers/hoc/withWallet';
import {HOST} from '../../config/constants';
import { weiToEth } from "../../config/utils";
import BuyModal from './BuyModal';

export default ({ topTeamsLoading, topTeams }) => (
  <Container>
    <Row>
      <Col className="text-center">
        <h2>Top Teams</h2>
        {topTeamsLoading && <h4>Loading....</h4>}
      </Col>
    </Row>
    {topTeams && topTeams.length && <Cards topTeams={topTeams} /> }
  </Container>
)


const Cards = WithWallet(props => (
  <Row>
    {props.topTeams.map(teams => (
      <Row key={teams[0]._id} style={{ marginBottom: 20 }}>
        <h4>{teams[0].sportId.name}</h4>
        <Row>{teams.map(team => card(team, props))}</Row>
      </Row>
    ))}
  </Row>
))

const card = (team, props) => (
  <Col md={4} key={team._id}>
    <Card>
      <div className="custom-card-img">
        <img src={HOST + '/api/logos/' + team.logo} alt={`${team.symbol} Logo`} />
      </div>
      <CardBody>
        <CardTitle>{team.symbol} | {weiToEth(team.price)} ETH</CardTitle>
        <CardSubtitle>{team.name}</CardSubtitle>
        <CardText>{team.description.substr(0, 100)}...</CardText>
        <BuyModal team={team} {...props} />
      </CardBody>
    </Card>
  </Col>
)
