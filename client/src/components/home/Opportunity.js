import React from 'react';

import bg from '../../images/cheering.jpg';

export default () => (
  <div className="site-content home light text-center" style={{...style}}>
    <div className="container">
      <div className="row justify-content-md-center">
        <div className="col-md-12">
          <h2 className="title">A Gigantic Opportunity</h2>
          <p className="description">A regular sports fan Invests precious time and money in watching  the match via (ECM) electronic communication medium or buy a match ticket and visit stadium.
            Sentiments, loyalty and affection towards team is also invested. There is no monetary benefit nor much involvement to enhance his/her entertainment. </p>
        </div>
      </div>
    </div>

    <div className="container">
      <div className="row">
        <div className="card special" data-background-color="black">
          <p className="description">In SportEther you own the <b>Team Token</b> which is a crypto asset like (Bitcoin, Ethereum Etc.) hence making you a part of the team virtually. When your team performs live you are virtually connected to your teams performance by participating in <b>Virtual Tournament Grounds</b> which  will give you adrenaline rush for you to support and cheer for your teams win, ultimately enhancing your entertainment experience and if your team wins you are benefited with ethers. Which is a truly rewarding experience.</p>
        </div>
      </div>
    </div>

  </div>
)
const style = {
  background: 'linear-gradient(0deg, rgba(255, 255, 255, 0.1), rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0.9), rgb(255, 255, 255)), url('+ bg +') bottom/cover no-repeat'
}
