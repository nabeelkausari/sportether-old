import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div className="site-content home black highlights">
    <div className="container">
      <div className="row text-center">
        <div className="col">
          <h2 className="title">Team Tokens on Marketplace</h2>
        </div>
      </div>
      <div className="row justify-content-md-center">
        <div className="col-md-12 text-center">
          <p className="description">
            Following is the detailed table to our marketplace sale structure. Our sale structure is partitioned into 4 phases and is automated i.e. once a phase completes the other will automatically commence as it is illustrated in the table the early birds have the opportunity to grab major discounts.(For Eg. Phase 1 buyers have the opportunity to make <span className="highlight-red">5X</span> on their investment on conclusion of the sale). The exchange price  refers to the actual value of the token after the completion of the sale process.
          </p>
          <Link className="btn btn-danger" to="/marketplace">Visit Marketplace</Link>
        </div>
      </div>
      <div className="row" style={{ marginTop: 20}}>
        <div className="col-md-12">
          <div className="card__content light-table">
            <div className="table-responsive">
              <table className="table team-result">
                <thead>
                <tr>
                  <th className="team-result__date">Phases</th>
                  <th className="team-result__score">Token Value on Ethereum</th>
                  <th className="team-result__score">Discount</th>
                  <th className="team-result__points">Supply Available For Sale</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td className="team-result__date">Phase 1</td>
                  <td className="team-result__status">0.004 ETH</td>
                  <td className="team-result__score">80%</td>
                  <td className="team-result__score">10%</td>
                </tr>
                <tr>
                  <td className="team-result__date">Phase 2</td>
                  <td className="team-result__status">0.01 ETH</td>
                  <td className="team-result__score">50%</td>
                  <td className="team-result__score">20%</td>
                </tr>
                <tr>
                  <td className="team-result__date">Phase 3</td>
                  <td className="team-result__status">0.014 ETH</td>
                  <td className="team-result__score">30%</td>
                  <td className="team-result__score">50%</td>
                </tr>
                <tr>
                  <td className="team-result__date">Phase 4</td>
                  <td className="team-result__status">0.016 ETH</td>
                  <td className="team-result__score">20%</td>
                  <td className="team-result__score">20%</td>
                </tr>
                <tr>
                  <td className="team-result__date"><b>Exchange Price</b></td>
                  <td className="team-result__status" style={{ background: "#fecb0352" }}>0.02 ETH</td>
                  <td>-</td>
                  <td>-</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)
