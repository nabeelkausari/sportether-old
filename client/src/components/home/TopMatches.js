import React from 'react';
import { Container, Row, Col, Card, CardBody, CardTitle, Button } from 'reactstrap';
import {HOST} from "../../config/constants"

export default ({ topMatchesLoading, topMatches }) => {
  if (topMatchesLoading) return <h3>Loading....</h3>
  if (!topMatches) return <div></div>;
  return (
    <Container>
      <Row><Col><h3>Top Matches</h3></Col></Row>
      <Row>{topMatches.map(match => (
        <Col key={match._id} md={3}>
          <Card className="text-center">
            <div className="match-logos">
              <div className="match-logo"><img src={`${HOST}/api/logos/${match.teamA.token.logo}`}/></div>
              <div className="match-logo"><img src={`${HOST}/api/logos/${match.teamB.token.logo}`}/></div>
            </div>
            <CardBody>
              <CardTitle>{match.teamA.token.symbol} <span>vs</span> {match.teamB.token.symbol}</CardTitle>
              {/*<CardSubtitle>Card subtitle</CardSubtitle>*/}
              {/*<CardText>{match.description}</CardText>*/}
              <Button>Participate</Button>
            </CardBody>
          </Card>
        </Col>
      ))}</Row>
    </Container>
  )
}

