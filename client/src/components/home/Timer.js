import React, { Component } from 'react';

export default class Timer extends Component {
  state = {
    countDownDate: new Date("May 18, 2018 23:59:59").getTime(),
    days: '',
    hours: '',
    minutes: '',
    seconds: ''
  }

  timerSet = () => {
    let x = setInterval(() => {

      // Get todays date and time
      let now = new Date().getTime();

      // Find the distance between now an the count down date
      let distance = this.state.countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in an element with id="demo"
      this.setState({ days, hours, minutes, seconds })

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
      }
    }, 1000);
  }

  componentWillMount() {
    this.timerSet();
  }
  render() {
    const { days, hours, minutes, seconds, countDownDate } = this.state;
    if (days < 0) return <div/>
    return (
      <div>
        <h2 className="color-white">{this.props.headerText}</h2>
        <h4 className="color-white">{new Date(countDownDate).toDateString()} {new Date(countDownDate).toLocaleTimeString()}</h4>
        <div id="countdown">
          <dl><dt>{days}</dt><dd>days</dd><dd /></dl>
          <dl><dt>{hours}</dt><dd>hours</dd><dd /></dl>
          <dl><dt>{minutes}</dt><dd>minutes</dd><dd /></dl>
          <dl><dt>{seconds}</dt><dd>seconds</dd><dd /></dl>
        </div>
      </div>

    )
  }
}
