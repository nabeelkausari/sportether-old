import React, { Component } from 'react';
import { Element } from 'react-scroll'

import HomeContainer from '../../containers/home';

import HomeLayout from '../layouts/Home';
import About from './About';
import Hero from './HeroContent';
import Opportunity from './Opportunity';
import Benefits from './Benefits';
import Play from './Play';
import Spo from './Spo';
import FAQ from './Faq';
import Sale from './Sale';
import NoBetting from './NoBetting';

import TopTeams from './TopTeams';

import Coming from './ComingSoon';

class Home extends Component {

  componentWillMount() {
    // if (!this.props.topMatches) this.props.getTopMatches();
    // if (!this.props.topTeams) this.props.getTopTeams();
  }

  render() {
    const { topMatches, topMatchesLoading, topTeams, topTeamsLoading } = this.props
    return (
      <HomeLayout {...this.props}>
        <Hero/>
        <Element name="about">
          <About/>
        </Element>
        <Element name="opportunity">
          <Opportunity/>
        </Element>
        <Element name="benefits">
          <Benefits />
        </Element>
        <Element name="play">
          <Play />
        </Element>
        <Element name="spo">
          <Spo />
        </Element>
        <Element name="sale">
          <Sale />
        </Element>
        <NoBetting />
        <Element name="faq">
          <FAQ />
        </Element>
        {/*{TopTeams({ topTeamsLoading, topTeams })}*/}
        {/*{TopMatches({ topMatchesLoading, topMatches })}*/}
      </HomeLayout>
    )
  }
}

export default HomeContainer(Home)
