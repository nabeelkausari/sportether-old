import React from 'react';

export default () => (
  <div className="site-content home grey">
    <div className="container">
      <div className="row text-center">
        <div className="col">
          <h2>Benefits of using SportEther</h2>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-md-6">
          <h3 className="highlight-red">Team Token Ownership</h3>
          <p className="description sub">
            <span>1 in 1 Million</span> yes only 1 lucky sport fan out of 1 Million will be able to hold the exclusive Team Token and experience the feeling of a team owner because of the limited token supply, our supply is only 0.0001% of estimated fan following of a sport for E.g. football has 3.5 billion fans across the globe and we offer only 3500 Team Tokens.
          </p>
        </div>
        <div className="col-md-6">
          <h3 className="highlight-red">Crypto Token</h3>
          <p className="description sub">
            Team Token comes with 3 major benefits, Very first being participation in VTGs and earning money. Second, leasing Team token to other participants and earning. And the third, holding the team token for its rarity and trading in exchanges for huge profits.
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <h3 className="highlight-red">Solid Reward</h3>
          <p className="description sub">
            Rewards and earnings in the SportEther platform are accorded in <span>ethereum</span> which is a trusted crypto asset.
          </p>
        </div>
        <div className="col-md-6">
          <h3 className="highlight-red">Game Indulgence</h3>
          <p className="description sub">
            Pleasure of being a part of the team you love and play the sport in VTGs alongside your teams live performance giving an extraordinary feeling of performing with the team.
          </p>
        </div>
      </div>
    </div>
  </div>
)
