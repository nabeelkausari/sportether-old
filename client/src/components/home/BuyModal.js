import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form';

import {weiToEth} from "../../config/utils";
import renderField from '../auth/renderField';
import Container from '../../containers/teams/buyTeam';
import logo from '../../images/logo.png'

class BuyModal extends React.Component {
  state = {
    modal: false
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleFormSubmit = ({ unit }) => {
    this.props.buyTokens({ unit, tokenId: this.props.team._id, redirect: () => {
      this.toggle()
    } })
  }

  getDiscount = (discountedPrice) => {
    let actualPrice = 20000000000000000; // 0.02 ETH
    return 100 - (discountedPrice/actualPrice * 100)
  }

  renderAlert = () => {
    if (this.props.buyTokensError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {JSON.stringify(this.props.buyTokensError)}
        </div>
      )
    }
  }

  isLoggedIn = () => {
    return this.props.authLoading === false && this.props.authenticated === true;
  }

  render() {
    const { team, ethBalance, className, sport } = this.props;
    return (
      <div className={className}>
        <span className="price">{ weiToEth(team.price)} ETH <span className="off">{this.getDiscount(team.price)}% Off</span></span>
        <button onClick={this.toggle}>Buy</button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Buy {team.symbol}</ModalHeader>
          <form onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
            <ModalBody>
              <div className="container">
                {
                  this.isLoggedIn()
                    ? <div>
                      { (ethBalance > 0 )
                        ? <div>
                          <h4>Balance (ETH) : {ethBalance}</h4>
                          <div className="team-card">
                            <div className="card-logo">
                              <img src={logo}/>
                            </div>
                            <div className={`sport-title ${sport.toLowerCase()}-card-bg`}>
                              <h4>{sport}</h4>
                            </div>
                            <div className="body">
                              <div className="title">{team.symbol.substr(0, 2)}<span>{team.symbol.substr(2)}</span></div>
                              <div className="sub-title">{team.name}</div>
                              <div className="buy">
                                <span className="price">{ weiToEth(team.price)} ETH <span className="off">{this.getDiscount(team.price)}% Off</span></span>
                                <div className={`token-balance ${sport.toLowerCase()}-card-bg`}>
                                  <span>{ team.balance }</span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <fieldset className="form-group token-unit">
                            <Field label={`Qty (${team.symbol})`} name="unit" component={renderField} type="number" placeholder={`Enter amount of ${team.symbol}`} />
                          </fieldset>
                        </div>
                        : <div className="text-center">
                          <p>You need to add ethereum to your account in order to purchase a team token.</p>
                          <Link className="btn btn-primary" to="/account/wallet">Manage Wallet</Link>
                        </div>
                      }
                    </div>
                    : <div className="text-center">
                      <p>You need to be logged in to buy this token</p>
                      <Link className="btn btn-primary" to="/sign-up" style={{ marginRight: 10 }}>Sign Up</Link>
                      <Link className="btn btn-primary" to="/sign-in">Sign In</Link>
                    </div>
                }
                {this.renderAlert()}
              </div>
            </ModalBody>
            <ModalFooter>
              <div className="form-group form-group--submit">
                {this.isLoggedIn() && (ethBalance > 0 ) && <button className="btn btn-primary" action="submit">Buy</button>}{' '}
                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
              </div>
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}

export default Container(BuyModal);
