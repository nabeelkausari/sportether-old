import React from 'react';
import Slider from  'react-slick';

import slide1 from '../../images/spm-football.jpg';
import slide2 from '../../images/spm-cricket.jpg';

export default () => {
  return (
    <Slider className="slick posts posts-slider posts--slider-top-news" {...options}>
      {/* Slide #1 */}
      <div className="slick__slide">
        <div className="posts__item posts__item--category-1">
          {/* Main Image */}
          <figure className="posts__thumb">
            <img src={slide1} alt className="duotone-img" data-gradient-map="#282840, #f92552" />
          </figure>
          {/* Post Content */}
          <div className="posts__inner">
            <h3 className="posts__title">
              The Alchemists player
              <span className="posts__title-higlight">Steven Masterson</span> breaks a new world record
            </h3>
          </div>
          {/* Post Content / End */}
        </div>
      </div>
      {/* Slide #1 / End */}
      {/* Slide #2 */}
      <div className="slick__slide">
        <div className="posts__item posts__item--category-1">
          {/* Main Image */}
          <figure className="posts__thumb">
            <img src={slide2} alt className="duotone-img" data-gradient-map="#282840, #9e69ee" />
          </figure>
          {/* Main Image / End */}
          {/* Post Content */}
          <div className="posts__inner">\
            <h3 className="posts__title">
              Alchemists coach on <span className="posts__title-higlight">Jake Summer's injury</span>
              "It looks really bad"
            </h3>
          </div>
          {/* Post Content / End */}
        </div>
      </div>
      {/* Slide #2 / End */}
    </Slider>
  )
}
