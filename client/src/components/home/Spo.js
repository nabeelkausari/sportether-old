import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import WithAuthState from '../../containers/hoc/withAuthState';
import NotifyModal from './NotifyModal';
import coin from '../../images/spo-coin.png';
import coin2x from '../../images/spo-coin2x.png';

class SpoContent extends Component {
  state = {
    modal: false
  }

  handleModal = () => this.setState({ modal: !this.state.modal})
  render() {
    const isLoggedIn = this.props.authLoading === false && this.props.authenticated === true;
    return (
      <div className="site-content home light-grey">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-6">
              <h2 className="title">SPO Coin</h2>
              <p className="description">SportEther Coin (SPO Coin) is an ERC20 token powered by Ethereum Blockchain. Future platform transactions and other payments will be carried out in SPO Coins. It has a strong potential for increase in value and liquidity. SPO Coins will be tradable in major exchanges.</p>
              <Link className="btn btn-primary" to="/spo-coin">Read more</Link> {" "}
              { isLoggedIn && <Link className="btn btn-success" to="/account/bonus">Get Free Coins</Link>}
              { !isLoggedIn && <button className="btn btn-success" onClick={this.handleModal}>Get Free Coins</button>}
              <NotifyModal
                onClose={() => this.props.redirect('/sign-up')}
                modal={this.state.modal}
                title='Login first!'
                message='In order to receive your Free SPO Coins, you need to signup and claim for bonus'
                showConfirmBtn={false}
                loading={false}
                timeout={5000}
              />
            </div>
            <div className="col-md-6">
              <img src={coin} srcSet={`${coin2x} 2x`}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default WithAuthState(SpoContent)
