import React from 'react';
import { Link as ScrollLink } from 'react-scroll'
import { Link } from 'react-router-dom';

import InviteModal from './InviteModal';
import WithAuthState from '../../containers/hoc/withAuthState';
import logo from '../../images/logoLarge.png'
import Timer from '../home/Timer';

export default WithAuthState(props => {
  const isLoggedIn = props.authLoading === false && props.authenticated === true;
  return (
    <section id="container" className="header">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-xxs-12 col-md-6 left-main">
            <h1 className="logo"><img src={logo} alt="SportEther"/></h1>
            <p className="desc color-white" lang="en" data-lang-token="t2">
              A revolutionary sports entertainment & gaming platform
              built on top of <b>Ethereum Blockchain Technology</b>
            </p>
            <ScrollLink
              style={{ cursor: 'pointer' }}
              to='about' spy={true} smooth={true}
              offset={-62} duration={500}
              className="btn btn-white learn-more"
            >
              Learn more
            </ScrollLink>
          </div>
          <div className="col-xxs-12 col-md-6">
            <div className="right-main">
              <div className="hero-v2">
                <div className="countdown-box text-center">
                  <Timer headerText="Live match participation (VTG)" />
                  <div className="call-to-action">
                    {!isLoggedIn && <Link className="btn highlight" to="/sign-up">Sign Up</Link>}
                    {isLoggedIn && <InviteModal/>}
                    {!isLoggedIn
                      ? <h3>Sign up now and earn <span>300 SPO</span> coins!</h3>
                      : <h3>Refer and earn <span>200 SPO</span> coins on each signup!</h3>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bottom">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center" lang="en">
              Join us on
              <a href="https://t.me/sportether" target="_blank"><i className="fa fa-telegram" /></a>
              <a href="https://twitter.com/sportether" target="_blank"><i className="fa fa-twitter" /></a>
              <a href="https://www.facebook.com/sportether" target="_blank"><i className="fa fa-facebook" /></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
})
