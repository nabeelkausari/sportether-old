import React from 'react';
import {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  PinterestShareButton,
  VKShareButton,
  OKShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  RedditShareButton,
  EmailShareButton,
  TumblrShareButton,
  LivejournalShareButton,
  MailruShareButton,

  FacebookIcon,
  TwitterIcon,
  GooglePlusIcon,
  LinkedinIcon,
  PinterestIcon,
  VKIcon,
  OKIcon,
  TelegramIcon,
  WhatsappIcon,
  RedditIcon,
  TumblrIcon,
  MailruIcon,
  EmailIcon,
  LivejournalIcon,
} from 'react-share';

export default props => {
    const shareUrl = props.link;
    const title = 'SportEther';

    return (
      <div className="social">
        <h4>Promote via social channels</h4>
        <p className="desc">Remember! You will earn 200 SPO coins on each signup</p>
        <div className="social-share">
          <div className="social-item">
            <FacebookShareButton
              url={shareUrl}
              quote={title}
              className="social-btn">
              <FacebookIcon
                size={32}
                round />
            </FacebookShareButton>
          </div>

          <div className="social-item">
            <TwitterShareButton
              url={shareUrl}
              title={title}
              className="social-btn">
              <TwitterIcon
                size={32}
                round />
            </TwitterShareButton>
          </div>

          <div className="social-item">
            <TelegramShareButton
              url={shareUrl}
              title={title}
              className="social-btn">
              <TelegramIcon size={32} round />
            </TelegramShareButton>

          </div>

          <div className="social-item">
            <WhatsappShareButton
              url={shareUrl}
              title={title}
              separator=":: "
              className="social-btn">
              <WhatsappIcon size={32} round />
            </WhatsappShareButton>

          </div>

          <div className="social-item">
            <GooglePlusShareButton
              url={shareUrl}
              className="social-btn">
              <GooglePlusIcon
                size={32}
                round />
            </GooglePlusShareButton>
          </div>

          <div className="social-item">
            <LinkedinShareButton
              url={shareUrl}
              title={title}
              windowWidth={750}
              windowHeight={600}
              className="social-btn">
              <LinkedinIcon
                size={32}
                round />
            </LinkedinShareButton>
          </div>

          {/*<div className="social-item">*/}
          {/*<PinterestShareButton*/}
          {/*url={String(window.location)}*/}
          {/*media={`${String(window.location)}/${exampleImage}`}*/}
          {/*windowWidth={1000}*/}
          {/*windowHeight={730}*/}
          {/*className="social-btn">*/}
          {/*<PinterestIcon size={32} round />*/}
          {/*</PinterestShareButton>*/}

          {/*<div className="social-item">*/}
          {/*<VKShareButton*/}
          {/*url={shareUrl}*/}
          {/*image={`${String(window.location)}/${exampleImage}`}*/}
          {/*windowWidth={660}*/}
          {/*windowHeight={460}*/}
          {/*className="social-btn">*/}
          {/*<VKIcon*/}
          {/*size={32}*/}
          {/*round />*/}
          {/*</VKShareButton>*/}

          {/*<div className="social-item">*/}
          {/*<OKShareButton*/}
          {/*url={shareUrl}*/}
          {/*image={`${String(window.location)}/${exampleImage}`}*/}
          {/*windowWidth={660}*/}
          {/*windowHeight={460}*/}
          {/*className="social-btn">*/}
          {/*<OKIcon*/}
          {/*size={32}*/}
          {/*round />*/}
          {/*</OKShareButton>*/}

          {/*</div>*/}

          <div className="social-item">
            <RedditShareButton
              url={shareUrl}
              title={title}
              windowWidth={660}
              windowHeight={460}
              className="social-btn">
              <RedditIcon
                size={32}
                round />
            </RedditShareButton>
          </div>

          <div className="social-item">
            <TumblrShareButton
              url={shareUrl}
              title={title}
              windowWidth={660}
              windowHeight={460}
              className="social-btn">
              <TumblrIcon
                size={32}
                round />
            </TumblrShareButton>
          </div>

          <div className="social-item">
            <LivejournalShareButton
              url={shareUrl}
              title={title}
              description={shareUrl}
              className="social-btn"
            >
              <LivejournalIcon size={32} round />
            </LivejournalShareButton>
          </div>

          <div className="social-item">
            <MailruShareButton
              url={shareUrl}
              title={title}
              className="social-btn">
              <MailruIcon
                size={32}
                round />
            </MailruShareButton>
          </div>

          <div className="social-item">
            <EmailShareButton
              url={shareUrl}
              subject={title}
              body="body"
              className="social-btn">
              <EmailIcon
                size={32}
                round />
            </EmailShareButton>
          </div>
        </div>
      </div>
    );
}
