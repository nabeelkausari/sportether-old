import React from 'react';
import { Link } from 'react-router-dom';

import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody,
} from 'react-accessible-accordion';

export default () => (
  <div className="site-content home white">
    <div className="container">
      <div className="row text-center">
        <div className="col">
          <h2 className="title">FAQs</h2>
        </div>
      </div>
      <div className="row justify-content-md-center">
        <div className="col-md-12">
          <Accordion>
            <AccordionItem>
              <AccordionItemTitle>
                <h5>What is SportEther?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>SportEther is a unique sports entertainment gaming  platform wherein it offers its users to enjoy the privilege of owning their favorite sports team in the shape of a crypto asset  and participate in matches virtually  at the same time when their real team performs in live matches.</p>
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <h5>What is the SportEther Team Token?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>SportEther Team Tokens are ERC20 standard tokens and are crypto Assets like Bitcoin, Ethereum etc. Tokens are the operational assets behind every VTG participations. Team Tokens will be tradable on an exchange within and is fully compatible with most Ether wallets.</p>
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <h5>What is Virtual Tournament Ground (VTG)?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>Virtual tournament grounds (VTGs) are virtual stadiums for the team token holders to use/participate/play match which they wish to and connect to the live sports.</p>
              </AccordionItemBody>
            </AccordionItem>
            <AccordionItem>
              <AccordionItemTitle>
                <h5>How many SportEther Team tokens are available?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>Only 1 lucky sport fan out of <b>1 Million</b> will be able to hold the exclusive Team Token because of the <b>extremely limited token supply</b>, our supply is only <b>0.0001%</b> of estimated fan following of a sport for E.g. football has 3.5 billion fans across the globe and we offer only <b>3500</b> Team Tokens.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Are there any discounts for early buyers?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>Yes! We are offering a significant discounts to early adopters. Contact us at contact@sportether.com for more information.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Why do you accept only Ether (ETH)?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>By restricting the ability to purchase SportEther team tokens using only ETH, we’ve increased the level of security for supporters exponentially. SportEther team tokens will be available on your SportEther's Account and can be withdrawn to your desired ETH wallet address.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Is there any minimum token purchase?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>Minimum purchase is 1 team token.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>How can I own a team token?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>To own a team token, all you need to do is transfer Ethers to your SportEther account and purchase your desired team tokens.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>When will I receive my SportEther team tokens?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>SportEther team tokens will instantly reflect in your SportEther account after every successful purchase and thereafter is immediately transferable to any ERC20 compatible wallet.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Can I send Ethers from an exchange wallet?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>YES, you can send Ethers from an exchange wallet address (e.g., Coinbase, Binance)</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Do you have a referral program?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>Yes, You can find out more about our generous referral program here: <Link to="/referral-program">sportether.com/referral-program</Link>.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Where can I store my SportEther team tokens?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>You’ll be able to receive and store your Sportiex Team tokens in any Ethereum wallet supporting ERC20 tokens. You cannot store the tokens on any exchange-based wallet (e.g., Coinbase). Here is a list of the most popular compatible wallets:
                  <ul>
                    <li><a href="https://www.myetherwallet.com/" target="_blank">MyEtherWallet</a> (no download needed)</li>
                    <li><a href="https://metamask.io/" target="_blank">MetaMask</a> (Firefox and Chrome browser add-on)</li>
                    <li><a href="https://github.com/ethereum/mist/releases" target="_blank">Mist</a> (desktop)</li>
                    <li><a href="https://parity.io/" target="_blank">Parity</a> (desktop)</li>
                    <li><a href="https://token.im" target="_blank">imToken</a> (iOS/Android)</li>
                    <li><a href="https://itunes.apple.com/us/app/trust-ethereum-wallet/id1288339409" target="_blank">Trust</a> (iOS)</li>
                    <li><a href="https://play.google.com/store/apps/details?id=com.wallet.crypto.trustapp" target="_blank">Trust</a> (Android)</li>
                    <li><a href="https://itunes.apple.com/app/cipher-browser-for-ethereum/id1294572970?ls=1&amp;mt=8" target="_blank">Cipher</a> (iOS)</li>
                    <li><a href="https://play.google.com/store/apps/details?id=com.cipherbrowser.cipher" target="_blank">Cipher</a> (Android)</li>
                  </ul>
                </p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>Will the SportEther team token trade on exchanges?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>Yes, SportEther Team Tokens will trade in an exchange within shortly after the commencement of the platform. We will regularly communicate in this regard via our channels.</p>
              </AccordionItemBody>
            </AccordionItem>

            <AccordionItem>
              <AccordionItemTitle>
                <h5>How do i get in touch with you?</h5>
              </AccordionItemTitle>
              <AccordionItemBody>
                <p>You can reach us on <a href="https://t.me/sportether" target="_blank">Telegram</a> or via email contact@sportether.com.</p>
              </AccordionItemBody>
            </AccordionItem>

          </Accordion>
        </div>
      </div>
    </div>
  </div>
)
