import React from 'react';
import { Link } from 'react-router-dom';

import teams from '../../images/team-tokens.png';
import teams2x from '../../images/team-tokens2x.png';
import grounds from '../../images/grounds.png';
import grounds2x from '../../images/grounds2x.png';

export default () => (
  <div className="site-content home white">
    <div className="container">
      <div className="row">
        <div className="col-md-12 col-lg-12 text-center">
          <h2>About SportEther</h2>
          <p className="description">SportEther is engineered with a vision to enhance the sports entertainment experience and remunerate the sports fans for their following, investments, loyalty and love towards their teams.
            It provides an array of unique features in one of a kind platform. Starting with <b>team tokens</b> and <b>virtual tournament grounds</b>. Our aim is to benefit the sport fans by giving them a platform to connect with their favorite teams in live matches and compete alongside for rewards and extra benefits.</p>
        </div>
      </div>
    </div>

    <div className="container">
      <div className="row unique">
        <div className="col-lg-6 col-md-12">
          <img src={teams} srcSet={`${teams2x} 2x`} />
        </div>
        <div className="col-lg-6 col-md-12">
          <h3>Sports Team Tokens</h3>
          <p className="description sub">
            SportEther provides ERC20 tokens for all performing sports teams <b>as a gaming object within the platform</b>. These tokens are real assets on Ethereum blockchain and can be traded in crypto exchanges.
          </p>
          <Link to="/team-tokens" className="btn btn-inverse btn-sm btn-outline btn-condensed">Read more</Link>
        </div>
      </div>
    </div>

    <div className="container">
      <div className="row unique">
        <div className="col-lg-6 col-md-12">
          <img src={grounds} srcSet={`${grounds2x} 2x`} />
        </div>
        <div className="col-lg-6 col-md-12">
          <h3 className="title">Virtual Tournament Grounds</h3>
          <p className="description sub">
            Tournament grounds are connected to real live matches. All team token holders can participate in grounds for a minimal ground fee and can win rewards based on the match result.
          </p>
          <Link to="/virtual-tournament-grounds" className="btn btn-inverse btn-sm btn-outline btn-condensed">Read more</Link>
        </div>
      </div>
    </div>
  </div>
)
