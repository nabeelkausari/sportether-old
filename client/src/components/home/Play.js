import React from 'react';

export default () => (
  <div className="site-content home white">
    <div className="container">
      <div className="row text-center">
        <div className="col">
          <h2 className="title">How it works?</h2>
        </div>
      </div>
      <div className="row justify-content-md-center">
        <div className="col-md-12">
          <dl className="steps">
            <dt>Get a team token</dt>
            <dd>Buy it from marketplace, exchange or lease it from someone who already owns</dd>
            <dt>Set your reward goal</dt>
            <dd>Ground fee varies between (10, 25, 50, 75, 100)% of the valuation of your team token</dd>
            <dt>Enter a Virtual Tournament Ground (VTG)</dt>
            <dd>Create or join an existing one</dd>
            <dt>Let the fun begin</dt>
            <dd>Watch your team's performance</dd>
            <dt>Celebrate with rewards</dt>
            <dd>On your team's success, take your crypto rewards</dd>
          </dl>
        </div>
      </div>
    </div>
  </div>
)
