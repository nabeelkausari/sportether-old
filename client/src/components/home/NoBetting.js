import React from 'react';

export default () => (
  <div className="site-content home light-grey">
    <div className="container">
      <div className="row justify-content-md-center">
        <div className="col-md-12 text-center">
          <h2 className="title">Not a Betting Platform</h2>
          <p className="description">
            SportEther is a revolutionary entertaining platform with monetary benefits, it is unlike any other betting platform. The following table will illustrate to you how SportEther is different from others:          </p>
        </div>
        <div className="col-md-12">
          <div className="table-responsive">
            <table className="table team-result no-betting">
              <thead>
              <tr>
                <th className="team-result__date"><h4>Other Betting Platforms</h4></th>
                <th className="team-result__date" style={{ verticalAlign: "middle" }}><div className="no-betting-logo"/></th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td className="team-result__date">Only for gamblers</td>
                <td className="team-result__date">Only for Sport Enthusiasts & Investors</td>
              </tr>
              <tr>
                <td className="team-result__date">Restless predictors taking un-calculated risks and ending up making ir-repairable losses</td>
                <td className="team-result__date">Each and every sports lover, whether a stadium cheerer or a stream watcher can participate.</td>
              </tr>
              <tr>
                <td className="team-result__date">Extreme predictions required</td>
                <td className="team-result__date">Knowledge of the sport and love for the team is required, It helps users to celebrate the team’s win with real monetary gains.</td>
              </tr>
              <tr>
                <td className="team-result__date">High risk ratio due to unfixed stakes/bets</td>
                <td className="team-result__date">Fixed ground fees and fixed prize</td>
              </tr>
              <tr>
                <td className="team-result__date">Just a betting platform, not for investors</td>
                <td className="team-result__date">Investing in a tradable crypto team token is a proper form of investment.</td>
              </tr>
              <tr>
                <td className="team-result__date">Consists speculative nature</td>
                <td className="team-result__date">Essence of sporting body conducting virtual tournaments.</td>
              </tr>
              <tr>
                <td className="team-result__date">Can bet/invest at any moment of time during the match</td>
                <td className="team-result__date">Not a betting platform hence cannot make any calls once the match is commenced to the end of it.</td>
              </tr>
              <tr>
                <td className="team-result__date">You bet on a team/players</td>
                <td className="team-result__date">You own the team token therefore you are the team E.g. You participate in a car race with your own car and want to win.</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
)
