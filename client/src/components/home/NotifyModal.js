import React from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import WithAuthState from '../../containers/hoc/withAuthState';
import Spinner from "../wallet/Spinner"

class NotifyModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: props.modal
    };
  }

  close = () => {
    this.setState({ modal: false });
    if (this.props.onClose) this.props.onClose()
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.modal !== this.props.modal){
      this.setState({modal:nextProps.modal});
    }
  }

  timeOutModal = duration => {
    setTimeout(() => this.close(), duration)
  }

  render() {
    const { title, message, showConfirmBtn, confirmBtnText, timeout, loading } = this.props;
    return (
      <div style={{ margin: 5, display: "inline-block" }}>
        <Modal isOpen={this.state.modal} className="modal--login">
          <ModalHeader />
          <ModalBody>
            <div className="modal-account-holder">
              <div className="modal-account__item" style={{ flexBasis: "100%", textAlign: 'center'}}>
                <h3>{title}{loading && <Spinner theme='dark'/>}</h3>
                <h5>{message}</h5>
                {showConfirmBtn && <Button disabled={loading} onClick={this.close}>{confirmBtnText || 'OK'}</Button>}
                {!showConfirmBtn && this.state.modal && this.timeOutModal(timeout || 3000)}
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default WithAuthState(NotifyModal);
