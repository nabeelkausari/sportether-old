import React from 'react';
import Slider from 'react-slick';
import { chunk } from 'lodash';

import player1 from '../../images/samples/roster_player_01_sm.jpg';

import productImg from '../../images/samples/product-img1.png';
import featuredPlayer from '../../images/samples/widget-featured-player.png';
import logo from '../../images/logo.png';
import {HOST} from "../../config/constants"

const options = {
  slidesToShow: 3,
  autoplay: false,
  autoplaySpeed: 3000,
  arrows: true,
  dots: false,
  infinite: false,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        slidesToShow: 1
      }
    }
  ]
}

export default ({ topTeamsLoading, topTeams }) => (
  <div className="site-content">
    <div className="container">
      {/* Team Roster: Grid Slider */}
      {topTeams && topTeams.length && topTeams.map(teams => (
        <Slider key={teams[0]._id} {...options} className="team-roster team-roster--grid-sm js-team-roster--grid-sm row">
          {teams.map(team => card(team))}
        </Slider>
      ))}

      {/* Team Roster: Grid Slider / End */}
      <div className="row">
        {/* Team Roster: Table */}
        <div className="col-md-8">
          <div className="card card--has-table">
            <div className="card__header">
              <h4>Complete Roster</h4>
            </div>
            <div className="card__content">
              <div className="table-responsive">
                <table className="table table--lg team-roster-table">
                  <thead>
                  <tr>
                    <th className="team-roster-table__number">NBR</th>
                    <th className="team-roster-table__name">Player Name</th>
                    <th className="team-roster-table__position hidden-xs hidden-sm">Position</th>
                    <th className="team-roster-table__age">Age</th>
                    <th className="team-roster-table__height">Height</th>
                    <th className="team-roster-table__weight">Weight</th>
                    <th className="team-roster-table__college hidden-xs hidden-sm">College</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td className="team-roster-table__number">38</td>
                    <td className="team-roster-table__name">Games Girobili</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">1st Shooting Guard</td>
                    <td className="team-roster-table__age">18</td>
                    <td className="team-roster-table__height">6'66"</td>
                    <td className="team-roster-table__weight">205 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">South Beach College</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">24</td>
                    <td className="team-roster-table__name">Mark Stevens</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">1st Power Forward</td>
                    <td className="team-roster-table__age">20</td>
                    <td className="team-roster-table__height">6'8"</td>
                    <td className="team-roster-table__weight">220 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">Green Palm College</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">06</td>
                    <td className="team-roster-table__name">Jay T.Roks</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">1st Small Forward</td>
                    <td className="team-roster-table__age">17</td>
                    <td className="team-roster-table__height">6'10"</td>
                    <td className="team-roster-table__weight">234 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">The Sandy Coast Institute</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">15</td>
                    <td className="team-roster-table__name">Nick Rodgers</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">1st Point Guard</td>
                    <td className="team-roster-table__age">18</td>
                    <td className="team-roster-table__height">6'9"</td>
                    <td className="team-roster-table__weight">226 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">South Beach College</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">12</td>
                    <td className="team-roster-table__name">Tony Ironson</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">1st Center</td>
                    <td className="team-roster-table__age">19</td>
                    <td className="team-roster-table__height">6'7"</td>
                    <td className="team-roster-table__weight">240 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">Green Palm College</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">26</td>
                    <td className="team-roster-table__name">Thomas Black</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">2nd Shooting Guard</td>
                    <td className="team-roster-table__age">18</td>
                    <td className="team-roster-table__height">6'5"</td>
                    <td className="team-roster-table__weight">238 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">The Sandy Coast Institute</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">32</td>
                    <td className="team-roster-table__name">Spike Arrowhead</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">2nd Power Forward</td>
                    <td className="team-roster-table__age">18</td>
                    <td className="team-roster-table__height">6'8"</td>
                    <td className="team-roster-table__weight">252 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">The Sandy Coast Institute</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">39</td>
                    <td className="team-roster-table__name">Tim Robertson</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">2nd Small Forward</td>
                    <td className="team-roster-table__age">20</td>
                    <td className="team-roster-table__height">6'7"</td>
                    <td className="team-roster-table__weight">242 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">Green Palm College</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">09</td>
                    <td className="team-roster-table__name">Jake Dogmich</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">2nd Point Guard</td>
                    <td className="team-roster-table__age">20</td>
                    <td className="team-roster-table__height">6'10"</td>
                    <td className="team-roster-table__weight">213 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">Green Palm Beach</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">02</td>
                    <td className="team-roster-table__name">Griffin Peterson</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">2nd Center</td>
                    <td className="team-roster-table__age">19</td>
                    <td className="team-roster-table__height">6'8"</td>
                    <td className="team-roster-table__weight">217 lbs</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">South Beach College</td>
                  </tr>
                  <tr>
                    <td className="team-roster-table__number">CH</td>
                    <td className="team-roster-table__name">Robert Frankson</td>
                    <td className="team-roster-table__position hidden-xs hidden-sm">Team Coach</td>
                    <td className="team-roster-table__age">52</td>
                    <td className="team-roster-table__height">-</td>
                    <td className="team-roster-table__weight">-</td>
                    <td className="team-roster-table__college hidden-xs hidden-sm">Green Palm College</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        {/* Team Roster: Table / End */}
        {/* Featured Player */}
        <div className="col-md-4">
          {/* Widget: Featured Player - Alternative Extended */}
          <aside className="widget card widget--sidebar widget-player widget-player--alt">
            <div className="widget__title card__header">
              <h4>Featured Player</h4>
            </div>
            <div className="widget__content card__content">
              <div className="widget-player__team-logo">
                <img src={logo} />
              </div>
              <figure className="widget-player__photo">
                <img src={featuredPlayer} />
              </figure>
              <header className="widget-player__header clearfix">
                <div className="widget-player__number">38</div>
                <h4 className="widget-player__name">
                  <span className="widget-player__first-name">James</span>
                  <span className="widget-player__last-name">Girobili</span>
                </h4>
              </header>
              <div className="widget-player__content">
                <div className="widget-player__content-inner">
                  <div className="widget-player__stat widget-player__assists">
                    <h6 className="widget-player__stat-label">Assists</h6>
                    <div className="widget-player__stat-number">16.9</div>
                    <div className="widget-player__stat-legend">AVG</div>
                  </div>
                  <div className="widget-player__stat widget-player__steals">
                    <h6 className="widget-player__stat-label">Steals</h6>
                    <div className="widget-player__stat-number">7.2</div>
                    <div className="widget-player__stat-legend">AVG</div>
                  </div>
                  <div className="widget-player__stat widget-player__blocks">
                    <h6 className="widget-player__stat-label">Blocks</h6>
                    <div className="widget-player__stat-number">12.4</div>
                    <div className="widget-player__stat-legend">AVG</div>
                  </div>
                </div>
              </div>
              <footer className="widget-player__footer">
                    <span className="widget-player__footer-txt">
                      1st Shooting Guard
                    </span>
              </footer>
            </div>
            <div className="widget__content-secondary">
              {/* Player Details */}
              <div className="widget-player__details">
                <div className="widget-player__details-row">
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">2 Points</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">1250</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">3 Points</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">680</span>
                    </div>
                  </div>
                </div>
                <div className="widget-player__details-row">
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">Rebounds</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">234</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">Assists</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">751</span>
                    </div>
                  </div>
                </div>
                <div className="widget-player__details-row">
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">Steals</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">472</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">Blocks</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">565</span>
                    </div>
                  </div>
                </div>
                <div className="widget-player__details-row">
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">Fouls</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">97</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                          <span className="widget-player__details-holder">
                            <span className="widget-player__details-label">Game Played</span>
                            <span className="widget-player__details-desc">In his career</span>
                          </span>
                      <span className="widget-player__details-value">104</span>
                    </div>
                  </div>
                </div>
              </div>
              {/* Player Details / End */}
            </div>
            <div className="widget__content-tertiary widget__content--bottom-decor">
              <div className="widget__content-inner">
                <div className="widget-player__stats row">
                  <div className="col-xs-4">
                    <div className="widget-player__stat-item">
                      <div className="widget-player__stat-circular circular">
                        <div className="circular__bar" data-percent={88}>
                          <span className="circular__percents">88<small>%</small></span>
                        </div>
                        <span className="circular__label">Shot<br /> Accuracy</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-4">
                    <div className="widget-player__stat-item">
                      <div className="widget-player__stat-circular circular">
                        <div className="circular__bar" data-percent={63}>
                          <span className="circular__percents">63<small>%</small></span>
                        </div>
                        <span className="circular__label">Pass<br /> Accuracy</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-4">
                    <div className="widget-player__stat-item">
                      <div className="widget-player__stat-circular circular">
                        <div className="circular__bar" data-percent="75.5">
                          <span className="circular__percents">75.5<small>%</small></span>
                        </div>
                        <span className="circular__label">Total<br /> Efficiency</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </aside>
          {/* Widget: Featured Player - Alternative Extended / End */}
        </div>
        {/* Featured Player / End */}
      </div>
    </div>
  </div>
)

const card = team => (
  <div className="col-sm-6 col-md-6 col-lg-4" key={team._id}>
    {/* Team Member #1 */}
    <div className="team-roster__item">
      <div className="team-roster__holder">
        <figure className="team-roster__img">
          <a href="#">
            <img src={HOST + '/api/logos/' + team.logo} alt={`${team.symbol} Logo`} />
          </a>
        </figure>
        <div className="team-roster__content">
          <header className="team-roster__member-header">
            <div style={{ display: 'flex' }}>
              <div className="team-roster__member-number pre">{team.symbol.substr(0, 2)}</div>
              <div className="team-roster__member-number">{team.symbol.substr(2)}</div>
            </div>
            <h2 className="team-roster__member-name">
              <span className="team-roster__member-first-name">SportEther</span>
              <span className="team-roster__member-last-name">{team.name.substr(8)}</span>
            </h2>
          </header>
          <ul className="team-roster__member-details list-unstyled">
            <li className="team-roster__member-details-item"><span className="item-title">Price:</span> <span className="item-desc">0.02 ETH</span></li>
            <li className="team-roster__member-details-item"><span className="item-title">Available:</span> <span className="item-desc">8000/25000</span></li>
          </ul>
        </div>
      </div>
    </div>
    {/* Team Member #1 / End */}
    {/* Team Member #4 */}
    <div className="team-roster__item">
      <div className="team-roster__holder">
        <figure className="team-roster__img">
          <a href="#">
            <img src={HOST + '/api/logos/' + team.logo} alt={`${team.symbol} Logo`} />
            <span className="btn-fab" />
          </a>
        </figure>
        <div className="team-roster__content">
          <header className="team-roster__member-header">
            <div style={{ display: 'flex' }}>
              <div className="team-roster__member-number pre">{team.symbol.substr(0, 2)}</div>
              <div className="team-roster__member-number">{team.symbol.substr(2)}</div>
            </div>
            <h2 className="team-roster__member-name">
              <span className="team-roster__member-first-name">SportEther</span>
              <span className="team-roster__member-last-name">{team.name.substr(8)}</span>
            </h2>
          </header>
          {/*<div className="team-roster__member-subheader">*/}
            {/*<div className="team-roster__member-position">{team.name.substr(8)}</div>*/}
          {/*</div>*/}
          <ul className="team-roster__member-details list-unstyled">
            <li className="team-roster__member-details-item"><span className="item-title">Height:</span> <span className="item-desc">6'9"</span></li>
            <li className="team-roster__member-details-item"><span className="item-title">Weight:</span> <span className="item-desc">202lbs</span></li>
            <li className="team-roster__member-details-item"><span className="item-title">Age:</span> <span className="item-desc">19</span></li>
          </ul>
        </div>
      </div>
    </div>
    {/* Team Member #4 / End */}
  </div>
)
