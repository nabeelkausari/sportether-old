import React from 'react';

import logo from '../../images/logoLarge.png'
import Timer from '../home/Timer';

export default () => {
  return (
    <section id="container" className="header coming">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-xxs-12 col-md-6 left-main">
            <h1 className="logo coming" style={{ display: 'block !important'}}><img src={logo} alt="SportEther"/></h1>
            <p className="desc color-white" lang="en" data-lang-token="t2">
              A revolutionary sports entertainment & gaming platform
              built on top of <b>Ethereum Blockchain Technology</b>
            </p>
          </div>
          <div className="col-xxs-12 col-md-6">
            <div className="right-main">
              <div className="hero-v2">
                <div className="countdown-box text-center">
                  <Timer headerText="Witness the revolution of sports entertainment" />
                  <div className="call-to-action">
                    <h3><span>Coming Soon</span></h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bottom">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center" lang="en">
              Join us on
              <a href="https://t.me/sportether" target="_blank"><i className="fa fa-telegram" /></a>
              <a href="https://twitter.com/sportether" target="_blank"><i className="fa fa-twitter" /></a>
              <a href="https://www.facebook.com/sportether" target="_blank"><i className="fa fa-facebook" /></a>
              <a href="https://instagram.com/sportether" target="_blank"><i className="fa fa-instagram" /></a>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
