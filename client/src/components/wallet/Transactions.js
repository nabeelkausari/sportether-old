import React from 'react';
import moment from 'moment';
import {weiToEth} from "../../config/utils"

const Transactions = (transactions) => {
  if (!transactions || !transactions.length) {
    return (
      <div style={{ marginTop: 40 }}>
        <h4>Transaction History (ETH)</h4>
        <div className="card card--has-table transaction">
          <div className="card__content">
            <div className="table-responsive">
              <table className="table team-result">
                <thead>
                <tr>
                  <th className="team-result__date">Date</th>
                  <th className="team-result__score">Type</th>
                  <th className="team-result__score">Team</th>
                  <th className="team-result__score">Units</th>
                  <th className="team-result__score">Ethers</th>
                  <th className="team-result__score">Status</th>
                </tr>
                </thead>
                <tbody>
                <tr className="transaction-row">
                  <td>You have no transaction records</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }
  let deposits = transactions.filter(t => t.type === 'deposit');
  let withdrawals = transactions.filter(t => t.type === 'withdrawal');
  let bonuses = transactions.filter(t => t.type === 'bonus');
  let purchases = transactions.filter(t => t.type === 'buy');
  return (
    <div className="row" style={{ marginTop: 40 }}>
      <div className="col-md-6">
        <h4>Deposit History (ETH)</h4>
        <div style={{ minHeight: '306px', maxHeight: '306px', overflow: 'scroll', marginBottom: 30 }} className="card card--has-table transaction">
          <div className="card__content">
            <div className="table-responsive">
              <table className="table team-result">
                <thead>
                <tr>
                  <th className="team-result__date">Date</th>
                  <th className="team-result__score">Ethers</th>
                  <th className="team-result__score">Status</th>
                </tr>
                </thead>
                <tbody>
                {(!deposits || !deposits.length)
                  ? <tr className="transaction-row">
                    <td>You have no deposit records</td>
                  </tr>
                  : deposits.map(txn => (
                    <tr className="transaction-row" key={txn._id}>
                      <td>{moment(txn.createdAt).format("ll LT")}</td>
                      <td className="text-center">{txn.ethUnit && weiToEth(txn.ethUnit)}</td>
                      <td className="text-center">{txn.status}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-6">
        <h4>Withdrawal History (ETH)</h4>
        <div style={{ minHeight: '306px', maxHeight: '306px', overflow: 'scroll', marginBottom: 30 }} className="card card--has-table transaction">
          <div className="card__content">
            <div className="table-responsive">
              <table className="table team-result">
                <thead>
                <tr>
                  <th className="team-result__date">Date</th>
                  <th className="team-result__score">Ethers</th>
                  <th className="team-result__score">Txn Link</th>
                  <th className="team-result__score">Status</th>
                </tr>
                </thead>
                <tbody>
                {(!withdrawals || !withdrawals.length)
                  ? <tr className="transaction-row">
                    <td>You have no withdrawal records</td>
                  </tr>
                  : withdrawals.map(txn => (
                    <tr className="transaction-row" key={txn._id}>
                      <td>{moment(txn.createdAt).format("ll LT")}</td>
                      <td className="text-center">{txn.ethUnit && weiToEth(txn.ethUnit)}</td>
                      <td className="text-center"><a target="_blank" href={`https://etherscan.io/tx/${txn.txnId}`}>Link</a></td>
                      <td className="text-center">{txn.status}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-6">
        <h4>Purchase History (ETH)</h4>
        <div style={{ minHeight: '306px', maxHeight: '306px', overflow: 'scroll', marginBottom: 30 }} className="card card--has-table transaction">
          <div className="card__content">
            <div className="table-responsive">
              <table className="table team-result">
                <thead>
                <tr>
                  <th className="team-result__date">Date</th>
                  <th className="team-result__score">Team</th>
                  <th className="team-result__score">Units</th>
                  <th className="team-result__score">Ethers</th>
                  <th className="team-result__score">Status</th>
                </tr>
                </thead>
                <tbody>
                {(!purchases || !purchases.length)
                  ? <tr className="transaction-row">
                    <td>You have no purchase records</td>
                  </tr>
                  : purchases.map(txn => (
                    <tr className="transaction-row" key={txn._id}>
                      <td>{moment(txn.createdAt).format("ll LT")}</td>
                      <td className="text-center">{txn.tokenId && txn.tokenId.symbol}</td>
                      <td className="text-center">{txn.tokenUnit}</td>
                      <td className="text-center">{txn.ethUnit && weiToEth(txn.ethUnit)}</td>
                      <td className="text-center">{txn.status}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-6">
        <h4>Bonus History (ETH)</h4>
        <div style={{ minHeight: '306px', maxHeight: '306px', overflow: 'scroll', marginBottom: 30 }} className="card card--has-table transaction">
          <div className="card__content">
            <div className="table-responsive">
              <table className="table team-result">
                <thead>
                <tr>
                  <th className="team-result__date">Date</th>
                  <th className="team-result__score">From</th>
                  <th className="team-result__score">Ethers</th>
                  <th className="team-result__score">Status</th>
                </tr>
                </thead>
                <tbody>
                {(!bonuses || !bonuses.length)
                  ? <tr className="transaction-row">
                    <td>You have no bonus records</td>
                  </tr>
                  : bonuses.map(txn => (
                    <tr className="transaction-row" key={txn._id}>
                      <td>{moment(txn.createdAt).format("ll LT")}</td>
                      <td className="text-center">{txn.bonusFrom.username}</td>
                      <td className="text-center">{txn.ethUnit && weiToEth(txn.ethUnit)}</td>
                      <td className="text-center">{txn.status}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Transactions;
