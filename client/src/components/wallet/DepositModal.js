import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import syncBtn from './Sync';

class DepositModal extends React.Component {
  state = {
    modal: false,
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    const {
      syncWallet, syncWalletLoading, ethAddress, ethBalance, disabled } = this.props;
    return (
      <div style={{ marginRight: 10, display: "inline-block" }}>
        <Button disabled={disabled} onClick={this.toggle}>Deposit</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Deposit Ethereum</ModalHeader>
          <ModalBody>
            <div>
              <p className="desc">
                In order to buy team tokens in SportEther you have to fund your
                account with Ethers.
                To do this, send Ethers to your Ethereum address and <b className="highlight-deposit-text">click 'UPDATE BALANCE' Button after blockchain confirmations</b>
              </p>
              <p className="alert address">{ethAddress}</p>
            </div>
          </ModalBody>
          <ModalFooter>
            {syncBtn({ syncWalletLoading, syncWallet })}
            <Button color="secondary" onClick={this.toggle}>Ok</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default DepositModal;
