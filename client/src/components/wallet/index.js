import React, { Component } from 'react';
import { Card, CardBody, CardTitle, CardSubtitle, Row, Col } from 'reactstrap'
import {NotificationManager} from 'react-notifications';
import ReactTooltip from 'react-tooltip'
import { Link } from 'react-router-dom';

import Layout from '../layouts';
import WithdrawModal from './WithdrawModal';
import DepositModal from './DepositModal';
import syncBtn from './Sync';
import Transactions from './Transactions';
import TeamTokens from './TeamTokens';
import WalletContainer from '../../containers/wallet';
import WithAuthState from '../../containers/hoc/withAuthState';

class Wallet extends Component {
  componentWillMount() {
    this.props.getBalance()
    this.props.getTransactions()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.syncWalletMessage !== this.props.syncWalletMessage && nextProps.syncWalletMessage !== "") {
      if (nextProps.syncWalletUpdated) {
        NotificationManager.success(nextProps.syncWalletMessage, 'Deposit Success', 5000);
      } else {
        NotificationManager.warning(nextProps.syncWalletMessage, '', 3000);
      }
    }
    if (
      nextProps.withdrawEthLoading !== this.props.withdrawEthLoading &&
      nextProps.withdrawEthLoading === false && nextProps.withdrawEthTxn
    ) {
      NotificationManager.success('Txn Details', 'Withdrawal Success', 15000, () => {
        window.open(
          'https://etherscan.io/tx/' + nextProps.withdrawEthTxn,
          '_blank'
        );
      });
    }
  }


  render() {
    const {
      syncWallet, syncWalletLoading, transactions, tokens, withdrawEthLoading,
      ethAddress, ethBalance, spoBalance, spoReserved, user } = this.props;
    const spo = spoReserved + spoBalance;
    const approx = spo * 0.16;
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <Row>
              <Col style={{ marginBottom: 10 }}>
                <h1>Wallet</h1>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <h4>Ethereum (ETH)</h4>
                <Card>
                  <CardBody>
                    <h2>{ethBalance} ETH</h2>
                    <CardSubtitle>Available Ethereum Balance</CardSubtitle>
                    <div style={{ marginTop: 20 }}>
                      <DepositModal {...this.props} />
                      <WithdrawModal {...this.props} />
                      {syncBtn({ syncWalletLoading, syncWallet, disabled: withdrawEthLoading })}
                    </div>
                  </CardBody>
                </Card>
              </Col>
              <Col md={6}>
                <h4>SportEther Coin (SPO)</h4>
                <Card>
                  <CardBody>
                    <h2>{spo} SPO <span>
                      <span style={{ display: 'inline', color: '#c12a2e', fontWeight: 700, fontSize: 16 }}>${approx} </span>
                      <span style={{ fontWeight: 500, textTransform: 'none', color: "#9fa5ab", fontSize: 16 }}>(approx)</span>
                    </span></h2>
                    <CardSubtitle>Available SportEther Coin Balance</CardSubtitle>
                    <div style={{ marginTop: 20 }}>
                      <a data-tip="Coming Soon"><DepositModal  disabled {...this.props} /></a>
                      <a data-tip="Coming Soon"><WithdrawModal disabled {...this.props} /></a>
                      <ReactTooltip place="top" type="dark" effect="solid"/>
                      {user && !user.bonusClaimed && !user.isAdmin &&
                      <Link className="btn btn-primary" to="/account/bonus">Claim Bonus</Link>
                      }
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            {TeamTokens(tokens)}
            {Transactions(transactions)}
          </div>
        </div>
      </Layout>
    )
  }
}

export default WithAuthState(WalletContainer(Wallet));
