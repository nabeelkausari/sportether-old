import React from 'react';
import { Button } from 'reactstrap';

import Spinner from './Spinner';

export default ({ syncWalletLoading, syncWallet, disabled }) => (
  <div style={{ display: 'inline-block'}}>
    <Button disabled={syncWalletLoading || disabled} className={!syncWalletLoading && 'btn-success'} onClick={syncWallet}>
      {syncWalletLoading
        ? <div>Updating <Spinner/></div>
        : <div><i className="fa fa-refresh" aria-hidden="true"/> Update Balance</div>
      }
    </Button>
  </div>
)
