import React from 'react';

export default tokens => {
  return (
    <div className="card card--has-table">
      <div className="card__header card__header--has-btn">
        <h4>Team Tokens</h4>
      </div>
      <div className="card__content">
        <div className="table-responsive">
          <table className="table team-result">
            <thead>
            <tr>
              <th className="team-result__date">Token Name</th>
              <th className="team-result__score">Symbol</th>
              <th className="team-result__score">Available Balance</th>
              <th className="team-result__points">Deposit</th>
              <th className="team-result__rebounds">Withdraw</th>
            </tr>
            </thead>
            <tbody>
            {!tokens || !tokens.length && <tr>
              <td className="team-result__date">You have no team tokens</td>
            </tr>}
            {tokens && tokens.map(token => (
              <tr key={token.tokenId.symbol}>
                <td className="team-result__date">SportEther {token.tokenId.name}</td>
                <td className="team-result__status">{token.tokenId.symbol.substr(0,2)}<span className="team-result__game">{token.tokenId.symbol.substr(2)}</span></td>
                <td className="team-result__score">{token.balance}</td>
                <td className="team-result__rebounds"><button className="btn-custom" disabled>Coming Soon</button></td>
                <td className="team-result__assists"><button className="btn-custom" disabled>Coming Soon</button></td>
              </tr>
            ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
