import React from 'react';

export default ({ theme = 'light' }) => (
  <div className="spinner">
    <div style={{ backgroundColor: theme === 'light' ? '#fff' : '#000'}} className="bounce1" />
    <div style={{ backgroundColor: theme === 'light' ? '#fff' : '#000'}} className="bounce2" />
    <div style={{ backgroundColor: theme === 'light' ? '#fff' : '#000'}} className="bounce3" />
  </div>
)
