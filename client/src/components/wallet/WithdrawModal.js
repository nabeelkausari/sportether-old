import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Field } from 'redux-form';

import {ethToWei, weiToEth} from "../../config/utils";
import Container from '../../containers/wallet/withdrawEth';
import renderField from '../auth/renderField';

class WithdrawModal extends React.Component {
  state = {modal: false};

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleFormSubmit = ({ units, address }) => {
    this.props.withdrawEth({ units: ethToWei(units), address });
  }

  renderAlert = () => {
    if (this.props.withdrawEthError) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.withdrawEthError}
        </div>
      )
    }
  }

  handleUnitChange = (event, value) => {
    let fee = value ? weiToEth(ethToWei(value) - ethToWei(0.002)) : 0
    this.props.change('fee', fee);
  }

  render() {
    const { disabled, ethBalance, form, withdrawEthLoading } = this.props;
    return (
      <div style={{ marginRight: 10, display: "inline-block" }}>
        <Button disabled={disabled} onClick={this.toggle}>Withdraw</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className + ' modal-lg'}>
          <ModalHeader toggle={this.toggle}>Withdraw Ethereum</ModalHeader>
          <form onSubmit={this.props.handleSubmit(this.handleFormSubmit)}>
            <ModalBody>
              <div className="container">
                <h5>Balance: {ethBalance} ETH</h5>
                <fieldset className="form-group">
                  <Field disabled={withdrawEthLoading} label="Quantity (ETH)" onChange={this.handleUnitChange} name="units" component={renderField} type="number" placeholder="Enter the amount of ethers..." />
                </fieldset>
                <fieldset className="form-group">
                  <Field disabled={withdrawEthLoading} label="Address" name="address" component={renderField} type="text" placeholder="Enter your ETH withdrawal address..." />
                </fieldset>
                <fieldset className="form-group">
                  <Field label="Amount - (Fee 0.002 ETH)" name="fee" component={renderField} type="number" disabled />
                </fieldset>
                {this.renderAlert()}
                <div className="alert alert-warning">
                  <p>Please verify your withdrawal address. We cannot refund an incorrect withdrawal.</p>
                  <p><strong>Do not withdraw directly to a crowdfund or ICO. We will not credit your account with tokens from that sale.</strong></p>
                  <p>In the withdrawal address field, we only support hex addresses. DO NOT use direct iban, indirect iban, or unique userid. A proper hex address starts with "0x"</p>
                  <p>Please use a small amount for your first withdrawal to verify everything is working as expected before sending larger amounts. We cannot recover your funds if the address is incorrect.</p>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <div className="form-group form-group--submit">
                <button disabled={withdrawEthLoading} action="submit" className="btn btn-primary">
                  {withdrawEthLoading ? 'Processing...' : 'Withdraw'}
                </button>
                <Button color="secondary" onClick={this.toggle}>{withdrawEthLoading ? 'OK' : 'Cancel'}</Button>
              </div>
            </ModalFooter>
          </form>
        </Modal>
      </div>
    );
  }
}


export default Container(WithdrawModal);
