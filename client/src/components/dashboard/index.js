import React, { Component } from 'react';
import { Card, CardBody, CardTitle, CardSubtitle, Row, Col, ListGroup, ListGroupItem, Collapse, CardHeader, Badge } from 'reactstrap'

import Layout from '../layouts';
import Modal from './Modal';
import Transactions from './Transactions';
import WalletContainer from '../../containers/wallet';
import {HOST} from "../../config/constants"

class Wallet extends Component {
  state = { collapse: 0, cards: [1, 2, 3, 4, 5] }

  toggle = e => {
    let event = e.target.dataset.event;
    this.setState({ collapse: this.state.collapse === Number(event) ? 0 : Number(event) });
  }

  componentWillMount() {
    this.props.getBalance()
    this.props.getTransactions()
  }

  render() {
    console.log(this.props)
    const { depositEth, withdrawEth, transactions, tokens } = this.props;
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <Row>
              <Col style={{ marginBottom: 10 }}>
                <h1>Dashboard</h1>
              </Col>
            </Row>
            <Row>
              <Col>
                <h4>Ethereum (ETH)</h4>
                <Card>
                  <CardBody>
                    <CardTitle>{this.props.ethBalance} ETH</CardTitle>
                    <CardSubtitle>Available Ethereum Balance</CardSubtitle>
                    <div style={{ marginTop: 20 }}>
                      <Modal buttonLabel="Deposit" action={depositEth} />
                      <Modal buttonLabel="Withdraw" action={withdrawEth} />
                    </div>
                  </CardBody>
                </Card>
                { transactions && Transactions(transactions)}
              </Col>
              <Col md="2">
                <h4>Team Tokens</h4>
                <ListGroup>
                  { tokens && tokens.map(token => (
                    <ListGroupItem className="justify-content-between list-token">
                      <div className="list-token-logo">
                        <img src={`${HOST}/api/logos/${token.tokenId.logo}`}/>
                      </div>
                      {token.tokenId.symbol} <Badge style={{ marginLeft: 10 }} pill>{token.balance}</Badge>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                {/*{cardAccordian(this.state.cards, this.state.collapse, transactions, this.toggle, depositEth, withdrawtEth)}*/}

                {/*<h4>Team Token (SCRCB)</h4>*/}
                {/*<Card>*/}
                {/*<CardBody>*/}
                {/*<CardTitle>{this.props.ethBalance} ETH</CardTitle>*/}
                {/*<CardSubtitle>Available Ethereum Balance</CardSubtitle>*/}
                {/*<div style={{ marginTop: 20 }}>*/}
                {/*<Modal buttonLabel="Deposit" action={depositEth} />*/}
                {/*<Modal buttonLabel="Withdraw" action={withdrawtEth} />*/}
                {/*</div>*/}
                {/*</CardBody>*/}
                {/*</Card>*/}
                {/**/}
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    )
  }
}

const cardAccordian = (cards, collapse, transactions, toggle, depositEth, withdrawtEth) => {
  return cards.map(index => {
    return (
      <Card style={{ marginBottom: '1rem' }} key={index}>
        <CardHeader onClick={toggle} data-event={index}>Header</CardHeader>
        <Collapse isOpen={collapse === index}>
          <CardBody>
            <Card>
              <CardBody>
                <CardTitle>12 SCRCB</CardTitle>
                <CardSubtitle>Available TEAM Balance</CardSubtitle>
                <div style={{ marginTop: 20 }}>
                  <Modal buttonLabel="Deposit" action={depositEth} />
                  <Modal buttonLabel="Withdraw" action={withdrawtEth} />
                </div>
              </CardBody>
            </Card>
          </CardBody>
          <CardBody>
            { transactions && Transactions(transactions)}
          </CardBody>
        </Collapse>
      </Card>
    )
  })
}

export default WalletContainer(Wallet);
