import React from 'react';
import moment from 'moment';
import { Table, Row, Col } from 'reactstrap'
import {weiToEth} from "../../config/utils"

const Transactions = (transactions) => (
  <Row style={{ marginTop: 20 }}>
    <Col>
      <h4>Transaction History</h4>
      <Table responsive striped bordered>
        <thead>
        <tr>
          <th>Date</th>
          <th>Type</th>
          <th>Team</th>
          <th>Unit</th>
          <th>Ethers</th>
          <th>Status</th>
        </tr>
        </thead>
        <tbody>
        {transactions.map(txn => (
          <tr key={txn._id}>
            <td>{moment(txn.createdAt).format("ll LT")}</td>
            <td>{txn.type}</td>
            <td>{txn.tokenId && txn.tokenId.symbol}</td>
            <td>{txn.tokenUnit}</td>
            <td>{txn.ethUnit && weiToEth(txn.ethUnit)}</td>
            <td>{txn.status}</td>
          </tr>
        ))}
        </tbody>
      </Table>
    </Col>
  </Row>
)

export default Transactions;
