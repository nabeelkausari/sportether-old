import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { ethToWei } from "../../config/utils";

class TransactionModal extends React.Component {
  state = {
    modal: false,
    withdrawalAddress: '',
    ethValue: ''
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleEthValue = e => {
    this.setState({ ethValue: e.target.value })
  }

  handleWithdrawalAddress = e => {
    this.setState({ withdrawalAddress: e.target.value })
  }

  handleAction = () => {
    this.props.action(ethToWei(this.state.ethValue), this.state.withdrawalAddress)
    this.toggle()
    this.setState({ withdrawalAddress: '', ethValue: '' })
  }

  render() {
    const { buttonLabel } = this.props;
    const { ethValue, withdrawalAddress } = this.state;
    return (
      <div style={{ marginRight: 10, display: "inline-block" }}>
        <Button onClick={this.toggle}>{buttonLabel}</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>{buttonLabel} Ether</ModalHeader>
          <ModalBody>
            <div>
              <Input type="number" value={ethValue}
                     onChange={this.handleEthValue}
                     placeholder="Enter amount of ethers"
              />
              Enter the amount of ethers you want to {buttonLabel}
            </div>
            {buttonLabel === "Withdraw" && <div>
                <Input type="text" value={withdrawalAddress}
                       onChange={this.handleWithdrawalAddress}
                       placeholder="Enter withdrawal address"
                />
                Enter your address, where you want to receive your ethers
              </div>
            }
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.handleAction}>{buttonLabel}</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default TransactionModal;
