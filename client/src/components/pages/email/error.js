import React from 'react';

import Layout from '../../layouts';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Oops!</h1>
            <p>Something went wrong with your email confirmation!</p>
          </div>
        </div>
      </div>
    </div>

  </Layout>
)
