import React from 'react';
import { Link } from 'react-router-dom';

import Layout from '../../layouts';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Email Confirmed!</h1>
            <p>Your email address has been confirmed!</p>
            <Link className="btn btn-primary" to="/sign-in">Login</Link>
          </div>
        </div>
      </div>
    </div>
  </Layout>
)
