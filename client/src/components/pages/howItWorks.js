import React from 'react';

import Layout from '../layouts';
import Play from '../home/Play';

export default props => (
  <Layout {...props}>
    <Play />
  </Layout>
)
