import React from 'react';

import Layout from '../layouts';
import vtgProcess from '../../images/vtg.png';
import vtgProcess2x from '../../images/vtg2x.png';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-lg-12 text-center">
            <h2>Virtual Tournament Grounds (VTG)</h2>
            <p className="description">
              Virtual tournament grounds (VTGs) are virtual stadiums for the team token holders to use/participate/play match which they wish to and connect to the live sports. VTGs have a dynamic working design where it calculates the ground fee according to team token value and and the token holder selects his desired participation slabs of 10, 25, 50, 75 and 100% to enter into the VTG.
              Once entered the VTG will automatically start your virtual match exactly at the time when the real live match commences.
              The virtual match will end when the real live match ends. If you win you will be accorded your prize as per calculations of VTG.
            </p>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <h3>Exemplification of VTG</h3>
            <p className="description sub">
              Bren holds a token of Team ‘A’ whose value is 0.02 ether and Walter holds Team ‘B’ whose value is 0.03 ether.
            </p>
            <p className="description sub">
              Both decide to participate in their teams upcoming match against each other. Anyone of the holder creates a VTG of 25% value and the other joins his ground by accepting the same VTG value.
            </p>
            <p className="description sub">
              As the VTG value here is 25%, team ‘A’s ground fee is 25% of 0.02 ether which is 0.005 ether and respectively team ‘B’s ground fee is 0.0075 and the reward for the match is by adding 0.005 + 0.0075 - 6% (match conduction fee) = 0.01175 ethers.  Once both the team have accepted and successfully entered the VTG the virtual match will commence and conclude automatically when the real live match commences and concludes resulting in win, lose or draw, who ever wins he will take the prize of 0.01175 ethers. And if its a draw the VTG fee will be reverted to their respective owners after the deduction of 6% match conduction fee.
            </p>
          </div>
          <div className="col-md-6">
            <img src={vtgProcess} srcSet={`${vtgProcess2x} 2x`} />
          </div>
        </div>
      </div>

    </div>
  </Layout>
)
