import React from 'react';
import { Link } from 'react-router-dom';

import Layout from '../layouts';
import WithAuthState from '../../containers/hoc/withAuthState';
import InviteModal from '../home/InviteModal';

import referral2 from '../../images/SportEther-Referral-1.png';
import referral1 from '../../images/SportEther-Referral-2.png';

export default WithAuthState(props => {
  const isLoggedIn = props.authLoading === false && props.authenticated === true;
  return (
    <Layout {...props}>
      <div className="site-content home white">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-lg-12 text-center">
              <h2>SportEther Dual Referral Infinite Bonanza</h2>
              <p className="description">
                Yes! For the very first time in the crypto industry we give you two referral opportunities and a chance to make our referral program your business. Every SportEther user holds an opportunity to refer and earn by using his unique referral link.            </p>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row text-center">
            <div className="col-md-6">
              <img src={referral2}/>
            </div>
            <div className="col-md-6">
              <h3>SPO Coins Giveaway Campaign</h3>
              <p className="description sub">
                SPO Coins Giveaway Campaign allows you to collect SPO coins for FREE by inviting your friends through your referral link to join our social community. If your friend completes signup process using your referral link and claims for bonus after joining our social community. You will receive 200 SPO coins and your friend will receive 300 SPO coins.
              </p>
              {isLoggedIn ? <InviteModal/> : <Link className="btn btn-primary" to="/sign-up">Signup Now</Link>}
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row text-center">
            <div className="col-md-6">
              <img src={referral1}/>
            </div>
            <div className="col-md-6">
              <h3>Ethereum Referral Program</h3>
              <p className="description sub">
                Ethereum Referral Program gives you an opportunity to earn 10% of ethereum on the token purchase of your referral/friend and he gets 5% as money back and there after if he refers to any third person you get 1% of the third parties purchase as royalty for parent referee. We have designed our Team Token Referral program in such a way that you can earn in continuity after referring, giving you a fabulous business opportunity with our Team Token Referral Program
              </p>
              <Link className="btn btn-primary" to="/marketplace">Visit Marketplace</Link>
            </div>
          </div>
        </div>

      </div>
    </Layout>
  )
})
