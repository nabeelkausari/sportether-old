import React from 'react';

import Layout from '../layouts';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-lg-12 text-center">
            <h2>DISCLAIMER & LEGAL</h2>
            <p className="description">
              Sportiex Ltd, or its channel SportEther.com is not affiliated in any way to and claims no association, in any capacity whatsoever, with any sport team or any sport governing bodies.
            </p>
              <p className="description">Sportiex acknowledges that the respective sport authorities  national and international, all sport league systems of all sports domestic/international, franchises/teams own all proprietary names and marks relating to the relevant tournament or competition. However Sportiex Ltd offerings via its channel SportEther.com is the sole and absolute property of Sportiex Ltd.
              </p>
              <p className="description">Sportiex acknowledges that intellectual property rights in the logo and marks, may be owned by a third party including the sport federations, clubs, teams, leagues, cups, clubs, players’, managers, broadcasters etc. Any reference or depiction of the logos, marks and names on SportEther.com, is merely incidental in nature. Please check the relevant laws in your jurisdiction before accessing Sportiex offerings. Any liability or penalty, for violation of the applicable laws is solely of the user.
            </p></div>
        </div>
      </div>
    </div>
  </Layout>
)
