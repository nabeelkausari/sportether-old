import React from 'react';

import Layout from '../layouts';
import About from '../home/About';

export default props => (
  <Layout {...props}>
    <About />
  </Layout>
)
