import React from 'react';

import Layout from '../layouts';

import coin from '../../images/spo-coin-dark-bg.png';
import coin2x from '../../images/spo-coin-dark-bg2x.png';

export default props => (
  <Layout {...props}>
    <div className="site-content home black highlights">
      <div className="container">
        <div className="row">
          <div className="col-md-12 text-center">
            <img style={{ height: "50vh" }} src={coin} srcSet={`${coin2x} 2x`}/>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>What are SPO coins?</h3>
            <p className="description sub">SPO is a Digital asset Built on top of Ethereum Blockchain, The total Supply of SPO is 1 Billion,we expect the value, utility, and liquidity of SPO to increase substantially.</p>
            <p className="description sub">By owning SPO coins through the SportEther signup and referral reward program, you become one of the <b>early</b> holders of the coin and we expect you to reap benefits with the rising utility & demand of the coin.</p>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>Why are you giving away SPO coins?</h3>
            <p className="description sub">
              <b><i>“When You make a commitment, you build hope. When you keep it You Build Trust”</i></b> our generous gesture of rewarding with free SPO is to build Trust and to involve you in helping us build SportEther. We want to reward you for joining in early and being part of the SportEther success.
            </p>
            <p className="description sub">
              There will be multiple ways of earning SPO without needing to pay for it for early adopters of our community.
            </p>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>How to participate in SPO Give away Campaign?</h3>
            <ol className="description sub">
              <li>Sign up</li>
              <li>Join our telegram Community</li>
              <li>Follow us on twitter and Facebook</li>
              <li>Refer others to earn extra SPO</li>
            </ol>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>How can I see my SPO balance?</h3>
            <p className="description sub">
              You will be able to see your SPO balance in your wallet.
            </p>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>What would be the next phases of SPO distribution and when would they launch?</h3>
            <p className="description sub">
              We’re working on a few more phases internally and we’ll announce new phases of the SPO <b>Community Distribution Program</b> soon for you to be able to earn more SPO. You can keep track of these by joining our <a href="https://t.me/sportether" target="_blank"><b>Telegram Channel</b> - http://t.me/SportEther</a>  and following us on <a href="https://twitter.com/sportether" target="_blank"><b>Twitter</b> - http://twitter.com/SportEther</a>.            </p>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>What’s the future of SPO?</h3>
            <p className="description sub">
              We have quite a few interesting use cases lined up for SPO and how you can benefit from it. A few of the future implementations would be:
            </p>
            <ul className="description sub">
              <li>Discounts on platform transactions if done via SPO coins.</li>
              <li>We intend to launch our Esports Platform At that point, SPO will be used as Fuel for Rewarding Gamers.</li>
              <li>We have plans to share our profitable revenue to the SPO Holders.</li>
              <li>And more to come....</li>
            </ul>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>Can I use SPO to trade Team Tokens?</h3>
            <p className="description sub">
              As and when we list SPO on SportEther, you will be able to trade it like any other cryptocurrency. You will also be able to use SPO Coin for any other use cases we enlist in the future. Since SPO is a utility coin/token for our platform SportEther, we have plans of bringing in more use cases for SPO eventually.
            </p>
          </div>
        </div>
      </div>
    </div>
  </Layout>
)
