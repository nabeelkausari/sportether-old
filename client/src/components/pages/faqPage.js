import React from 'react';

import Layout from '../layouts';
import FAQ from '../home/Faq';

export default props => (
  <Layout {...props}>
    <FAQ />
  </Layout>
)
