import React from 'react';
import { Link } from 'react-router-dom';

import Layout from '../layouts';
import Signup from '../auth/signup';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-lg-6 text-center">
            <Signup />
          </div>
          <div className="col-md-6 col-lg-6">
            <Link to="/sign-in" className="btn btn-primary btn-block">Login</Link>
          </div>
        </div>
      </div>
    </div>

  </Layout>
)
