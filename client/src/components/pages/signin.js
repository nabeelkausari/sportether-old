import React from 'react';
import { Link } from 'react-router-dom';

import Layout from '../layouts';
import Signin from '../auth/signin';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-lg-6">
            <Link to="/sign-up" className="btn btn-primary btn-block">Register</Link>
          </div>
          <div className="col-md-6 col-lg-6 text-center">
            <Signin/>
          </div>
        </div>
      </div>
    </div>
  </Layout>
)
