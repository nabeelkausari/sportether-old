import React from 'react';
import { Link } from 'react-router-dom';

import Layout from '../layouts';
import token1 from '../../images/token-1.png';
import token2 from '../../images/token-2.png';
import token3 from '../../images/token-3.png';

export default props => (
  <Layout {...props}>
    <div className="site-content home white">
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-lg-12 text-center">
            <h2>Sports Team Tokens</h2>
            <p className="description">
              SportEther Team Tokens are digital Assets like Bitcoin, Ethereum etc. Tokens are the operational assets behind every VTG participations. Team Tokens will be tradable on an exchange within and is fully compatible with most Ether wallets.
            </p>
            <Link to="/marketplace" className="btn btn-primary">Checkout Team Tokens</Link>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row text-center">
          <div className="col-md-4">
            <div className="icon"><img src={token1}/></div>
            <h3>Fast and secure</h3>
            <p className="description sub">
              SportEther Team Tokens are  fully secure digital asset regulated by a Smart Contract on the Ethereum blockchain. Transferring SportEther team tokens is fast and easy thanks to Ethereum technology.</p>
          </div>
          <div className="col-md-4">
            <div className="icon"><img src={token2}/></div>
            <h3>ERC-20 based</h3>
            <p className="description sub">
              SportEther Team Tokens are fully compliant with the most popular token standard in the world: ERC-20. This makes it compatible with a broad list of wallets (e.g., MyEtherWallet, MetaMask), a truly key factor for boosting the token’s widespread adoption.
            </p>
          </div>
          <div className="col-md-4">
            <div className="icon"><img src={token3}/></div>
            <h3>Value holder</h3>
            <p className="description sub">
              SportEther Team Tokens  are extremely limited tokens. Token demand will rise as  there are limited tokens and vast number of buyers. Demand growth will likely result in a significant token price increase.
            </p>
          </div>
        </div>
      </div>

    </div>
  </Layout>
)
