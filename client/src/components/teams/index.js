import React, { Component } from 'react';

import TeamsContainer from '../../containers/teams';
import Layout from '../layouts';
import TeamsList from './List';

class Teams extends Component {

  componentWillMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.sport !== prevProps.match.params.sport) {
      this.loadData();
    }
  }

  loadData = () => {
    let { sport } = this.props.match.params;
    this.props.getTeams(sport)
  }

  render() {
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <div className="row">
              <div className="col">
                <h2>Teams page {this.props.match.params.sport}</h2>
                <TeamsList {...this.props} />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default TeamsContainer(Teams)
