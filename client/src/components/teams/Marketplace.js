import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';

import WithWallet from '../../containers/hoc/withWallet';
import TeamsContainer from '../../containers/teams';
import Layout from '../layouts';
import Selected from './Selected';
import Loader from '../layouts/Loader';


class Marketplace extends Component {

  componentWillMount() {
    this.props.getSelectedTeams()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.boughtTokens &&
      nextProps.boughtTokens !== this.props.boughtTokens &&
      nextProps.buyTokensLoading === false
    ) {
      NotificationManager.success(
        `${nextProps.boughtTokens.unit} ${nextProps.boughtTokens.symbol}`,
        'Token Purchase Success', 15000
      );
    }
  }

  render() {
    let { selectedTeams } = this.props;
    if (!selectedTeams) return <Loader/>;
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <div className="row">
              <div className="col">
                <h3>Marketplace</h3>
                <Selected sport="Football" sportTeams={selectedTeams.football} {...this.props} />
                <Selected sport="Cricket" sportTeams={selectedTeams.cricket} {...this.props} />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default WithWallet(TeamsContainer(Marketplace))
