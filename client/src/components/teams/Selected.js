import React, { Component } from 'react';
import { Container, Row } from 'reactstrap';
import { Link } from 'react-router-dom';

import BuyModal from "../home/BuyModal"

import logo from '../../images/logo.png'

class Selected extends Component {
  render() {
    let { sportTeams, sport } = this.props;
    return (
      <Container style={{ marginBottom: 60 }}>
        <Row>
          <div className="col-md-12 text-center">
            <h4>{sport}</h4>
          </div>
        </Row>
        <Row>{sportTeams && sportTeams.map(team => (
          <div key={team._id} className="col-md-4 team-list" >
            <div className="team-card">
              <div className="card-logo">
                <img src={logo}/>
              </div>
              <div className={`sport-title ${sport.toLowerCase()}-card-bg`}>
                <h4>{sport}</h4>
              </div>
              <div className="body">
                <div className="title">{team.symbol.substr(0, 2)}<span>{team.symbol.substr(2)}</span></div>
                <div className="sub-title">{team.name}</div>
                <BuyModal sport={sport} className="buy" team={team} { ...this.props } />
              </div>
            </div>
          </div>
        ))}</Row>
        <Row style={{ marginTop: 20 }}>
          <div className="col-md-12 text-center">
            <Link className="btn btn-primary" to={`/marketplace/${sport.toLowerCase()}`}>All {sport} teams</Link>
          </div>
        </Row>
      </Container>
    )
  }
}

export default Selected

