import React, { Component } from 'react';
import { Container, Row, Input } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

import BuyModal from "../home/BuyModal"
import WithWallet from '../../containers/hoc/withWallet';
import Loader from '../layouts/Loader';

import logo from '../../images/logo.png'

class TeamList extends Component {
  state = {
    teamList: []
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.boughtTokens &&
      nextProps.boughtTokens !== this.props.boughtTokens &&
      nextProps.buyTokensLoading === false
    ) {
      NotificationManager.success(
        `${nextProps.boughtTokens.unit} ${nextProps.boughtTokens.symbol}`,
        'Token Purchase Success', 15000
      );
    }
    if (nextProps.teams !== this.props.teams) {
      this.setState({ teamList: nextProps.teams })
    }
  }

  filterList = event => {
    let updatedList = this.props.teams;
    updatedList = updatedList.filter(item => {
      return (
        item.symbol.toLowerCase().search(event.target.value.toLowerCase()) !== -1 ||
        item.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1
      );
    });
    this.setState({teamList: updatedList});
  }

  render() {
    let { teams } = this.props;
    let { teamList } = this.state;
    if (!teams || !teams.length) return <Loader/>;
    return (
      <Container>
        <Row>
          <div className="col-md-12 filter-team">
            <Input onChange={this.filterList} placeholder="Search Team..."/>
          </div>
        </Row>
        {!teamList.length && <Row><div className="col-md-4"><h5>No Teams Found</h5></div></Row>}
        <Row>{teamList.map(team => (
          <div key={team._id} className="col-md-4 team-list" >
            <div className="team-card">
              <div className="card-logo">
                <img src={logo}/>
              </div>
              <div className={`sport-title ${team.sportId.name.toLowerCase()}-card-bg`}>
                <h4>{team.sportId.name}</h4>
              </div>
              <div className="body">
                <div className="title">{team.symbol.substr(0, 2)}<span>{team.symbol.substr(2)}</span></div>
                <div className="sub-title">{team.name}</div>
                <BuyModal sport={team.sportId.name} className="buy" team={team} { ...this.props } />
              </div>
            </div>
          </div>
        ))}</Row>
      </Container>
    )
  }
}

export default WithWallet(TeamList)

