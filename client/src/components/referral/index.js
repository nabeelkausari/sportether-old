import React, { Component } from 'react';
import moment from 'moment';
import { Row, Col } from 'reactstrap'

import InviteModal from '../home/InviteModal';
import Container from '../../containers/referral';
import Layout from '../layouts';
import { weiToEth } from "../../config/utils";

class ReferralTracker extends Component {

  componentWillMount() {
    this.props.getReferrals();
  }

  render() {
    const { referralList } = this.props;
    return (
      <Layout {...this.props}>
        <div className="site-content">
          <div className="container">
            <Row>
              <Col style={{ marginBottom: 10 }}>
                <h2>Referral Tracker</h2>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="card__content" style={{ background: '#fff' }}>
                  <div className="table-responsive">
                    <table className="table team-result">
                      <thead>
                      <tr>
                        <th className="team-result__date">Date</th>
                        <th className="team-result__score">Email</th>
                        <th className="team-result__score">Username</th>
                        <th className="team-result__score">Bonus Claimed</th>
                        <th className="team-result__score">Bonus Approved</th>
                        <th className="team-result__points">SPO Bonus</th>
                        <th className="team-result__rebounds">Ethereum Bonus</th>
                      </tr>
                      </thead>
                      <tbody>
                      {!referralList || !referralList.length && [<tr>
                        <td colSpan={7} className="team-result__date text-center">You've not referred anyone yet!</td>
                      </tr>,<tr>
                        <td colSpan={7} className="team-result__date text-center"><InviteModal/></td>
                      </tr>
                      ]}
                      {referralList && referralList.map(referral => (
                        <tr>
                          <td className="team-result__date">{moment(referral.user.createdAt).format("ll LT")}</td>
                          <td className="team-result__status">{referral.user.email}</td>
                          <td className="team-result__score">{referral.user.username}</td>
                          <td className="team-result__score">{referral.user.bonusClaimed ? 'Yes' : 'No'}</td>
                          <td className="team-result__score">{referral.user.bonusApproved ? 'Yes' : 'No'}</td>
                          <td className="team-result__score">{referral.spoBonus}</td>
                          <td className="team-result__rebounds">{weiToEth(referral.ethBonus)}</td>
                        </tr>
                      ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </Layout>
    )
  }
}


export default Container(ReferralTracker);
