import React, { Component } from 'react';

import GroundsContainer from '../../containers/grounds';
import Layout from '../layouts';
import GroundsList from './List';

class Home extends Component {

  render() {
    console.log(this.props.match.params)
    return (
      <Layout>
        <h1>Grounds detail</h1>
        <GroundsList {...this.props} />
      </Layout>
    )
  }
}

export default GroundsContainer(Home)
