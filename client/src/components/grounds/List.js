import React from 'react';
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, Button } from 'reactstrap';
import {HOST} from "../../config/constants"

export default ({ groundsLoading, grounds, singleMatch, enterGround, exitGround, me }) => {
  if (groundsLoading) return <h3>Loading....</h3>
  if (!grounds || !singleMatch || !me ) return <div></div>;
  let openGrounds = grounds.filter(ground => (
    ground.open === true
    || (
      (ground.teamA.user && ground.teamA.user.toString() === me._id)
      || (ground.teamB.user && ground.teamB.user.toString() === me._id)
    )
  ));
  return (
    <Container>
      <Row><Col><h3>Grounds</h3></Col></Row>
      {!openGrounds.length && <div>No open grounds</div>}
      <Row>{openGrounds.map(ground => (
        <Col key={ground._id} md={3}>
          <Card className="text-center">
            <div className="match-logos">
              <div style={hasEntered(ground, 'teamA')} className="match-logo">
                <img src={`${HOST}/api/logos/${singleMatch.teamA.token.logo}`}/>
              </div>
              <div style={hasEntered(ground, 'teamB')} className="match-logo">
                <img src={`${HOST}/api/logos/${singleMatch.teamB.token.logo}`}/>
              </div>
            </div>
            <CardBody>
              <CardTitle>{singleMatch.teamA.token.symbol} <span>vs</span> {singleMatch.teamB.token.symbol}</CardTitle>
              <CardText>{ground.percent}%</CardText>
              {renderTeamAButton(ground, singleMatch, enterGround, exitGround, me)}
              {renderTeamBButton(ground, singleMatch, enterGround, exitGround, me)}
            </CardBody>
          </Card>
        </Col>
      ))}</Row>
    </Container>
  )
}

const renderTeamAButton = (ground, singleMatch, enterGround, exitGround, me) => {
  if (ground.canExit && ground.teamA.user && ground.teamA.user.toString() === me._id) {
    return (
      <Button
        className="btn-danger"
        onClick={() => exitGround(ground._id, singleMatch._id)}
        style={{ marginRight: 20 }}
      >Exit A</Button>
    )
  }
  return (
    <Button
      className="btn-success"
      onClick={() => enterGround(ground._id, singleMatch.teamA.token._id, singleMatch._id)}
      disabled={ground.teamA.user || singleMatch.isLive || singleMatch.complete}
      style={{ marginRight: 20 }}
    >Enter A</Button>
  )
}
const renderTeamBButton = (ground, singleMatch, enterGround, exitGround, me) => {
  if (ground.canExit && ground.teamB.user && ground.teamB.user.toString() === me._id) {
    return (
      <Button
        className="btn-danger"
        onClick={() => exitGround(ground._id, singleMatch._id)}
      >Exit B</Button>
    )
  }
  return (
    <Button
      className="btn-success"
      onClick={() => enterGround(ground._id, singleMatch.teamB.token._id, singleMatch._id)}
      disabled={ground.teamB.user || singleMatch.isLive || singleMatch.complete}
    >Enter B</Button>
  )
}

const hasEntered = (ground, teamSide) => {
  return ground[teamSide].user ? {opacity: 1} : {opacity: 0.1};
}

