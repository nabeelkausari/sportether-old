import React from 'react';
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, Button } from 'reactstrap';
import {HOST} from "../../config/constants"

export default ({ groundsLoading, grounds, singleMatch, me, withdrawReward, withdrawTeam }) => {
  if (groundsLoading) return <h3>Loading....</h3>
  if (!grounds || !singleMatch || !me ) return <div></div>;
  let myGrounds = grounds.filter(ground => (
    ((ground.teamA.user && ground.teamA.user.toString() === me._id) ||
      (ground.teamB.user && ground.teamB.user.toString() === me._id)) && ground.open === false
  ));
  return (
    <Container>
      <Row><Col><h3>My Grounds</h3></Col></Row>
      <Row>{myGrounds.map(ground => {
        let teamSide = getTeamSide(ground, me);
        return (
          <Col md={5}>
            <Card className="text-center">
              <div className="match-logos">
                <div style={myTeam(ground, 'teamA', me)} className="match-logo">
                  <img src={`${HOST}/api/logos/${singleMatch.teamA.token.logo}`}/>
                </div>
                <div style={myTeam(ground, 'teamB', me)} className="match-logo">
                  <img src={`${HOST}/api/logos/${singleMatch.teamB.token.logo}`}/>
                </div>
              </div>
              <CardBody>
                <CardTitle>{singleMatch.teamA.token.symbol} <span>vs</span> {singleMatch.teamB.token.symbol}</CardTitle>
                <CardText>{ground.percent}%</CardText>
                {renderRewardButton(ground, teamSide, withdrawReward, singleMatch._id)}
                {ground.canWithdraw && !ground.withdrawTokens[teamSide] && <Button
                  onClick={() => withdrawTeam(ground._id, singleMatch._id)}
                >Withdraw Team</Button>}
              </CardBody>
            </Card>
          </Col>
        )
      })}</Row>
    </Container>
  )
}


const myTeam = (ground, teamSide, me) => {
  return ground[teamSide].user.toString() === me._id ? {background: "aliceblue"} : {};
}

const getTeamSide = (ground, me) => {
  return ground.teamA.user.toString() === me._id ? "teamA" : "teamB";
}

const renderRewardButton = (ground, teamSide, withdrawReward, matchId) => {
  if (
    ground.canWithdraw && ground.result
    && (
      (ground.result === 'draw' && !ground.drawRewards[teamSide].received)
      || (ground.result === teamSide && !ground.reward.received)
    )
  ) {
    return (
      <Button
        onClick={() => withdrawReward(ground._id, matchId)}
        style={{ marginRight: 20 }}
      >Withdraw Reward</Button>
    )
  }
}

