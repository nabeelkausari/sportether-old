import React, { Component } from 'react';

import GroundsContainer from '../../containers/grounds';
import Layout from '../layouts';
import GroundsList from './List';
import MyGrounds from './MyGrounds';
import {Card, CardBody, CardTitle, Button} from 'reactstrap';
import {HOST} from "../../config/constants"

class Home extends Component {
  componentWillMount() {
    let matchId = this.props.match.params.match;
    this.props.getMatch(matchId)
    this.props.getGrounds(matchId)
  }

  render() {
    let { singleMatch, createGround, match } = this.props;
    return (
      <Layout>
        <h1>Grounds page</h1>
        { singleMatch && <Card className="text-center">
          <div className="match-logos">
            <div className="match-logo"><img src={`${HOST}/api/logos/${singleMatch.teamA.token.logo}`}/></div>
            <div className="match-logo"><img src={`${HOST}/api/logos/${singleMatch.teamB.token.logo}`}/></div>
          </div>
          <CardBody>
            <CardTitle>{singleMatch.teamA.token.symbol} <span>vs</span> {singleMatch.teamB.token.symbol}</CardTitle>
            {/*<CardSubtitle>Card subtitle</CardSubtitle>*/}
            {/*<CardText>{match.description}</CardText>*/}
          </CardBody>
        </Card>}
        <MyGrounds {...this.props} />
        <GroundsList {...this.props} />
        { singleMatch && singleMatch.active && <Button onClick={() => createGround(match.params.match)}>Create Ground</Button>}
      </Layout>
    )
  }
}

export default GroundsContainer(Home)
