import axios from 'axios';
import {reset} from 'redux-form';

import {
  BUY_TOKENS, BUY_TOKENS_ERROR, BUY_TOKENS_SUCCESS, GET_BALANCE, GET_BALANCE_ERROR,
  GET_BALANCE_SUCCESS, GET_TRANSACTIONS, GET_TRANSACTIONS_ERROR, GET_TRANSACTIONS_SUCCESS, WITHDRAW_ETH,
  WITHDRAW_ETH_ERROR, WITHDRAW_ETH_SUCCESS,
  SYNC_WALLET, SYNC_WALLET_SUCCESS, SYNC_WALLET_ERROR, SYNC_WALLET_COMPLETE
} from "./types"
import {getAuthHeaders, HOST} from "../../config/constants";

export const buyTokens = ({ unit, tokenId, redirect }) => dispatch => {
  dispatch({ type: BUY_TOKENS });
  axios.post(`${HOST}/api/tokens/buy`, { unit, tokenId }, getAuthHeaders())
    .then(res => {
      fetchData(BUY_TOKENS_SUCCESS, dispatch, res.data);
      dispatch(reset('buyTeam'));
      redirect();
    })
    .catch(err => fetchData(BUY_TOKENS_ERROR, dispatch, err.response))
}

export const requireSync = () => dispatch => {
  axios.get(`${HOST}/api/wallets/requireSync`, getAuthHeaders())
    .then(res => {
      if (res.data.requireSync && !res.data.syncLock) dispatch(syncWallet());
      if (res.data.syncLock) dispatch({ type: SYNC_WALLET })
      if (!res.data.requireSync && !res.data.syncLock) dispatch({ type: SYNC_WALLET_COMPLETE })
    })
}

export const syncWallet = () => dispatch => {
  dispatch({ type: SYNC_WALLET });
  axios.put(`${HOST}/api/wallets/sync`, {}, getAuthHeaders())
    .then(res => fetchData(SYNC_WALLET_SUCCESS, dispatch, res.data))
    .catch(err => fetchData(SYNC_WALLET_ERROR, dispatch, err))
}

export const withdrawEth = ({ units, address }) => dispatch => {
  dispatch({ type: WITHDRAW_ETH });
  axios.put(`${HOST}/api/wallets/withdrawEth`, {units, address}, getAuthHeaders())
    .then(res => fetchData(WITHDRAW_ETH_SUCCESS, dispatch, res.data))
    .catch(err => fetchData(WITHDRAW_ETH_ERROR, dispatch, err.response.data))
}

export const getBalance = () => dispatch => {
  dispatch({ type: GET_BALANCE });
  axios.get(`${HOST}/api/wallets`, getAuthHeaders())
    .then(res => {
      dispatch({ type: GET_BALANCE_SUCCESS, payload: res.data })
    })
    .catch(err => {
      dispatch({ type: GET_BALANCE_ERROR, payload: err })
    })
}

export const getTransactions = () => dispatch => {
  dispatch({ type: GET_TRANSACTIONS });
  axios.get(`${HOST}/api/transactions`, getAuthHeaders())
    .then(res => {
      dispatch({ type: GET_TRANSACTIONS_SUCCESS, payload: res.data })
    })
    .catch(err => {
      dispatch({ type: GET_TRANSACTIONS_ERROR, payload: err })
    })
}

const fetchData = (type, dispatch, payload) => {
  dispatch({ type, payload })
  dispatch(getBalance());
  dispatch(getTransactions());
}
