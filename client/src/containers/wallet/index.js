import { connect } from 'react-redux';

import { getBalance, syncWallet, withdrawEth, getTransactions } from './actions';

const mapStateToProps = state => {
  return {
    ethBalance: state.wallet.ethBalance,
    spoBalance: state.wallet.spoBalance,
    spoReserved: state.wallet.spoReserved,
    ethAddress: state.wallet.ethAddress,
    transactions: state.wallet.transactions,
    tokens: state.wallet.tokens,
    syncWalletLoading: state.wallet.syncWalletLoading,
    syncWalletMessage: state.wallet.syncWalletMessage,
    syncWalletUpdated: state.wallet.syncWalletUpdated,
    withdrawEthLoading: state.wallet.withdrawEthLoading,
    withdrawEthError: state.wallet.withdrawEthError,
    withdrawEthTxn: state.wallet.withdrawEthTxn
  }
}

export default connect(mapStateToProps, {
  getBalance, syncWallet, withdrawEth, getTransactions
})
