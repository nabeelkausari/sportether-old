import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {getBalance, withdrawEth} from './actions';

const validate = formProps => {
  const errors = {};

  if (!formProps.units) errors.units = 'Please enter the amount of ethers'
  if (!formProps.address) errors.address = 'Please enter a proper ETH wallet address'
  return errors;
}

export default Comp => {
  const withdrawEthComp = connect(null, { getBalance, withdrawEth })(Comp)

  return reduxForm({
    form: 'withdrawEth',
    validate
  })(withdrawEthComp);
}
