import {
  BUY_TOKENS, BUY_TOKENS_ERROR, BUY_TOKENS_SUCCESS,
  SYNC_WALLET, SYNC_WALLET_SUCCESS, SYNC_WALLET_ERROR,
  GET_BALANCE, GET_BALANCE_ERROR,
  GET_BALANCE_SUCCESS, GET_TRANSACTIONS, GET_TRANSACTIONS_ERROR, GET_TRANSACTIONS_SUCCESS, WITHDRAW_ETH,
  WITHDRAW_ETH_ERROR,
  WITHDRAW_ETH_SUCCESS, SYNC_WALLET_COMPLETE
} from "./types"
import { weiToEth } from "../../config/utils";

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_BALANCE:
      return { ...state, balanceLoading: true };
    case GET_BALANCE_SUCCESS:
    case GET_BALANCE_ERROR:
      let { ethBalance, tokens, ethAddress,spoBalance, spoReserved } = payload;
      return { ...state,
        balanceLoading: false,
        ethBalance: weiToEth(ethBalance),
        spoBalance,
        spoReserved,
        ethAddress,
        tokens
      }

    case SYNC_WALLET:
      return { ...state, syncWalletLoading: true, syncWalletMessage: '', syncWalletUpdated: null };
    case SYNC_WALLET_SUCCESS:
      return { ...state, syncWalletLoading: false, syncWalletMessage: payload.message, syncWalletUpdated: payload.updated }
    case SYNC_WALLET_ERROR:
      return { ...state, syncWalletLoading: false, syncWalletMessage: payload }
    case SYNC_WALLET_COMPLETE:
      return { ...state, syncWalletLoading: false }

    case WITHDRAW_ETH:
      return { ...state, withdrawEthLoading: true, withdrawEthError: null, withdrawEthTxn: '' };
    case WITHDRAW_ETH_SUCCESS:
      return { ...state, withdrawEthLoading: false, withdrawEthTxn: payload.transactionHash }
    case WITHDRAW_ETH_ERROR:
      return { ...state, withdrawEthLoading: false, withdrawEthError: payload.data.error }

    case GET_TRANSACTIONS:
      return { ...state, transactionsLoading: true };
    case GET_TRANSACTIONS_SUCCESS:
    case GET_TRANSACTIONS_ERROR:
      return { ...state, transactionsLoading: false, transactions: payload }

    case BUY_TOKENS:
      return { ...state, buyTokensLoading: true, boughtTokens: null, buyTokensError: null };
    case BUY_TOKENS_SUCCESS:
      return { ...state, buyTokensLoading: false, boughtTokens: payload }
    case BUY_TOKENS_ERROR:
      return { ...state, buyTokensLoading: false, buyTokensError: payload.data.error }

    default:
      return state;
  }
}
