import axios from 'axios';
import {HOST} from "../../config/constants"
import { GET_SPORTS, GET_SPORTS_ERROR, GET_SPORTS_SUCCESS } from "./types"

export const getSports = () => dispatch => {
  dispatch({ type: GET_SPORTS })
  axios.get(HOST + '/api/sports')
    .then(res => dispatch({ type: GET_SPORTS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_SPORTS_ERROR, payload: err }))
}
