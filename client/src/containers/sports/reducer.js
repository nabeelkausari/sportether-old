import { GET_SPORTS, GET_SPORTS_ERROR, GET_SPORTS_SUCCESS } from "./types"

export default (state = {}, { type, payload}) => {
  switch (type) {
    case GET_SPORTS:
      return { ...state, sportsLoading: true, sportsErr: null };
    case GET_SPORTS_SUCCESS:
      return { ...state, sportsLoading: false, list: payload };
    case GET_SPORTS_ERROR:
      return { ...state, sportsLoading: false, sportsErr: payload };

    default:
      return state
  }
}
