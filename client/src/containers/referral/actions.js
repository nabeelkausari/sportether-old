import axios from 'axios';

import {GET_REFERRAL_LIST, GET_REFERRAL_LIST_ERROR, GET_REFERRAL_LIST_SUCCESS} from "./types"
import {getAuthHeaders, HOST} from "../../config/constants";

export const getReferrals = () => dispatch => {
  dispatch({ type: GET_REFERRAL_LIST });
  axios.get(`${HOST}/api/referrals`, getAuthHeaders())
    .then(res => dispatch({ type: GET_REFERRAL_LIST_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_REFERRAL_LIST_ERROR, payload: err }))
}

