import { connect } from 'react-redux';

import {getReferrals} from './actions';

const mapStateToProps = state => {
  return {
    referralList: state.referral.list,
  }
}

export default connect(mapStateToProps, { getReferrals })
