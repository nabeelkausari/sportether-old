import {GET_REFERRAL_LIST, GET_REFERRAL_LIST_ERROR, GET_REFERRAL_LIST_SUCCESS} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_REFERRAL_LIST:
      return { ...state, referralListLoading: true, referralListError: null };
    case GET_REFERRAL_LIST_SUCCESS:
      return { ...state, referralListLoading: false, list: payload }
    case GET_REFERRAL_LIST_ERROR:
      return { ...state, referralListLoading: false, referralListError: payload.data }

    default:
      return state;
  }
}
