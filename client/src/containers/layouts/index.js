import { connect } from 'react-redux';

import { logoutUser, isAuthenticated, redirect, clearAuthErrors } from "../auth/actions";
import { requireSync } from "../wallet/actions";

const mapStateToProps = state => {
  return {
    authLoading: state.auth.authLoading,
    authenticated: state.auth.authenticated,
    user: state.auth.user
  }
}

export default connect(mapStateToProps, {logoutUser, isAuthenticated, redirect, clearAuthErrors, requireSync })
