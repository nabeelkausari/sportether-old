import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {claimBonus, getClaims, confirmBonus} from './actions';
import { redirect } from "../auth/actions";

const validate = formProps => {
  const errors = {};

  if (!formProps.facebook) errors.facebook = 'Please enter you facebook username to verify'
  if (!formProps.twitter) errors.twitter = 'Please enter you twitter username to verify'
  if (!formProps.telegram) errors.telegram = 'Please enter you telegram username to verify'
  return errors;
}

const mapStateToProps = state => {
  return {
    claimBonusSuccess: state.bonus.claimBonusSuccess,
    claims: state.bonus.claims,
  }
}

export default Comp => {
  const bonusClaim = connect(mapStateToProps, { claimBonus, redirect, getClaims, confirmBonus })(Comp)

  return reduxForm({
    form: 'bonusClaim',
    validate
  })(bonusClaim);
}
