import {
  GET_CLAIMS, GET_CLAIMS_ERROR, GET_CLAIMS_SUCCESS,
  SAVE_BONUS_CLAIM, SAVE_BONUS_CLAIM_ERROR, SAVE_BONUS_CLAIM_SUCCESS
} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case SAVE_BONUS_CLAIM:
      return { ...state, claimBonusLoading: true, claimBonusError: null, claimBonusSuccess: null };
    case SAVE_BONUS_CLAIM_SUCCESS:
      return { ...state, claimBonusLoading: false, claimBonusSuccess: true }
    case SAVE_BONUS_CLAIM_ERROR:
      return { ...state, claimBonusLoading: false, claimBonusError: payload.data, claimBonusSuccess: false }

    case GET_CLAIMS:
      return { ...state, getClaimsLoading: true, getClaimsError: null };
    case GET_CLAIMS_SUCCESS:
      return { ...state, getClaimsLoading: false, claims: payload }
    case GET_CLAIMS_ERROR:
      return { ...state, getClaimsLoading: false, getClaimsError: payload.data }

    default:
      return state;
  }
}
