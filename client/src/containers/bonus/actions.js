import axios from 'axios';

import {
  CONFIRM_BONUS_CLAIM, CONFIRM_BONUS_CLAIM_ERROR, CONFIRM_BONUS_CLAIM_SUCCESS, GET_CLAIMS, GET_CLAIMS_ERROR,
  GET_CLAIMS_SUCCESS,
  SAVE_BONUS_CLAIM,
  SAVE_BONUS_CLAIM_ERROR,
  SAVE_BONUS_CLAIM_SUCCESS
} from "./types"
import {getAuthHeaders, HOST} from "../../config/constants";

export const claimBonus = ({ telegram, facebook, twitter, redirect }) => dispatch => {
  dispatch({ type: SAVE_BONUS_CLAIM });
  axios.post(`${HOST}/api/claimBonus`, { telegram, facebook, twitter }, getAuthHeaders())
    .then(res => {
      dispatch({ type: SAVE_BONUS_CLAIM_SUCCESS, payload: res.data });
      redirect();
    })
    .catch(err => dispatch({ type: SAVE_BONUS_CLAIM_ERROR, payload: err }))
}

export const confirmBonus = ({ userId }) => dispatch => {
  dispatch({ type: CONFIRM_BONUS_CLAIM });
  axios.post(`${HOST}/api/confirmBonus`, { userId }, getAuthHeaders())
    .then(res => dispatch({ type: CONFIRM_BONUS_CLAIM_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: CONFIRM_BONUS_CLAIM_ERROR, payload: err }))
}

export const getClaims = () => dispatch => {
  dispatch({ type: GET_CLAIMS });
  axios.get(`${HOST}/api/claims`, getAuthHeaders())
    .then(res => dispatch({ type: GET_CLAIMS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_CLAIMS_ERROR, payload: err }))
}

