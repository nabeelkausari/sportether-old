import { connect } from 'react-redux';
import { getTeams, getSelectedTeams } from './actions';

const mapStateTopProps = state => {
  return {
    teams: state.teams.teams,
    selectedTeams: state.teams.selectedTeams,
    selectedTeamsLoading: state.teams.selectedTeamsLoading
  }
}

export default connect(mapStateTopProps, { getTeams, getSelectedTeams })
