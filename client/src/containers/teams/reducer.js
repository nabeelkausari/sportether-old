import {
  GET_TEAMS, GET_TEAMS_ERROR, GET_TEAMS_SUCCESS,
  GET_SELECTED_TEAMS, GET_SELECTED_TEAMS_ERROR, GET_SELECTED_TEAMS_SUCCESS
} from "./types"

export default (state = {}, { type, payload}) => {
  switch (type) {
    case GET_TEAMS:
      return { ...state, teamsLoading: true, teamsErr: null };
    case GET_TEAMS_SUCCESS:
      return { ...state, teamsLoading: false, teams: payload };
    case GET_TEAMS_ERROR:
      return { ...state, teamsLoading: false, teamsErr: payload };

    case GET_SELECTED_TEAMS:
      return { ...state, selectedTeamsLoading: true, selectedTeamsErr: null };
    case GET_SELECTED_TEAMS_SUCCESS:
      return { ...state, selectedTeamsLoading: false, selectedTeams: payload };
    case GET_SELECTED_TEAMS_ERROR:
      return { ...state, selectedTeamsLoading: false, selectedTeamsErr: payload };

    default:
      return state
  }
}
