import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

const validate = formProps => {
  const errors = {};

  if (!formProps.unit) errors.unit = 'Please enter the number of team tokens you want to buy'
  return errors;
}

export default Comp => {
  const buyTeamComp = connect(null)(Comp)

  return reduxForm({
    form: 'buyTeam',
    validate
  })(buyTeamComp);
}
