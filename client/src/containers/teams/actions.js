import axios from 'axios';
import {HOST} from "../../config/constants"
import { GET_TEAMS, GET_TEAMS_ERROR, GET_TEAMS_SUCCESS, GET_SELECTED_TEAMS, GET_SELECTED_TEAMS_ERROR, GET_SELECTED_TEAMS_SUCCESS } from "./types"

export const getTeams = sport => dispatch => {
  dispatch({ type: GET_TEAMS })
  axios.get(HOST + '/api/teams?sport=' + sport)
    .then(res => dispatch({ type: GET_TEAMS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_TEAMS_ERROR, payload: err }))
}

export const getSelectedTeams = () => dispatch => {
  dispatch({ type: GET_SELECTED_TEAMS })
  let football = ['SFRMA', 'SFFCB', 'SFARG'];
  let cricket = ['SCAUS', 'SCIND', 'SCENG']
  axios.get(`${HOST}/api/selected?football=${football}&cricket=${cricket}`)
    .then(res => dispatch({ type: GET_SELECTED_TEAMS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_SELECTED_TEAMS_ERROR, payload: err }))
}
