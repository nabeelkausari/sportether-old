import { connect } from 'react-redux';
import { getTopMatches, getTopTeams } from './actions';

function mapStateTopProps(state) {
  return {
    topMatches: state.home.topMatches,
    topMatchesLoading: state.home.topMatchesLoading,
    topTeams: state.home.topTeams,
    topTeamsLoading: state.home.topTeamsLoading
  }
}

export default connect(mapStateTopProps, { getTopMatches, getTopTeams })
