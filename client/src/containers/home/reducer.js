import {
  GET_TOP_MATCHES, GET_TOP_MATCHES_ERROR, GET_TOP_MATCHES_SUCCESS, GET_TOP_TEAMS, GET_TOP_TEAMS_ERROR,
  GET_TOP_TEAMS_SUCCESS
} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case GET_TOP_TEAMS:
      return { ...state, topTeamsLoading: true };
    case GET_TOP_TEAMS_SUCCESS:
    case GET_TOP_TEAMS_ERROR:
      return { ...state, topTeamsLoading: false, topTeams: payload };

    case GET_TOP_MATCHES:
      return { ...state, topMatchesLoading: true };
    case GET_TOP_MATCHES_SUCCESS:
    case GET_TOP_MATCHES_ERROR:
      return { ...state, topMatchesLoading: false, topMatches: payload };
    default:
      return state
  }
}
