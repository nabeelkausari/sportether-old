import axios from 'axios';

import { HOST } from '../../config/constants';
import {
  GET_TOP_MATCHES, GET_TOP_TEAMS, GET_TOP_TEAMS_SUCCESS, GET_TOP_TEAMS_ERROR, GET_TOP_MATCHES_ERROR,
  GET_TOP_MATCHES_SUCCESS
} from "./types"

export const getTopTeams = () => {
  return dispatch => {
    dispatch({ type: GET_TOP_TEAMS })
    axios.get(HOST + '/api/topTeams?limit=10')
      .then(res => dispatch({ type: GET_TOP_TEAMS_SUCCESS, payload: res.data }))
      .catch(err => dispatch({ type: GET_TOP_TEAMS_ERROR, payload: err }))
  }
}

export const getTopMatches = () => {
  return dispatch => {
    dispatch({ type: GET_TOP_MATCHES })
    axios.get(HOST + '/api/matches?sportId=5aa6661a1a04c64fb318e9a1&limit=6')
      .then(res => dispatch({ type: GET_TOP_MATCHES_SUCCESS, payload: res.data }))
      .catch(err => dispatch({ type: GET_TOP_MATCHES_ERROR, payload: err }))
  }
}
