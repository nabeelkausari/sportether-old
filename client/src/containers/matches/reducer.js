import {
  GET_ACTIVE_MATCHES, GET_ACTIVE_MATCHES_ERROR, GET_ACTIVE_MATCHES_SUCCESS, GET_COMPLETED_MATCHES,
  GET_COMPLETED_MATCHES_ERROR,
  GET_COMPLETED_MATCHES_SUCCESS,
  GET_MATCHES, GET_MATCHES_ERROR, GET_MATCHES_SUCCESS
} from "./types"

export default (state = {}, { type, payload}) => {
  switch (type) {
    case GET_MATCHES:
      return { ...state, matchesLoading: true, matchesErr: null };
    case GET_MATCHES_SUCCESS:
      return { ...state, matchesLoading: false, matches: payload };
    case GET_MATCHES_ERROR:
      return { ...state, matchesLoading: false, matchesErr: payload };

    case GET_ACTIVE_MATCHES:
      return { ...state, activeMatchesLoading: true, activeMatchesErr: null };
    case GET_ACTIVE_MATCHES_SUCCESS:
      return { ...state, activeMatchesLoading: false, active: payload };
    case GET_ACTIVE_MATCHES_ERROR:
      return { ...state, activeMatchesLoading: false, activeMatchesErr: payload };

    case GET_COMPLETED_MATCHES:
      return { ...state, completedMatchesLoading: true, completedMatchesErr: null };
    case GET_COMPLETED_MATCHES_SUCCESS:
      return { ...state, completedMatchesLoading: false, completed: payload };
    case GET_COMPLETED_MATCHES_ERROR:
      return { ...state, completedMatchesLoading: false, completedMatchesErr: payload };


    default:
      return state
  }
}
