import axios from 'axios';
import {HOST} from "../../config/constants"
import {
  GET_ACTIVE_MATCHES, GET_ACTIVE_MATCHES_ERROR, GET_ACTIVE_MATCHES_SUCCESS, GET_COMPLETED_MATCHES,
  GET_COMPLETED_MATCHES_ERROR,
  GET_COMPLETED_MATCHES_SUCCESS,
  GET_MATCHES, GET_MATCHES_ERROR, GET_MATCHES_SUCCESS
} from "./types"


export const getMatches = sport => dispatch => {
  dispatch({ type: GET_MATCHES })
  axios.get(HOST + '/api/matches?sport=' + sport)
    .then(res => dispatch({ type: GET_MATCHES_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_MATCHES_ERROR, payload: err }))
}

export const getActiveMatches = sport => dispatch => {
  dispatch({ type: GET_ACTIVE_MATCHES })
  axios.get(`${HOST}/api/matches?sport=${sport}&active=true`)
    .then(res => dispatch({ type: GET_ACTIVE_MATCHES_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_ACTIVE_MATCHES_ERROR, payload: err }))
}

export const getCompletedMatches = sport => dispatch => {
  dispatch({ type: GET_COMPLETED_MATCHES })
  axios.get(`${HOST}/api/matches?sport=${sport}&complete=true`)
    .then(res => dispatch({ type: GET_COMPLETED_MATCHES_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_COMPLETED_MATCHES_ERROR, payload: err }))
}
