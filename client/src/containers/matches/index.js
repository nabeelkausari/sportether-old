import { connect } from 'react-redux';
import { getMatches, getActiveMatches, getCompletedMatches } from './actions';

const mapStateTopProps = state => {
  return {
    matches: state.matches.matches,
    activeMatches: state.matches.active,
    completedMatches: state.matches.completed
  }
}

export default connect(mapStateTopProps, { getMatches, getActiveMatches, getCompletedMatches })
