export const GET_MATCHES = "GET_MATCHES";
export const GET_MATCHES_SUCCESS = "GET_MATCHES_SUCCESS";
export const GET_MATCHES_ERROR = "GET_MATCHES_ERROR";

export const GET_ACTIVE_MATCHES = "GET_ACTIVE_MATCHES";
export const GET_ACTIVE_MATCHES_SUCCESS = "GET_ACTIVE_MATCHES_SUCCESS";
export const GET_ACTIVE_MATCHES_ERROR = "GET_ACTIVE_MATCHES_ERROR";

export const GET_COMPLETED_MATCHES = "GET_COMPLETED_MATCHES";
export const GET_COMPLETED_MATCHES_SUCCESS = "GET_COMPLETED_MATCHES_SUCCESS";
export const GET_COMPLETED_MATCHES_ERROR = "GET_COMPLETED_MATCHES_ERROR";
