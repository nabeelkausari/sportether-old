import { connect } from 'react-redux';
import { getGrounds, getMatch, createGround, enterGround, exitGround, withdrawReward, withdrawTeam } from './actions';

const mapStateTopProps = state => {
  return {
    grounds: state.grounds.grounds,
    singleMatch: state.grounds.match,
    me: state.auth.user
  }
}

export default connect(mapStateTopProps, { getGrounds, getMatch, createGround, enterGround, exitGround, withdrawReward, withdrawTeam })
