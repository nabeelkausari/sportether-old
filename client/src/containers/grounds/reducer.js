import {
  CREATE_GROUND, CREATE_GROUND_ERROR, CREATE_GROUND_SUCCESS, ENTER_GROUND, ENTER_GROUND_ERROR, ENTER_GROUND_SUCCESS,
  GET_GROUNDS, GET_GROUNDS_ERROR, GET_GROUNDS_SUCCESS, GET_MATCH, GET_MATCH_ERROR,
  GET_MATCH_SUCCESS
} from "./types"

export default (state = {}, { type, payload}) => {
  switch (type) {
    case GET_GROUNDS:
      return { ...state, groundsLoading: true, groundsErr: null };
    case GET_GROUNDS_SUCCESS:
      return { ...state, groundsLoading: false, grounds: payload };
    case GET_GROUNDS_ERROR:
      return { ...state, groundsLoading: false, groundsErr: payload };

    case GET_MATCH:
      return { ...state, matchLoading: true, matchErr: null };
    case GET_MATCH_SUCCESS:
      return { ...state, matchLoading: false, match: payload };
    case GET_MATCH_ERROR:
      return { ...state, matchLoading: false, matchErr: payload };

    case ENTER_GROUND:
      return { ...state, enterGroundLoading: true, enterGroundErr: null };
    case ENTER_GROUND_SUCCESS:
      return { ...state, enterGroundLoading: false, enterGround: payload };
    case ENTER_GROUND_ERROR:
      return { ...state, enterGroundLoading: false, enterGroundErr: payload };

    default:
      return state
  }
}
