import axios from 'axios';
import {getAuthHeaders, HOST} from "../../config/constants"
import {
  GET_GROUNDS, GET_GROUNDS_ERROR, GET_GROUNDS_SUCCESS, GET_MATCH, GET_MATCH_ERROR, GET_MATCH_SUCCESS,
  CREATE_GROUND, CREATE_GROUND_SUCCESS, CREATE_GROUND_ERROR, ENTER_GROUND_SUCCESS, ENTER_GROUND_ERROR, ENTER_GROUND
} from "./types"


export const getGrounds = matchId => dispatch => {
  dispatch({ type: GET_GROUNDS })
  axios.get(HOST + '/api/grounds?matchId=' + matchId)
    .then(res => dispatch({ type: GET_GROUNDS_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_GROUNDS_ERROR, payload: err }))
}

export const getMatch = matchId => dispatch => {
  dispatch({ type: GET_MATCH });
  axios.get(HOST + '/api/matches/' + matchId)
    .then(res => dispatch({ type: GET_MATCH_SUCCESS, payload: res.data }))
    .catch(err => dispatch({ type: GET_MATCH_ERROR, payload: err }))
}

export const createGround = matchId => dispatch => {
  axios.post(HOST + '/api/grounds', { matchId, percent: 10 }, getAuthHeaders())
    .then(res => dispatch(getGrounds(matchId)))
    .catch(err => dispatch(getGrounds(matchId)))
}

export const enterGround = (groundId, teamId, matchId) => dispatch => {
  dispatch({ type: ENTER_GROUND })
  axios.put(HOST + '/api/grounds/enter', { groundId, teamId }, getAuthHeaders())
    .then(res => handleDispatch(ENTER_GROUND_SUCCESS, dispatch, matchId))
    .catch(err => handleDispatch(ENTER_GROUND_ERROR, dispatch, matchId))
}

export const exitGround = (groundId, matchId) => dispatch => {
  axios.put(HOST + '/api/grounds/exit', { groundId }, getAuthHeaders())
    .then(res => dispatch(getGrounds(matchId)))
    .catch(err => dispatch(getGrounds(matchId)))
}

export const withdrawTeam = (groundId, matchId) => dispatch => {
  axios.put(HOST + '/api/grounds/withdrawTeam', { groundId }, getAuthHeaders())
    .then(res => dispatch(getGrounds(matchId)))
    .catch(err => dispatch(getGrounds(matchId)))
}

export const withdrawReward = (groundId, matchId) => dispatch => {
  axios.put(HOST + '/api/grounds/withdrawReward', { groundId }, getAuthHeaders())
    .then(res => dispatch(getGrounds(matchId)))
    .catch(err => dispatch(getGrounds(matchId)))
}

const handleDispatch = (type, dispatch, matchId) => {
  dispatch({ type });
  dispatch(getGrounds(matchId))
}
