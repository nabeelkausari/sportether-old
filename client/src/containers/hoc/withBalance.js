import React, { Component } from 'react';
import { connect } from 'react-redux';

import { redirect, isAuthenticated } from "../auth/actions";
import { getBalance } from "../wallet/actions";


export default ComposedComponent => {
  class WithAuth extends Component {

    componentWillMount() {
      this.props.getBalance();
      this.props.isAuthenticated({ redirect: this.props.redirect })
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      authLoading: state.auth.authLoading,
      ethBalance: state.wallet.ethBalance,
      tokens: state.wallet.tokens
    }
  }

  return connect(mapStateToProps, { redirect, isAuthenticated, getBalance })(WithAuth)
}

