import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getSports } from "../sports/actions";

export default ComposedComponent => {
  class WithSports extends Component {

    componentWillMount() {
      this.props.getSports()
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      sports: state.sports.list
    }
  }

  return connect( mapStateToProps, { getSports })(WithSports)
}

