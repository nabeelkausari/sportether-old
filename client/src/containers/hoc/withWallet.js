import React, { Component } from 'react';
import { connect } from 'react-redux';

import { redirect, isAuthenticated } from "../auth/actions";
import { getBalance, buyTokens, getTransactions, withdrawEth, syncWallet } from "../wallet/actions";

export default ComposedComponent => {
  class WithAuth extends Component {

    componentWillMount() {
      this.props.getBalance();
      this.props.isAuthenticated({ redirect: this.props.redirect })
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      authLoading: state.auth.authLoading,
      ethBalance: state.wallet.ethBalance,
      tokens: state.wallet.tokens,
      boughtTokens: state.wallet.boughtTokens,
      buyTokensLoading: state.wallet.buyTokensLoading,
      buyTokensError: state.wallet.buyTokensError,
    }
  }

  return connect(mapStateToProps, {
    redirect, isAuthenticated, getBalance,
    buyTokens, getTransactions, withdrawEth, syncWallet
  })(WithAuth)
}

