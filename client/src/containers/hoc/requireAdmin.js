import React, { Component } from 'react';
import { connect } from 'react-redux';

import { redirect } from "../auth/actions";

export default ComposedComponent => {
  class Authentication extends Component {

    componentWillMount() {
      if (
        (this.props.authLoading === false && this.props.authenticated === false) ||
        (this.props.user && this.props.user.isAdmin === false)
      ) {
        this.props.redirect();
      }
    }

    componentWillUpdate(nextProps) {
      if((!this.props.authLoading === false && nextProps.authenticated === false) ||
        (!this.props.authLoading === false && nextProps.user && nextProps.user.isAdmin === false)
      ) {
        this.props.redirect();
      }
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      authLoading: state.auth.authLoading,
      user: state.auth.user
    }
  }

  return connect(mapStateToProps, { redirect })(Authentication)
}

