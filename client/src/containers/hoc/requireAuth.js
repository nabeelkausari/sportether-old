import React, { Component } from 'react';
import { connect } from 'react-redux';

import { redirect } from "../auth/actions";

export default ComposedComponent => {
  class Authentication extends Component {

    componentWillMount() {
      if (this.props.authLoading === false && this.props.authenticated === false) {
        this.props.redirect("/sign-in");
      }
    }

    componentWillUpdate(nextProps) {
      if(!this.props.authLoading === false && nextProps.authenticated === false) {
        this.props.redirect("/sign-in");
      }
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      authLoading: state.auth.authLoading
    }
  }

  return connect(mapStateToProps, { redirect })(Authentication)
}

