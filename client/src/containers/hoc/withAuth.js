import React, { Component } from 'react';
import { connect } from 'react-redux';

import { redirect, isAuthenticated } from "../auth/actions";

export default ComposedComponent => {
  class WithAuth extends Component {

    componentWillMount() {
      this.props.isAuthenticated({ redirect: this.props.redirect })
    }

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      authLoading: state.auth.authLoading
    }
  }

  return connect(mapStateToProps, { redirect, isAuthenticated })(WithAuth)
}

