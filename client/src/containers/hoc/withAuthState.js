import React, { Component } from 'react';
import { connect } from 'react-redux';

import { redirect } from "../auth/actions";

export default ComposedComponent => {
  class WithAuthState extends Component {

    render() {
      return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps(state) {
    return {
      authenticated: state.auth.authenticated,
      authLoading: state.auth.authLoading,
      user: state.auth.user
    }
  }

  return connect(mapStateToProps, { redirect })(WithAuthState)
}

