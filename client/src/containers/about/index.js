import React from 'react';
import Layout from '../../components/layouts'

const About = () => (
  <Layout>
    <h1>About Page</h1>
    <p>Did you get here via Redux?</p>
  </Layout>
);

export default About;
