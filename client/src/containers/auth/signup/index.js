import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

import { signupUser, redirect } from '../actions';
import { getReferralId } from "../../../config/utils";

const validate = formProps => {
  const errors = {};

  if (!formProps.email) errors.email = 'Please enter an email'
  if (!formProps.username) errors.username = 'Please enter a username'
  if (!formProps.password) errors.password = 'Please enter a password'
  if (!formProps.confirmPassword) errors.confirmPassword = 'Please enter a password confirmation'

  if (formProps.password !== formProps.confirmPassword) {
    errors.password = 'Passwords must match'
  }

  if (formProps.username && (formProps.username.trim().indexOf(" ") >= 0)) {
    errors.username = 'Username cannot have spaces in between'
  }
  return errors;
}

const mapStateToProps = state => {
  return {
    signupError: state.auth.signupError,
    signupLoading: state.auth.signupLoading,
    signupSuccess: state.auth.signupSuccess,
    initialValues: { referralId: getReferralId() },
  }
}

export default Comp => {
  const signup = reduxForm({
    form: 'signup',
    validate
  })(Comp);

  return connect(mapStateToProps, { signupUser, redirect })(signup);
}
