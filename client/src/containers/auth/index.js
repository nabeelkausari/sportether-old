import { connect } from 'react-redux';
import { logoutUser, clearAuthErrors } from './actions';

export default connect(null, { logoutUser, clearAuthErrors })
