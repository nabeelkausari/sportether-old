import axios from 'axios';
import { push } from 'react-router-redux';

import { HOST } from "../../config/constants";
import {
  SIGNUP_USER, SIGNUP_USER_ERROR, SIGNUP_USER_SUCCESS,
  AUTH_USER, AUTH_USER_ERROR, AUTH_USER_SUCCESS, LOGOUT_USER, CLEAR_AUTH_ERRORS
} from "./types"

export const clearAuthErrors = () => ({ type: CLEAR_AUTH_ERRORS });

export const signupUser = ({ email, password, username, referralUsername, redirect, preRedirect }) => dispatch => {
  dispatch({ type: SIGNUP_USER })
  if (preRedirect) preRedirect();
  axios.post(`${HOST}/api/signUp`, { email, password, username, referralUsername })
    .then(res => {
      dispatch({ type: SIGNUP_USER_SUCCESS, payload: res.data.user })
      removeReferralId();
      if (!preRedirect) redirect();
    })
    .catch(err => {
      dispatch({
        type: SIGNUP_USER_ERROR,
        payload: err.response && err.response.data && err.response.data.error
      })
    })
}

export const signinUser = ({ email, password, redirect }) => dispatch => {
  dispatch({ type: AUTH_USER })
  axios.post(`${HOST}/api/signIn`, { email, password })
    .then(res => doLogin(dispatch, res, redirect))
    .catch(err => dispatch(authError(err.response && err.response.data)))
}

const doLogin = (dispatch, res, redirect) => {
  dispatch({ type: AUTH_USER_SUCCESS, payload: res.data.user })
  localStorage.setItem('token', res.data.jwt)
  redirect(res.data.user);
}

export const logoutUser = ({ redirect }) => {
  localStorage.removeItem('token');
  redirect();
  return { type: LOGOUT_USER }
}

export const redirect = path => push(path || '/')

export const isAuthenticated = ({ redirect }) => dispatch => {
  dispatch({ type: AUTH_USER })
  let jwt = localStorage.getItem('token');
  axios.post(`${HOST}/api/getMe`, { jwt })
    .then(res => {
      if (!res.data.user) {
        dispatch(logoutUser({ redirect }))
      } else {
        dispatch({ type: AUTH_USER_SUCCESS, payload: res.data.user })
      }
    })
    .catch(() => dispatch(authError()))
}

export const authError = error => {
  return { type: AUTH_USER_ERROR, payload: error }
}

export const removeReferralId = () => {
  return localStorage.removeItem('referralId');
}
