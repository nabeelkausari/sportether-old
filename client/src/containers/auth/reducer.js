import {
  AUTH_USER, AUTH_USER_ERROR, AUTH_USER_SUCCESS, CLEAR_AUTH_ERRORS, LOGOUT_USER, SIGNUP_USER, SIGNUP_USER_ERROR,
  SIGNUP_USER_SUCCESS
} from "./types"

export default (state = {}, { type, payload }) => {
  switch (type) {
    case AUTH_USER:
      return { ...state, authLoading: true, error: null };
    case AUTH_USER_SUCCESS:
      return { ...state, authLoading: false, authenticated: true, user: payload };
    case AUTH_USER_ERROR:
      return { ...state, authLoading: false, authenticated: false, error: payload };

    case SIGNUP_USER:
      return { ...state, signupLoading: true, signupError: null };
    case SIGNUP_USER_SUCCESS:
      return { ...state, signupLoading: false, signupSuccess: true, user: payload };
    case SIGNUP_USER_ERROR:
      return { ...state, signupLoading: false, signupSuccess: false, signupError: payload };

    case CLEAR_AUTH_ERRORS:
      return { ...state, error: null, signupError: null }

    case LOGOUT_USER:
      return { ...state, authenticated: false };
    default:
      return state;
  }
}
