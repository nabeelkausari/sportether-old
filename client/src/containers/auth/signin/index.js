import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import {signinUser, redirect} from '../actions';

const validate = formProps => {
  const errors = {};

  if (!formProps.email) errors.email = 'Please enter an email'
  if (!formProps.password) errors.password = 'Please enter a password'
  return errors;
}

function mapStateToProps(state) {
  return { signinError: state.auth.error }
}

export default Comp => {
  const signin = connect(mapStateToProps, { signinUser, redirect })(Comp)

  return reduxForm({
    form: 'signin',
    validate
  })(signin);
}
