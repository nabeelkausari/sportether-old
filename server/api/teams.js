import Team from '../models/Token';
import Sport from '../models/Sport';

export default server => {
  server.post('/api/teams', async (req, res) => {
    try {
      let { name, symbol, logo, description, sportId } = req.body;
      let team = new Team({ name, symbol, logo, description, sportId });
      let teamObj = await team.save();
      res.json(teamObj)
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  const teamId = async (req, res, next, _id) => {
    try {
      req.team = await Team.findOne({ _id }).exec();
      next();
    } catch (err) {
      next(err);
    }
  };

  server.get('/api/team/:teamId', async (req, res) => {
    try {
      res.json(req.team);
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.put('/api/team/:teamId', async (req, res) => {
    try {
      if (req.team.address.mainNet || req.team.teamSale.mainNet) {
        throw "Can't update the team which is live on network"
      }
      let { name, symbol, logo, description, sportId } = req.body;
      let team = await req.team.update({ name, symbol, logo, description, sportId }, { new: true });
      res.json(team);
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.param('teamId', teamId);

  server.get('/api/teams', async (req, res) => {
    try {
      let sportId = req.query.sportId
        ? req.query.sportId
        : (await Sport.findOne({ slug: req.query.sport }).exec())._id;

      let teams = await Team.find({ sportId })
        .limit(parseInt(req.query.limit) || null)
        .select('name symbol sportId balance address price')
        .populate('sportId', 'name')
        .exec();

      res.json(teams)
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.get('/api/selected', async (req, res) => {
    try {
      let football = await Team.find({
        'symbol': { $in: [...req.query.football.split(',')]}
      }).select('name symbol balance address price').exec();
      let cricket = await Team.find({
        'symbol': { $in: [...req.query.cricket.split(',')]}
      }).select('name symbol balance address price').exec();

      res.json({ football, cricket })
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.get('/api/topTeams', async (req, res) => {
    try {
      let sports = await Sport.find({}).exec();
      let limit = parseInt(req.query.limit) < 10 ? parseInt(req.query.limit) : 10;
      let teamSet = await Promise.all(
        sports.map(sport => Team.find({ sportId: sport._id })
          .limit(limit)
          .populate('sportId', 'name')
          .exec()
        )
      );
      res.json(teamSet)
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  // Temp code
  // server.post('/api/updatePrices', async (req, res) => {
  //   try {
  //     let teams = await Team.update({ sportId: '5aa6661a1a04c64fb318e9a1' }, {balance: 2500, price: 4000000000000000}, {multi: true})
  //     res.json(teams)
  //   } catch (error) {
  //     res.status(422).send({ error });
  //   }
  // })

}
