import Ground from "../models/Ground";
import Wallet from "../models/Wallet";
import Transaction from "../models/Transaction";
import Fund from "../models/Fund";
import Participation from "../models/Participation";
import Match from "../models/Match"
import Sale from "../models/Sale"
import { requireAuth } from "../config/passport";

import {
  isGroundOpen, updateEtherFund, updateTokenFund,
  isUserParticipant, userCanWithdraw, removeParticipant, userCanExit, checkOpponent
} from "../services/grounds";

export default server => {

  server.put('/api/grounds/enter', requireAuth, async (req, res) => {
    try {
      let { groundId, teamId } = req.body;

      if (!groundId || !teamId) throw 'Invalid arguments'
      // Check whether ground is open
      let ground = await isGroundOpen(groundId)

      // Check Match status
      if (!ground.match.active) throw "Match is inactive";
      if (ground.match.isLive) throw "Match is live, cannot enter ground";

      // Check whether user has a team token
      let tokenFund = await Fund.findOne({ userId: req.user.id, tokenId: teamId }).exec();
      if (!tokenFund || tokenFund.balance === 0) throw "User does not have team";

      // Check whether user has sufficient funds
      let wallet = await Wallet.findOne({ userId: req.user.id }).exec();
      if (!wallet) throw "User has no wallet";

      let { ethBalance } = wallet;
      let value;
      let teamSide;

      if (ground.match.teamA.token.toString() === teamId) {
        value = parseInt(ground.match.teamA.value);
        teamSide = 'teamA';
        checkOpponent(ground, 'teamB', req.user.id);
      } else if (ground.match.teamB.token.toString() === teamId) {
        value = parseInt(ground.match.teamB.value);
        teamSide = 'teamB';
        checkOpponent(ground, 'teamA', req.user.id);
      } else {
        throw "Unknown team"
      }

      if (ground[teamSide].user) throw "Team is already occupied";

      let totalFee = value * (parseInt(ground.percent) / 100);
      if (parseInt(ethBalance) < totalFee) throw "Insufficient funds";

      // Deduct team token from user's wallet
      let tokenNewBalance = await updateTokenFund(req.user.id, teamId, false);

      // Deduct ethers from user's wallet
      let ethNewBalance = await updateEtherFund(req.user.id, totalFee, false);

      // Add a txn record
      let record = await new Transaction({
        userId: req.user.id,
        type: 'send',
        tokenId: teamId,
        groundId,
        tokenUnit: 1,
        tokenNewBalance,
        ethUnit: totalFee,
        ethNewBalance
      }).save();

      // If opponent has entered mark open as false, Mark fee as paid
      // Enter user in ground (Update teams field)
      let updation = {};
      if (teamSide === 'teamA') {
        updation = { teamA: { user: req.user.id, fee: totalFee, paid: true }}
        if (ground.teamB.user) {
          updation = { ...updation, open: false, canWithdraw: false, canExit: false }
          await new Sale({ userId: req.user.id, groundId, type: 'fee', amount: totalFee }).save();
        }
      } else {
        updation = { teamB: { user: req.user.id, fee: totalFee, paid: true }}
        if (ground.teamA.user) {
          updation = { ...updation, open: false, canWithdraw: false, canExit: false }
          await new Sale({ userId: req.user.id, groundId, type: 'fee', amount: totalFee }).save();
        }
      }
      await ground.update(updation);

      // Add Ground in user's collection
      let participation = await Participation.findOne({ userId: req.user.id }).exec()
      if (!participation) {
        participation = await new Participation({ userId: req.user.id }).save();
      }
      await participation.update({ grounds: [ ...participation.grounds, groundId ]});

      await record.update({ status: 'success'})

      res.json({ message: `User ${req.user.id} has successfully entered the ground` });
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.put('/api/grounds/exit', requireAuth, async (req, res) => {
    try {
      let { groundId } = req.body;

      // Check whether ground is open
      let ground = await userCanExit(groundId)
      // Check whether user is a participant
      let teamSide = await isUserParticipant(ground, req.user.id);

      let totalFee = ground[teamSide].fee;

      // Remove Ground in participation's collection
      await removeParticipant(req.user.id, groundId);

      // Add team token to user's wallet
      let tokenNewBalance = await updateTokenFund(req.user.id, ground.match[teamSide].token, true);
      // Add ethers to user's wallet
      let ethNewBalance = await updateEtherFund(req.user.id, totalFee, true)

      // Exit user from ground (Update teams field)
      let updation = {};
      updation[teamSide] = { user: null, paid: false }
      updation.open = true;
      await ground.update(updation)

      // Add a txn record
      await new Transaction({
        userId: req.user.id,
        type: 'receive',
        tokenId: ground.match[teamSide].token,
        groundId,
        tokenUnit: 1,
        tokenNewBalance,
        ethUnit: totalFee,
        ethNewBalance,
        status: 'success'
      }).save();

      res.json({ message: `User ${req.user.id} has been removed from the ground` });

    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.put('/api/grounds/withdrawTeam', requireAuth, async (req, res) => {
    try {
      let { groundId } = req.body;

      // Check whether user can withdraw from ground
      let ground = await userCanWithdraw(groundId);
      // Check whether user is a participant
      let teamSide = await isUserParticipant(ground, req.user.id);

      if (ground.withdrawTokens[teamSide]) {
        throw "User has already withdrawn team";
      }

      // This check prevents user to withdraw tokens
      // if he can exit from ground (if not, tokens will be added twice)
      let player = !ground.canExit && !ground.open
      if (!player) throw "Withdrawals are restricted to players";

      // Mark withdrawTokens as true
      let updation = { withdrawTokens: { ...ground.withdrawTokens }}
      updation.withdrawTokens[teamSide] = true;
      await ground.update(updation)

      // Add team token to user's wallet
      let tokenNewBalance = await updateTokenFund(req.user.id, ground.match[teamSide].token, true)
      // Add a txn record
      await new Transaction({
        userId: req.user.id,
        type: 'receive',
        tokenId: ground.match[teamSide].token,
        groundId,
        tokenUnit: 1,
        tokenNewBalance,
        status: 'success'
      }).save();

      res.json({ message: `User ${req.user.id} has withdrawn team ${ground.match[teamSide].token}` });

    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.put('/api/grounds/withdrawReward', requireAuth, async (req, res) => {
    try {
      let { groundId } = req.body;

      // Check whether ground is closed
      let ground = await userCanWithdraw(groundId);
      // Check whether user is a participant
      let teamSide = await isUserParticipant(ground, req.user.id);

      // if draw, get the draw rewards for user's team
      // If winner, check whether user's team is a winner
      let ethReward;
      if (ground.result === "draw") {
        if (ground.drawRewards[teamSide].received) {
          throw "User had already withdrawn rewards";
        }

        ethReward = ground.drawRewards[teamSide].amount;
        let updation = { ...ground.drawRewards }
        updation[teamSide].received = true
        await ground.update({ drawRewards: updation })

      } else if (ground.result === teamSide) {
        if (ground.reward.received) {
          throw "User had already withdrawn rewards";
        }

        ethReward = ground.reward.amount;
        let updation = { ...ground.reward }
        updation.received = true;
        await ground.update({ reward: updation })

      } else {
        throw "User is not a winner, hence can't withdraw"
      }

      // Add ethers to user's wallet
      let ethNewBalance = await updateEtherFund(req.user.id, ethReward, true);

      // Add a txn record
      await new Transaction({
        userId: req.user.id,
        type: 'receive',
        groundId,
        ethUnit: ethReward,
        ethNewBalance,
        status: 'success'
      }).save();

      res.json({ message: `User ${req.user.id} has succesfully withdrawn reward` });
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.post('/api/grounds', requireAuth, async (req, res) => {
    try {
      let { matchId, percent } = req.body;

      if (!matchId || !percent) throw "Invalid arguments";

      // TODO: Avoid creation if opponent has already created a ground with the same fee

      let match = await Match.findOne({ _id: matchId }).exec();

      if (!match) throw "Invalid match";
      if (!match.active) throw "Match is inactive";
      if (match.isLive) throw "Match is live, cannot create ground";

      let commission = 0.06;
      let teamAValue = parseInt(match.teamA.value) * (parseInt(percent) / 100);
      let teamBValue = parseInt(match.teamB.value) * (parseInt(percent) / 100);
      let drawA = teamAValue - (teamAValue * commission);
      let drawB = teamBValue - (teamBValue * commission);
      let winner = drawA + drawB;
      let sale = (teamAValue + teamBValue) * commission;

      let ground = await new Ground({
        match: matchId, percent : parseInt(percent), creator: req.user.id,
        reward: { amount: winner }, sale,
        drawRewards: { teamA: { amount: drawA }, teamB: { amount: drawB }},
      }).save();

      res.json(ground);
    } catch (error) {
      res.status(422).send({ error });
    }
  });


  server.get('/api/grounds', async (req, res) => {
    try {
      let { matchId, percent, open } = req.query;
      let query = { match: matchId }
      if (percent !== undefined) query.percent = percent;
      if (open !== undefined) query.open = open;

      let grounds = await Ground.find(query).exec();
      res.json(grounds)
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  const groundId = async (req, res, next, _id) => {
    try {
      req.ground = await Ground.findOne({ _id })
        .populate({
          path: 'match',
          select: '-creator -createdAt -updatedAt -__v',
          populate: {
            path: 'teamA.token teamB.token',
            select: 'name symbol logo'
          }
        })
        .select('-creator -createdAt -updatedAt -__v')
        .exec();
      next();
    } catch (err) {
      next(err);
    }
  };

  server.get('/api/grounds/:groundId', async (req, res) => {
    try {
      res.json(req.ground)
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.param('groundId', groundId);
}
