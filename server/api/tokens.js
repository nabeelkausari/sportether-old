import Token from '../models/Token';
import Fund from '../models/Fund';
import Wallet from '../models/Wallet';
import Transaction from "../models/Transaction";
import Sale from "../models/Sale";
import { requireAuth } from "../config/passport"
import { ethToWei, weiToEth } from "../config/constants";
import Referral from "../models/Referral"
import User from "../models/User"
import { tokenPurchaseWithBonusMail, tokenPurchaseMail, directEtherBonusMail, indirectEtherBonusMail } from "../mails/token";

export default server => {

  // Buy
  server.post('/api/tokens/buy', requireAuth, async (req, res) => {
    try {
      // REGULAR CHECKS
      let { unit, tokenId } = req.body;

      if ((unit % 1) > 0) throw "No fractions allowed, only whole number";

      // Check whether user has sufficient ethers
      let wallet = await Wallet.findOne({ userId: req.user.id }).populate('userId', 'email username').exec();
      if (!wallet) throw "User has no wallet";

      let { ethBalance } = wallet;
      let token = await Token.findById(tokenId).populate('sportId', 'slug').exec();
      if (!token) throw "Invalid token Id";
      let { price, balance } = token;

      // check the token availability
      if (balance < unit) throw "Required amount of tokens are not available";
      if (!price) throw "Error in token price";

      let totalCost = parseInt(unit) * parseInt(price);
      if (parseInt(ethBalance) < totalCost) throw "Insufficient funds";

      // ALL GOOD
      // find the token in user's funds
      let fund = await Fund.findOne({ userId: req.user.id, tokenId }).exec();
      if (!fund) {
        fund = await new Fund({ userId: req.user.id, tokenId }).save();
        await wallet.update({ tokens: [...wallet.tokens, fund._id ]});
      }

      // update the ether balance
      let ethNewBalance = parseInt(ethBalance) - totalCost;
      await wallet.update({ ethBalance: ethNewBalance });

      // update the user's token balance
      let tokenNewBalance = parseInt(fund.balance) + parseInt(unit);
      await fund.update({ balance: tokenNewBalance });

      // update the token supply balance and price
      let tokenSupplyBalance = token.balance - parseInt(unit)
      let tokenPrice = calculatePrice(token.sportId.slug, tokenSupplyBalance)
      await token.update({ balance: tokenSupplyBalance, price: tokenPrice })

      // update the sale entry
      await new Sale({ userId: req.user.id, tokenId, type: 'purchase', amount: totalCost }).save();

      // record the transaction
      await new Transaction({
        userId: req.user.id,
        type: 'buy',
        tokenId,
        tokenUnit: unit,
        tokenNewBalance,
        ethUnit: totalCost,
        ethNewBalance,
        status: 'success'
      }).save();

      let actualPrice = 20000000000000000; // 0.02 ETH

      let emailOptions = {
        email: wallet.userId.email,
        username: wallet.userId.username,
        token: token.name,
        symbol: token.symbol,
        unit,
        price: weiToEth(actualPrice),
        totalPrice: weiToEth(actualPrice * parseInt(unit)),
        discount: getDiscount(actualPrice, price),
        actualCost: weiToEth(totalCost)
      }

      // hadTransacted? -> SPO
      await userHadTransacted(req.user.id);
      // Reward the referrer -> ETH
      await rewardReferrer(req.user.id, totalCost, emailOptions);

      res.json({ unit, symbol: token.symbol });
    } catch (error) {
      res.status(422).send({ error })
    }
  })

  // Send - to Ground
  // Receive - from Ground
  // Deposit
  // Withdraw
  // SELL

  server.get('/api/deployedTokens', async (req, res) => {
    try {
      let tokens = await Token.find().exec();
      res.json(tokens)
    } catch (error) {
      res.status(422).send({ error });
    }
  });

}

export const userHadTransacted = async userId => {
  let user = await User.findOne({ _id: userId }).exec();
  if (user.hadTransacted) return
  // Find the parent User
  let referral = await Referral.findOne({ userId }).exec()
  // Move SPOs
  if (referral.parent) await moveSPOs(referral.parent, 200)
  await moveSPOs(userId, 300);
  // update user's hadTransacted field
  await user.update({ hadTransacted: true });
}

const rewardReferrer = async (userId, sale, emailOptions) => {
  let cashBackPercent = 5;
  let directBonusPercent = 10;
  let indirectBonusPercent = 1;
  let cashBack = sale * (cashBackPercent/100);
  let directBonus = sale * (directBonusPercent/100);
  let indirectBonus = sale * (indirectBonusPercent/100);

  // Find the parent User
  let referral = await Referral.findOne({ userId: userId })
    .populate('parent', 'email username')
    .exec();

  if (referral.parent) {
    let referralsParent = await Referral.findOne({ userId: referral.parent._id })
      .populate('parent', 'email')
      .exec();

    if (referralsParent.parent) {
      await addEthers(userId, referralsParent.parent._id, indirectBonus);
      await recordEtherBonus(referral.parent._id, referralsParent.parent._id, indirectBonus)

      indirectEtherBonusMail({
        email: referralsParent.parent.email,
        bonusPercent: indirectBonusPercent,
        bonusValue: weiToEth(indirectBonus),
        friend: referral.parent.username,
        friendsFriend: emailOptions.username
      })
    }
    await addEthers(userId, referral.parent._id, directBonus);
    await recordEtherBonus(userId, referral.parent._id, directBonus)
    await addEthers(userId, userId, cashBack);

    directEtherBonusMail({
      email: referral.parent.email,
      bonusPercent: directBonusPercent,
      bonusValue: weiToEth(directBonus),
      friend: emailOptions.username
    })

    tokenPurchaseWithBonusMail({
      ...emailOptions,
      bonusPercent: cashBackPercent,
      bonusValue: weiToEth(cashBack)
    })

  } else {
    tokenPurchaseMail({ ...emailOptions })
  }
}

const addEthers = async (bonusFrom, bonusReceiver, units) => {
  let wallet = await Wallet.findOne({ userId: bonusReceiver });
  let ethBalance = wallet.ethBalance + units;
  await wallet.update({ ethBalance });
  // record the transaction
  await new Transaction({
    userId: bonusReceiver,
    bonusFrom,
    type: 'bonus',
    ethUnit: units,
    ethNewBalance: ethBalance,
    status: 'success'
  }).save();
}

const recordEtherBonus = async (bonusFrom, bonusReceiver, units) => {
  let referral = await Referral.findOne({ userId: bonusReceiver }).exec();
  referral.children.forEach(child => {
    if (child.user.toString() === bonusFrom) {
      child.ethBonus += units
    }
  });
  await referral.update({ children: [...referral.children]})
}

export const moveSPOs = async (userId, units) => {
  let wallet = await Wallet.findOne({ userId })
  // move SPOs from reserved -> balance
  if (wallet.spoReserved < units) return;
  let spoReserved = wallet.spoReserved - units; // units not in wei
  let spoBalance = wallet.spoBalance + units; // units not in wei
  await wallet.update({ spoReserved , spoBalance })
}

const getDiscount = (actualPrice, discountedPrice) => {
  return 100 - (discountedPrice/actualPrice * 100)
}

const calculatePrice = (sport, balance) => {
  let price;
  switch (sport) {
    case "football":
      if (balance >= 3150) price = 0.004;
      else if (balance >= 2450) price = 0.008;
      else if (balance >= 700) price = 0.012;
      else price = 0.016;
      break;
    case "cricket":
      if (balance >= 2250) price = 0.004;
      else if (balance >= 1750) price = 0.008;
      else if (balance >= 500) price = 0.012;
      else price = 0.016;
      break;
  }
  return ethToWei(price);
}
