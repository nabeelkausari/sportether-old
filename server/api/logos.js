import Logo from '../models/Logo';
import Token from '../models/Token';

export default server => {
  const decodeBase64Image = dataString => {
    let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {};

    if (matches.length !== 3) {
      return new Error('Invalid input string');
    }

    response.contentType = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
  }

  server.post('/api/logos', async (req, res) => {
    try {
      let { img, teamId } = req.body;
      if (!Object.keys(img).length || !teamId) {
        throw "Incomplete arguments, logoImg or teamId is missing"
      }

      // delete old logo image
      await Logo.find({ teamId }).remove().exec();

      // save the logo image
      let imageBuffer = decodeBase64Image(img.data);
      let logoObj = new Logo({ img: imageBuffer, teamId });
      let logo = await logoObj.save();

      // update the teams logo id
      let updatedTeam = await Token.findOneAndUpdate({ _id: teamId }, {logo: logo._id}, {new: true});

      res.json(updatedTeam)
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  const logoId = async (req, res, next, _id) => {
    try {
      req.logo = await Logo.findOne({ _id }).exec();
      next();
    } catch (err) {
      next(err);
    }
  };

  server.get('/api/logos/:logoId', async (req, res) => {
    try {
      res.contentType(req.logo.img.contentType);
      res.send(req.logo.img.data);
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.param('logoId', logoId);
}
