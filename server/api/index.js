import tokensApi from './tokens';
import sportsApi from './sports';
import teamsApi from './teams';
import logosApi from './logos';
import authApi from './auth';
import walletsApi from './wallets';
import transactionsApi from './transactions';
import matchesApi from './matches';
import groundsApi from './grounds';
import bonusApi from './bonus';
import referralsApi from './referral';
import adminApis from './admin/index';

export default app => {
  tokensApi(app);
  sportsApi(app);
  teamsApi(app);
  logosApi(app);
  authApi(app);
  walletsApi(app);
  transactionsApi(app);
  matchesApi(app);
  groundsApi(app);
  bonusApi(app);
  referralsApi(app);
  adminApis(app)
}
