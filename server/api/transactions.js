import { requireAuth } from "../config/passport"
import Transaction from "../models/Transaction"

export default server => {
  // server.get('/api/transactions/deposits', requireAuth, async (req, res) => {
  //   try {
  //     let deposits = await Deposit.find({ userId: req.user.id }).limit(10).exec();
  //     res.json(deposits)
  //   } catch (err) {
  //     res.send(err)
  //   }
  // });
  // server.get('/api/transactions/withdrawals', requireAuth, async (req, res) => {
  //   try {
  //     let withdrawals = await Withdrawal.find({ userId: req.user.id }).limit(10).exec();
  //     res.json(withdrawals)
  //   } catch (err) {
  //     res.send(err)
  //   }
  // });
  // server.get('/api/transactions', requireAuth, async (req, res) => {
  //   try {
  //     let transactions = await Promise.all([
  //       Deposit.find({ userId: req.user.id }).sort('-createdAt').limit(10).exec(),
  //       Withdrawal.find({ userId: req.user.id }).sort('-createdAt').limit(10).exec()
  //     ]);
  //     res.json(transactions);
  //   } catch (err) {
  //     res.send(err)
  //   }
  // });
  server.get('/api/transactions', requireAuth, async (req, res) => {
    try {
      let transactions = await Transaction.find({ userId: req.user.id })
        .sort('-createdAt').limit(20)
        .populate('tokenId bonusFrom', '-_id symbol -_id username')
        .exec();
      res.json(transactions)
    } catch (error) {
      res.status(422).send({ error });
    }
  });
}
