import { getUsers } from './users';
import { requireAdmin } from '../../config/passport';

export default server => {
  let preText = '/api/admin';
  server.get(preText + '/getUsers', requireAdmin, getUsers)
}

