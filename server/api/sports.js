import Sport from '../models/Sport';

export default server => {
  server.post('/api/sports', async (req, res) => {
    try {
      let { name, description, fanFollowing, tokenSupply, slug } = req.body;
      let sport = new Sport({ name, description, fanFollowing, tokenSupply, slug });
      let sportObj = await sport.save();
      res.json(sportObj)
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  const sportId = async (req, res, next, _id) => {
    try {
      req.sport = await Sport.findOne({ _id }).exec();
      next();
    } catch (error) {
      next(error);
    }
  };

  server.get('/api/sport/:sportId', async (req, res) => {
    try {
      res.json(req.sport);
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.put('/api/sport/:sportId', async (req, res) => {
    try {
      let { name, description, fanFollowing, tokenSupply, slug } = req.body;
      let sport = await req.sport.update({ name, description, fanFollowing, tokenSupply, slug }, { new: true });
      res.json(sport);
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.param('sportId', sportId);

  server.get('/api/sports', async (req, res) => {
    try {
      let sports = await Sport.find({}).exec();
      res.json(sports)
    } catch (error) {
      res.status(422).send({ error });
    }
  });
}
