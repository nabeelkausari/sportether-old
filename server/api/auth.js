import { signIn, signUp, getMe, confirmEmail, claimBonus } from '../services/auth';
import { requireSignin, requireAdminSignin } from '../config/passport';

export default server => {
  server.post('/api/signUp', signUp)
  server.post('/api/signIn', requireSignin, signIn)
  server.post('/api/admin/signIn', requireAdminSignin, signIn)
  server.post('/api/getMe', getMe)
  server.get('/api/confirmEmail/:jwt', confirmEmail)
}
