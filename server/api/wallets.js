import web3, { setProvider, wallet } from '../ethereum/web3';
import Wallet from '../models/Wallet';
// import Address from '../models/Address';
import { requireAuth } from "../config/passport"
import Transaction from "../models/Transaction"
import Record from "../models/Record"
import { addLog, logAction } from "../services/logs";
import sendDepositMail from '../mails/deposit';
import sendWithdrawalMail from '../mails/withdrawal';
import {weiToEth} from "../config/constants"

export default server => {
  server.get('/api/wallets', requireAuth, async (req, res) => {
    try {
      let wallet = await Wallet.findOne({ userId: req.user.id })
        .populate({
          path: 'tokens',
          select: 'balance',
          populate: {
            path: 'tokenId',
            select: 'name symbol'
         }
        }).exec();
      res.json(wallet);
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  // server.post('/api/wallets', requireAuth, async (req, res) => {
  //   try {
  //     let { userId, walletIndex } = req.body;
  //
  //     let addr = await Address.findOne({idx: walletIndex}).exec()
  //     let wallet = await new Wallet({ userId, ethAddress: addr.address }).save();
  //     res.json({ wallet })
  //   } catch (error) {
  //     res.status(422).send({ error });
  //   }
  // });

  // server.post('/api/generateAddresses', async (req, res) => {
  //   try {
  //     let list = wallet.generateAddresses(1000)
  //     let docs = [];
  //     list.forEach((address, idx) => docs.push({ idx, address}))
  //     let addresses = await Address.insertMany(docs);
  //     res.json({ addresses })
  //   } catch (error) {
  //     res.status(422).send({ error });
  //   }
  // })

  server.get('/api/wallets/requireSync', requireAuth, async (req, res) => {
    try {
      let wallet = await Wallet.findOne({ userId: req.user.id }).populate('userId', 'walletIndex').exec();
      let { requireSync, syncLock } = wallet;
      if (requireSync) {
        return res.json({ requireSync, syncLock })
      }

      let isBalance = await checkBalanceSufficiency(wallet);
      let isSyncRequired = isBalance.sufficient;

      await wallet.update({ requireSync: isSyncRequired });
      res.json({ requireSync, syncLock });
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.put('/api/wallets/sync', requireAuth, async (req, res) => {
    try {
      let wallet = await Wallet.findOne({ userId: req.user.id }).populate('userId', 'walletIndex email').exec();
      let { ethBalance, ethAddress, syncLock } = wallet;

      if (syncLock) {
        return res.json({ message: "Sync is going on", updated: false })
      }

      // Get the balance
      let isBalance = await checkBalanceSufficiency(wallet);
      if (!isBalance.sufficient) {
        return res.json({ message: isBalance.message, updated: false })
      }

      let log = await addLog(req.user.id, 'eth-sync')

      // SYNC LOCKED
      await wallet.update({ syncLock: true });
      await logAction(log._id, 'balance detected & sync locked')

      // transfer balance to main wallet
      let { availableBalance, web3Provider, estimatedGas, MAIN_WALLET } = isBalance;
      let value = parseInt(availableBalance) - estimatedGas;
      await logAction(log._id, `sending ${weiToEth(value)} to MAIN_WALLET from ${ethAddress}`)
      let txn = await web3Provider.eth.sendTransaction({ from: ethAddress, to: MAIN_WALLET, value})
      await logAction(log._id, `sent`)

      // Internal Record
      await new Record({
        userId: req.user.id,
        type: 'deposit',
        amount: availableBalance,
        estimatedGas,
        txnHash: txn.transactionHash,
        gasUsed: txn.gasUsed,
        cumulativeGasUsed: txn.cumulativeGasUsed
      }).save();

      // Users transaction record
      let ethNewBalance = parseInt(ethBalance) + parseInt(availableBalance);
      let record = await new Transaction({
        userId: req.user.id,
        type: 'deposit',
        ethUnit: availableBalance,
        ethNewBalance: ethNewBalance
      }).save();

      // SYNC UNLOCKED
      // update the balance
      await logAction(log._id, 'updating wallet')
      await wallet.update({ ethBalance: ethNewBalance, syncLock: false, requireSync: false });
      await logAction(log._id, 'wallet updated and sync unlocked')
      await record.update({ status: 'success' });
      sendDepositMail(wallet.userId.email, weiToEth(availableBalance))
      res.json({ message: "Balance updated successfully", updated: true });
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  const checkBalanceSufficiency = async ({ ethAddress, userId }) => {
    let availableBalance = await web3.eth.getBalance(ethAddress)
    if (availableBalance <= 0) {
      return { message: "No new deposits yet", sufficient: false }
    }

    let MAIN_WALLET = process.env.mainWallet;
    let web3Provider = setProvider(userId.walletIndex)
    let gas = await web3Provider.eth.estimateGas({ from: ethAddress, to: MAIN_WALLET, value: availableBalance})
    let gasPrice = await web3Provider.eth.getGasPrice();
    let estimatedGas = (gas * parseInt(gasPrice));
    if (availableBalance <= estimatedGas) {
      return { message: "Insufficient funds", sufficient: false }
    }

    return { sufficient: true, availableBalance, web3Provider, estimatedGas, MAIN_WALLET }
  }

  server.put('/api/wallets/withdrawEth', requireAuth, async (req, res) => {
    try {
      let { units, address } = req.body;

      if (!web3.utils.isAddress(address)) throw "Not a valid ETH address";

      let wallet = await Wallet.findOne({ userId: req.user.id }).populate('userId', 'email').exec();
      let { ethBalance } = wallet;
      let fee = 2000000000000000; // 0.002 ETH
      if ((ethBalance < units) || (ethBalance < fee)) throw "Insufficient funds";

      // transfer balance from main wallet to users wallet
      let MAIN_WALLET = process.env.mainWallet;
      let amountToWithdraw = units - fee;

      const txn = await web3.eth.sendTransaction({ from: MAIN_WALLET, to: address, value: amountToWithdraw })

      // Internal Record
      await new Record({
        userId: req.user.id,
        type: 'withdraw',
        amount: amountToWithdraw,
        txnHash: txn.transactionHash,
        gasUsed: txn.gasUsed,
        cumulativeGasUsed: txn.cumulativeGasUsed
      }).save();

      // Users transaction record
      let ethNewBalance = parseInt(ethBalance) - parseInt(units);
      let record = await new Transaction({
        userId: req.user.id,
        type: 'withdrawal',
        to: address,
        ethUnit: units,
        txnId: txn.transactionHash,
        ethNewBalance: ethNewBalance
      }).save();

      await wallet.update({ ethBalance: ethNewBalance });
      await record.update({ status: 'success' })
      sendWithdrawalMail(wallet.userId.email, weiToEth(amountToWithdraw), txn.transactionHash)
      res.json({ ...txn });
    } catch (error) {
      res.status(422).send({ error });
    }
  });
}
