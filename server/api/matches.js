import Match from "../models/Match"
import { requireAdmin } from "../config/passport";
import Sport from "../models/Sport"
import Ground from "../models/Ground"

export default server => {

  server.put('/api/matches/conclude', requireAdmin, async (req, res) => {
    try {
      let { matchId, winningTeam, draw } = req.body;

      // If not match in request, throw
      if (!matchId) throw 'Match ID not provided'
      // Check winningTeam or draw should be defined
      if (!winningTeam && draw === undefined) throw 'No results defined'

      // Check whether match is active
      let match = await Match.findOne({ _id: matchId }).exec();
      if (!match) throw 'Invalid Match ID';
      if (!match.active) throw 'Match is not active';
      // Check whether match is live
      if (!match.isLive) throw 'Match is not live';
      // Check whether match is not complete
      if (match.complete) throw 'Match is already finished';


      let updates = { active: false, isLive: false, complete: true };
      let result;
      if (winningTeam) {
        if (winningTeam === match.teamA.token.toString()) result = 'teamA';
        else if (winningTeam === match.teamB.token.toString()) result = 'teamB';
        else throw 'Unknown winning team';
        updates.winningTeam = winningTeam
      } else if (draw) {
        result = 'draw'
        updates.draw = true
      } else throw 'Unknown result';

      await match.update(updates);

      // Update result in every ground
      await Ground.updateMany({ match: matchId, open: false }, { result, canWithdraw: true })

      res.json({ message: `Match ${matchId} has been concluded` });
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.put('/api/matches/start', requireAdmin, async (req, res) => {
    try {
      let { matchId } = req.body;

      // If not match in request, throw
      if (!matchId) throw 'Invalid Match ID'

      // Check whether match is active
      let match = await Match.findOne({ _id: matchId }).exec();
      if (!match.active) throw 'Match is not active';
      // Check whether match is not live
      if (match.isLive) throw 'Match is already started';
      // Check whether match is not complete
      if (match.complete) throw 'Match is already finished';

      // Mark live as true
      await match.update({ isLive: true });

      // Update result in every ground
      await Ground.updateMany({ match: matchId, open: true }, { canExit: true, open: false })

      res.json({ message: `Match ${matchId} has been started` });
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.put('/api/matches/deactivate', requireAdmin, async (req, res) => {
    try {
      let { matchId } = req.body;

      // If not match in request, throw
      if (!matchId) throw 'Invalid Match ID'

      // Check whether match is active
      let match = await Match.findOne({ _id: matchId }).exec();
      if (!match.active) throw 'Match is already inactive';
      // Check whether match is not live
      if (match.isLive) throw 'Match is going live, Cant deactivate it';
      // Check whether match is not complete
      if (match.complete) throw 'Match is already finished';

      // Mark active as false
      await match.update({ active: false });

      // Update result in every ground
      await Ground.updateMany({ match: matchId, open: false }, { canExit: true })

      res.json({ message: `Match ${matchId} is deactivated` });
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.put('/api/matches/activate', requireAdmin, async (req, res) => {
    try {
      let { matchId } = req.body;

      // If not match in request, throw
      if (!matchId) throw 'Invalid Match ID'

      let match = await Match.findOne({ _id: matchId }).exec();
      // Check whether match has valuation
      if (!match.teamA.value || !match.teamB.value) {
        throw 'Match does not have proper valuation'
      }
      // Check whether match is not active
      if (match.active) throw 'Match is already active';
      // Check whether match is not live
      if (match.isLive) throw 'Match is going live, Cant deactivate it';
      // Check whether match is not complete
      if (match.complete) throw 'Match is already finished';


      // Mark active as true
      await match.update({ active: true });

      // Update result in every ground
      await Ground.updateMany({ match: matchId, open: false }, { canExit: false })

      res.json({ message: `Match ${matchId} is activated` });
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.put('/api/matches/setValues', requireAdmin, async (req, res) => {
    try {
      let { matchId, valueA, valueB } = req.body;

      // If not match in request, throw
      if (!matchId) throw 'Invalid Match ID'
      if (!valueA || !valueB) throw 'Incomplete values provided'

      // Check whether match is not active
      // Check whether match is not complete
      let match = await Match.findOne({ _id: matchId }).exec();
      if (match.active) throw 'Match is already active';
      if (match.complete) throw 'Match is already finished';

      let updation = {
        teamA: { token: match.teamA.token, value: valueA },
        teamB: { token: match.teamB.token, value: valueB }
      };

      await match.update(updation);

      res.json({ message: `Values has been set for Match ${matchId}` });
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.get('/api/matches', async (req, res) => {
    try {
      let { sportId, sport, complete, active, isLive } = req.query;
      let _sportId = sportId ? sportId
        : (await Sport.findOne({ slug: sport }).exec())._id;

      let query = { sport: _sportId }
      if (complete !== undefined) query.complete = complete;
      if (active !== undefined) query.active = active;
      if (isLive !== undefined) query.isLive = isLive;

      let limit = parseInt(req.query.limit) || null
      let matches = await Match.find(query)
        .limit(limit)
        .populate('sport', 'name')
        .populate({
          path: 'teamA.token',
          select: 'name symbol logo'
        })
        .populate({
          path: 'teamB.token',
          select: 'name symbol logo'
        })
        .exec();
      res.json(matches)
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.post('/api/matches', requireAdmin, async (req, res) => {
    try {
      let {
        sport, teamA, teamB, valueA, valueB, description,
        location, startsAt, durationInMinutes
      } = req.body;

      let match = await new Match({
        sport, description,
        teamA: { token: teamA, value: valueA },
        teamB: { token: teamB, value: valueB },
        location, startsAt, durationInMinutes, creator: req.user.id
      }).save();

      res.json(match);
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  const matchId = async (req, res, next, _id) => {
    try {
      req.match = await Match.findOne({ _id })
        .populate('sport', 'name')
        .populate({
          path: 'teamA.token',
          select: 'name symbol logo'
        })
        .populate({
          path: 'teamB.token',
          select: 'name symbol logo'
        })
        .exec();
      next();
    } catch (err) {
      next(err);
    }
  };

  server.put('/api/matches/:matchId', requireAdmin, async (req, res) => {
    try {
      let { description, location, startsAt, durationInMinutes } = req.body;

      let obj = {};
      if (description) obj = { ...obj, description }
      if (location) obj = { ...obj, location }
      if (startsAt) obj = { ...obj, startsAt }
      if (durationInMinutes) obj = { ...obj, durationInMinutes }

      let updatedMatch = await req.match.update(obj);

      res.json(updatedMatch);
    } catch (error) {
      res.status(422).send({ error });
    }
  });

  server.get('/api/matches/:matchId', async (req, res) => {
    try {
      res.json(req.match)
    } catch (error) {
      res.status(422).send({ error });
    }
  })

  server.param('matchId', matchId);
}
