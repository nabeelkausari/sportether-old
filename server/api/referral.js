import Referral from '../models/Referral';
import { requireAuth } from '../config/passport';

export default server => {
  server.get('/api/referrals', requireAuth, getReferralList)
}

const getReferralList = async (req, res) => {
  try {
    const { children } = await Referral.findOne({userId: req.user.id})
      .populate('children.user', 'email username bonusClaimed bonusApproved createdAt')
      .exec();
    res.json(children);
  } catch (error) {
    res.status(422).send({ error });
  }
}
