import express from 'express';
import { createServer } from 'http';
import env from 'node-env-file';
import path from 'path';

import setupApis from './api';
import setupDb from './config/db';
import setupMiddlewares from './config/middlewares';
import setupPassport from './config/passport';
// import testDeploy from './ethereum/deploy';
// import testEmails from './mails/test';

// import tokens from 'twitter-tokens';
// import getTwitterFollowers from 'get-twitter-followers';

// getTwitterFollowers(tokens, 'sportether').then(followers => {
//   console.log(followers.map(f => f.screen_name)); // "User Objects" array https://dev.twitter.com/overview/api/users
// });


const prod = process.env.NODE_ENV === "production"
if (prod) {
  env('./conf.ini');
} else {
  env('./conf.dev.ini');
}
env('./.keys')
const port = parseInt(process.env.PORT, 10) || 5000

const app = express();
const server = createServer(app)

setupDb();
// testDeploy();

setupPassport();
setupMiddlewares(app);
setupApis(app);

app.use(express.static(path.join(__dirname + '/../client/build')));

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/../client/build/index.html'));
});

server.listen(port, err => {
  if (err) return console.log(err);
  console.log(`server listening on port ${port}`)
})
