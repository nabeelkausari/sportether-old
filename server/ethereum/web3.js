import HDWalletProvider from 'truffle-hdwallet-provider';
import { EthHdWallet } from 'eth-hd-wallet';
import Web3 from 'web3';

const ethNode = process.env.ethNode;
const provider = new HDWalletProvider(
  process.env.mnemonic,
  ethNode
);

export const wallet = EthHdWallet.fromMnemonic(process.env.mnemonic);

export const setProvider = index => {
  const provider = new HDWalletProvider(process.env.mnemonic, ethNode, index);
  return new Web3(provider)
}

let web3;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // We are in the browser and metamask is running
  web3 = new Web3(window.web3.currentProvider);
} else {
  // We are on the server OR user doesn't have metamask
  // const provider = new Web3.providers.HttpProvider('https://rinkeby.infura.io/KXrbRI4GTIGmyauAdPpg');
  web3 = new Web3(provider);
}

export default web3;
