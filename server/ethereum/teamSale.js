import web3 from './web3';
import TeamSale from './build/TeamSale.json';

export default address => {
  return new web3.eth.Contract(
    JSON.parse(TeamSale.interface),
    address
  );
}
