import web3 from './web3';
import Team from './build/Team.json';

export default address => {
  return new web3.eth.Contract(
    JSON.parse(Team.interface),
    address
  );
}
