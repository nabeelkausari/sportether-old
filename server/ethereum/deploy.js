import web3 from './web3';
import compiledTeam from './build/Team.json';
import compiledTeamSale from './build/TeamSale.json';
// import teamDetails from './teamDetails.json';
import Token from '../models/Token';

export default async () => {
  const [account] = await web3.eth.getAccounts();
  console.log('Attempting to deploy from account', account);
  const wallet = '0x718dA65C347c706D14CbC484a85C2535b640D956';
  const gas = '1000000';
  const weiAmount = '2000000000000000';
  const footballId = '5aa666461a04c64fb318e9a2'

  let teamDetails = await Token.find({ sportId: footballId }).limit(2).exec();

  for (let team of teamDetails) {
    console.log('------------------------------')
    console.log('teamCreation begins: ', [team.name, team.symbol, 2500])

    const teamResult = await new web3.eth.Contract(JSON.parse(compiledTeam.interface))
      .deploy({ data: compiledTeam.bytecode, arguments:['SportEther ' + team.name, team.symbol, 3500] })
      .send({ gas, from: account });
    console.log('teamCreation done')

    const { options: { address }} = teamResult;

    // console.log('teamSaleCreation begins')
    // const teamSaleResult = await new web3.eth.Contract(JSON.parse(compiledTeamSale.interface))
    //   .deploy({ data: compiledTeamSale.bytecode, arguments: [weiAmount, wallet, address] })
    //   .send({ gas, from: account });
    // console.log('teamSaleCreation done')

    const balance = await teamResult.methods.balanceOf(account).call();
    await teamResult.methods.transfer(wallet, balance)
      .send({ gas, from: account })
    // await teamResult.methods.approve(teamSaleResult.options.address, balance)
    //   .send({ gas, from: account });
    // console.log('approval done')

    await team.update({
      address: {
        testNet: address,
      },
      balance
      // teamSale: {
      //   testNet: teamSaleResult.options.address
      // }
    });

  }
  console.log('done');
}
