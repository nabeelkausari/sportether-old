import jwt from 'jwt-simple';

import Wallet from '../models/Wallet';
import Address from '../models/Address';
import User from '../models/User';
import Referral from '../models/Referral';
import constants from '../config/constants';
import { verificationMail, sendMail } from "../mails";
import { sendSignupMails } from "../mails/signup"

const tokenForUser = user => {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, constants.JWT_SECRET);
}

export const getMe = async (req, res) => {
  try {
    const decoded = jwt.decode(req.body.jwt, constants.JWT_SECRET);
    const user = await User.findById(decoded.sub)
      .select('firstName lastName email username confirmed isAdmin bonusClaimed bonusApproved').exec();
    res.json({ user });
  } catch (error) {
    res.status(422).send({ error });
  }
}

export const signIn = async (req, res, next) => {
  let { email, username, firstName, lastName, confirmed, bonusClaimed, bonusApproved, hadTransacted, isAdmin } = req.user;
  let subject = 'You’ve just signed in to your SportEther account';
  let mailContent = {
    title: 'Login Success!',
    hiddenMessage: 'You’ve successfully logged in to your SportEther account just now.',
    body: `
<div>
    <p>You’ve successfully logged in to your SportEther account just now.</p>
    <p>We send this email as a security measure each time you login to your account. If this was not you, please contact us immediately from <a href="mailto:contact@sportether.com">contact@sportether.com</a> - so we can look into it.</p>
</div>`
  }
  sendMail({ email, subject, mailContent });
  res.send({ jwt: tokenForUser(req.user), user: { email, username, firstName, lastName, confirmed, bonusClaimed, bonusApproved, hadTransacted, isAdmin } });
}

export const signUp = async (req, res, next) => {
  try {
    const { email, password, username, firstName, lastName, referralUsername } = req.body;
    if (!email || !password || !username) {
      let missingField;
      if (!email) missingField = 'Email';
      else if (!username) missingField = 'Username';
      else if (!password) missingField = 'Password';

      return res.status(422).send({ error: `${missingField} is required`});
    }
    const existingUser = await User.findOne({ email }).exec();
    if (existingUser) {
      return res.status(422).send({ error: 'Email already exists'});
    }
    const existingUsername = await User.findOne({ username }).exec();
    if (existingUsername) {
      return res.status(422).send({ error: 'Username already exists'});
    }
    const user = await new User({ email, password, username, firstName, lastName }).save();
    await createWallet(user._id, user.walletIndex)
    await addReferral(referralUsername, user._id);
    let jwt = tokenForUser(user);
    verificationMail({ username, email, jwt });
    res.json({ jwt, user: { email, password, username, firstName, lastName } })
  } catch (err) {
    return next(err)
  }
}

export const confirmEmail = async (req, res) => {
  try {
    const decoded = jwt.decode(req.params.jwt, constants.JWT_SECRET);
    const user = await User.findById(decoded.sub).select('username email confirmed').exec();
    await user.update({ confirmed: true });
    await sendSignupMails(user);
    res.redirect('/email-confirmed?username=' + user.username)
  } catch (error) {
    res.redirect('/email-error')
  }
}

const addReferral = async (referralUsername, userId) => {
  const parent = await User.findOne({ username: referralUsername }).exec();
  if (parent) {
    const parentReferral = await Referral.findOne({userId: parent._id}).exec();
    await parentReferral.update({ children: [...parentReferral.children, { user: userId }]});
  }

  await new Referral({ userId, parent }).save();
}


const createWallet = async (userId, walletIndex) => {
  let address = await Address.findOne({idx: walletIndex}).exec()
  await new Wallet({ userId, ethAddress: address.address }).save();
}
