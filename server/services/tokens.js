import Team from '../ethereum/team';
import TeamSale from '../ethereum/teamSale';
import web3 from '../ethereum/web3';
import Token from '../models/Token';

export const updateTokenBalance = async () => {
  const [account] = await web3.eth.getAccounts();
  const tokens = await Token.find({}).exec();
  let ret = [];

  for (let i = 0; i < tokens.length; i++) {
    let team = await Team(tokens[i].address.testNet);
    let balance = await team.methods.balanceOf(account).call();

    ret.push(await Token.findOneAndUpdate({ _id: tokens[i]._id }, { balance }, {new: true}));
  }
  return ret;
}

export const tokenPurchase = async saleAddress => {
  let teamSale = await TeamSale(saleAddress);
  let purchaseEvent = teamSale.events.TokenPurchase();
  purchaseEvent.watch((err, result) => {
    console.log('event err: ', err)
    console.log('event result: ', result)
  })
}

// export const transferTokens = async teamAddress => {
//   const [account] = await web3.eth.getAccounts();
//   let team = await Team(teamAddress);
//   let userAddress = '0x718dA65C347c706D14CbC484a85C2535b640D956';
//   await team.methods.transfer(userAddress, 100).send({ from: account, gas: '1000000' });
//
//   let balance = await team.methods.balanceOf(account).call()
//   let query = { address: { testNet: teamAddress }};
//   let res = await Token.findOneAndUpdate(query, { balance }, { new: true }).exec();
//   res.json(res)
// }

// export const buyTokens = async teamSaleAddress => {
//   const [account] = await web3.eth.getAccounts();
//   let team = await Team(teamAddress);
//   let userAddress = '0x718dA65C347c706D14CbC484a85C2535b640D956';
//   await team.methods.transfer(userAddress, 100).send({ from: account, gas: '1000000' });
//
//   let balance = await team.methods.balanceOf(account).call()
//   let query = { address: { testNet: teamAddress }};
//   let res = await Token.findOneAndUpdate(query, { balance }, { new: true }).exec();
//   res.json(res)
// }
