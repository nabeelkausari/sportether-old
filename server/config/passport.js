import passport from 'passport';

import User from '../models/User';
import constants from './constants';
import { Strategy, ExtractJwt } from 'passport-jwt';
import LocalStrategy from 'passport-local';

const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, async (email, password, done) => {
  try {
    let user = await User.findOne({ email }).exec();
    if (!user) return done(null, false);
    if (!user.confirmed) return done(null, false);

    user.comparePassword(password, (err, isMatch) => {
      if (!isMatch) return done(null, false);

      return done(null, user);
    });

  } catch (err) {
    return done(err)
  }
})

const adminSignin = new LocalStrategy(localOptions, async (email, password, done) => {
  try {
    let user = await User.findOne({ email }).exec();
    if (!user) return done(null, false);
    if (!user.isAdmin) done(null, false)

    user.comparePassword(password, (err, isMatch) => {
      if (!isMatch) return done(null, false);

      return done(null, user);
    });

  } catch (err) {
    return done(err)
  }
})


const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: constants.JWT_SECRET
};

// Create JWT strategy
const jwtLogin = new Strategy(jwtOptions, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub).exec();
    if (user) done(null, user)
    else done(null, false)
  } catch (err) {
    return done(err, false)
  }
});

// Create JWT strategy
const adminLogin = new Strategy(jwtOptions, async (payload, done) => {
  try {
    const user = await User.findById(payload.sub).exec();
    if (user.isAdmin) done(null, user)
    else done(null, false)
  } catch (err) {
    return done(err, false)
  }
});

export const requireAuth = passport.authenticate('jwt', { session: false })
export const requireSignin = passport.authenticate('local', { session: false })
export const requireAdmin = passport.authenticate('admin', { session: false })
export const requireAdminSignin = passport.authenticate('admin-signin', { session: false })

export default () => {
  passport.use('jwt', jwtLogin);
  passport.use('local', localLogin);
  passport.use('admin', adminLogin)
  passport.use('admin-signin', adminSignin)
}
