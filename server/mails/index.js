import nodeMailer from 'nodemailer';
import env from 'node-env-file';

env('./.keys');
import accountConfirmationTemplate from './templates/accountConfirmation';
import defaultTemplate from './templates/default';

let transporter = nodeMailer.createTransport({
  service: "gmail",
  auth: {
    type: 'OAuth2',
    user: process.env.gmailUser,
    clientId: process.env.gmailClientId,
    clientSecret: process.env.gmailClientSecret,
    refreshToken: process.env.gmailRefreshToken
  }
});

export const verificationMail = ({ username, email, jwt}) => {
  let mailOptions = {
    from: '"SportEther" <contact@sportether.com>',
    to: email,
    subject: 'Account Confirmation',
    html: accountConfirmationTemplate(username, `https://sportether.com/api/confirmEmail/${jwt}`)
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
  });
}

export const sendMail = ({ email, subject, mailContent }) => {
  let mailOptions = {
    from: '"SportEther" <contact@sportether.com>',
    to: email,
    subject,
    html: defaultTemplate(mailContent)
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
  });
}

//-- Signup completed --- user | referred | direct | indirect
//-- Deposit Completed
//-- Withdrawal Completed

//-- Bonus claim received
//-- Bonus claim approved
//-- Bonus received parent

//-- Purchase token
//-- Money back received
//-- Referral received --- direct | indirect
