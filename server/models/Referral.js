import mongoose, { Schema } from 'mongoose';

const ReferralSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  parent: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  children: [
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      spoBonus: {type: Number, default: 0},
      ethBonus: {type: Number, default: 0}
    }
  ]
}, { timestamps: true });

export default mongoose.model('Referral', ReferralSchema)
