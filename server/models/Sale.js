import mongoose, { Schema } from 'mongoose';

const SaleSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  tokenId: {
    type: Schema.Types.ObjectId,
    ref: 'Token'
  },
  groundId: {
    type: Schema.Types.ObjectId,
    ref: 'Ground'
  },
  type: { type: String, enum: ['purchase', 'fee', 'gain']}, // Why Gain?
  amount: Number,
  currency: { type: String, default: 'ETH'}
}, { timestamps: true });

export default mongoose.model('Sale', SaleSchema)
