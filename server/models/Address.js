import mongoose, { Schema } from 'mongoose';

const AddressSchema = new Schema({
  idx: Number,
  address: String
}, { versionKey: false });

export default mongoose.model('Address', AddressSchema)
