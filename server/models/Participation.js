import mongoose, { Schema } from 'mongoose';

const ParticipationSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  grounds: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Ground'
    }
  ]
}, { timestamps: true });

export default mongoose.model('Participation', ParticipationSchema)
