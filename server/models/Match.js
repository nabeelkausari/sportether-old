import mongoose, { Schema } from 'mongoose';

const MatchSchema = new Schema({
  sport: {
    type: Schema.Types.ObjectId,
    ref: 'Sport'
  },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  teamA: {
    token: {
      type: Schema.Types.ObjectId,
      ref: 'Token'
    },
    value: Number
  },
  teamB: {
    token: {
      type: Schema.Types.ObjectId,
      ref: 'Token'
    },
    value: Number
  },
  description: String,
  location: String,
  startsAt: Date,
  durationInMinutes: Number,
  active: { type: Boolean, default: false },
  isLive: { type: Boolean, default: false },
  complete: { type: Boolean, default: false },
  draw: { type: Boolean, default: false },
  winningTeam: {
    type: Schema.Types.ObjectId,
    ref: 'Token'
  }
}, { timestamps: true });

export default mongoose.model('Match', MatchSchema)
