import mongoose, { Schema } from 'mongoose';

const TokenSchema = new Schema({
  name: String,
  symbol: { type: String, unique: true },
  description: String,
  balance: Number,
  price: Number,
  address: {
    testNet: String,
    mainNet: String
  },
  teamSale: {
    testNet: String,
    mainNet: String
  },
  sportId: {
    type: Schema.Types.ObjectId,
    ref: 'Sport'
  },
  leagueId: {
    type: Schema.Types.ObjectId,
    ref: 'League'
  },
  logo: {
    type: Schema.Types.ObjectId,
    ref: 'Logo'
  },
}, { timestamps: true });

export default mongoose.model('Token', TokenSchema)
