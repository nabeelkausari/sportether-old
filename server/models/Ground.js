import mongoose, { Schema } from 'mongoose';

const GroundSchema = new Schema({
  match: {
    type: Schema.Types.ObjectId,
    ref: 'Match'
  },
  percent: Number,
  teamA: {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    fee: Number,
    paid: { type: Boolean, default: false }
  },
  teamB: {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    fee: Number,
    paid: { type: Boolean, default: false }
  },
  reward: {
    amount: Number,
    received: { type: Boolean, default: false }
  },
  drawRewards: {
    teamA: {
      amount: Number,
      received: { type: Boolean, default: false }
    },
    teamB: {
      amount: Number,
      received: { type: Boolean, default: false }
    },
  },
  withdrawTokens: {
    teamA : { type: Boolean, default: false },
    teamB : { type: Boolean, default: false },
  },
  open: { type: Boolean, default: true },
  canWithdraw: { type: Boolean, default: false },
  canExit: { type: Boolean, default: true },
  result: { type: String, enum: ['teamA', 'teamB', 'draw'] },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  sale: Number
}, { timestamps: true });

export default mongoose.model('Ground', GroundSchema)
