import mongoose, { Schema } from 'mongoose';

const SportSchema = new Schema({
  name: String,
  description: String,
  icon: String,
  fanFollowing: Number,
  tokenSupply: Number,
  slug: { type: String, unique: true }
});

export default mongoose.model('Sport', SportSchema)
