import mongoose, { Schema } from 'mongoose';

const WalletSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    unique: true
  },
  ethAddress: String,
  ethBalance: {type: Number, default: 0},
  spoBalance: {type: Number, default: 0},
  spoReserved: {type: Number, default: 0},
  tokens: [{
    type: Schema.Types.ObjectId,
    ref: 'Fund'
  }],
  requireSync: { type: Boolean, default: false },
  syncLock: { type: Boolean, default: false }
}, { timestamps: true });

export default mongoose.model('Wallet', WalletSchema)
