import mongoose, { Schema } from 'mongoose';

const BonusSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  facebook: String,
  twitter: String,
  telegram: String
}, { timestamps: true });

export default mongoose.model('Bonus', BonusSchema)
