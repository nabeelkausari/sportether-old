import mongoose, { Schema } from 'mongoose';

const LogoSchema = new Schema({
  teamId: {
    type: Schema.Types.ObjectId,
    ref: 'Token'
  },
  img: {
    data: Buffer,
    contentType: String
  }
}, { versionKey: false })

export default mongoose.model('Logo', LogoSchema)
