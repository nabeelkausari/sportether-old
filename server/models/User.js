import mongoose, { Schema } from 'mongoose';
import { hashSync, compare } from 'bcrypt-nodejs';
import sequence from 'mongoose-sequence';
const AutoIncrement = sequence(mongoose);

const UserSchema = new Schema({
  firstName: String,
  lastName: String,
  avatar: String,
  password: String,
  username: { type: String, unique: true },
  email: { type: String, unique: true },
  isAdmin: { type: Boolean, default: false },
  confirmed: { type: Boolean, default: false },
  hadTransacted: { type: Boolean, default: false }, // will turn true once user buys token or participates in ground
  bonusClaimed: { type: Boolean, default: false },
  bonusApproved: { type: Boolean, default: false },
}, { timestamps: true });

UserSchema.plugin(AutoIncrement, {inc_field: 'walletIndex'});

UserSchema.pre('save', function(next) {
  if (this.isModified('password')) {
    this.password = this._hashPassword(this.password);
    return next();
  }
  return next();
})

UserSchema.methods = {
  _hashPassword(password) {
    return hashSync(password)
  },
  comparePassword (candidatePassword, callback) {
    compare(candidatePassword, this.password, function (err, isMatch) {
      if (err) return callback(err);

      return callback(null, isMatch);
    })
  }
}

export default mongoose.model('User', UserSchema);
