import mongoose, { Schema } from 'mongoose';

const TransactionSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  tokenId: {
    type: Schema.Types.ObjectId,
    ref: 'Token'
  },
  from: String,
  to: String,
  groundId: {
    type: Schema.Types.ObjectId,
    ref: 'Ground'
  },
  bonusFrom: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  type: { type: String, enum: [ 'deposit', 'withdrawal', 'buy', 'send', 'receive', 'bonus'], required: true },
  status: { type: String, enum: [ 'pending', 'success', 'cancelled', 'error'], default: 'pending' },
  tokenUnit: Number,
  tokenNewBalance: Number,
  ethUnit: Number,
  ethNewBalance: Number,
  txnId: String
}, { timestamps: true });

export default mongoose.model('Transaction', TransactionSchema)
