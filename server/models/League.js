import mongoose, { Schema } from 'mongoose';

const LeagueSchema = new Schema({
  name: String,
  symbol: String,
  slug: { type: String, unique: true }
});

export default mongoose.model('League', LeagueSchema)
