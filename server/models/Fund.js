import mongoose, { Schema } from 'mongoose';

const FundSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  tokenId: {
    type: Schema.Types.ObjectId,
    ref: 'Token'
  },
  balance: { type: Number, default: 0 }
}, { timestamps: true });

export default mongoose.model('Fund', FundSchema)
