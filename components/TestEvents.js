import React, { Component } from 'react';
import Web3 from 'web3';

let web3;
if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // We are in the browser and metamask is running
  web3 = new Web3(window.web3.currentProvider);
} else {
  // We are on the server OR user doesn't have metamask
  const provider = new Web3.providers.HttpProvider('https://rinkeby.infura.io/KXrbRI4GTIGmyauAdPpg');
  web3 = new Web3(provider);
}
web3.eth.defaultAccount = web3.eth.accounts[0];

export default class TestEvents extends Component {

  componentWillMount() {
    this.subscription = web3.eth.subscribe('logs',
      {
        address: '0x46E5EaD94d6cC5FD4F29984944e2d2799cb08114',
        topics: ['0x623b3804fa71d67900d064613da8f94b9617215ee90799290593e1745087ad18', '0x000000000000000000000000b073e88a046f8ad9c371c992e6d7bce342629991', '0x000000000000000000000000718da65c347c706d14cbc484a85c2535b640d956']
      }, function(error, result){
        if (!error)
          console.log(result);
      })
      .on("data", function(transaction){
        console.log(transaction);
      });
  }

  componentWillUnmount() {
    this.subscription.unsubscribe(function(error, success){
      if(success)
        console.log('Successfully unsubscribed!');
    });
  }

  watch = () => {

    let saleContract = new web3.eth.Contract([
      {
        "constant": true,
        "inputs": [],
        "name": "weiRaised",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "wallet",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "token",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "rate",
        "outputs": [
          {
            "name": "",
            "type": "uint256"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
          {
            "name": "",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
      },
      {
        "anonymous": false,
        "inputs": [
          {
            "indexed": true,
            "name": "purchaser",
            "type": "address"
          },
          {
            "indexed": true,
            "name": "beneficiary",
            "type": "address"
          },
          {
            "indexed": false,
            "name": "value",
            "type": "uint256"
          },
          {
            "indexed": false,
            "name": "amount",
            "type": "uint256"
          }
        ],
        "name": "TokenPurchase",
        "type": "event"
      },
      {
        "constant": false,
        "inputs": [],
        "name": "buyTokens",
        "outputs": [],
        "payable": true,
        "stateMutability": "payable",
        "type": "function"
      },
      {
        "inputs": [
          {
            "name": "_wallet",
            "type": "address"
          },
          {
            "name": "_rate",
            "type": "uint256"
          },
          {
            "name": "_token",
            "type": "address"
          }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "constructor"
      },
      {
        "payable": true,
        "stateMutability": "payable",
        "type": "fallback"
      }
    ], "0x46E5EaD94d6cC5FD4F29984944e2d2799cb08114");
    saleContract.events.TokenPurchase({},
      { fromBlock: 0, toBlock: 'latest' },
      function(error, event){ console.log(event); })
      .on('data', function(event){
        console.log(event);
      })
      .on('changed', function(event){
        console.log('on changed', event);
      })
      .on('error', console.error);
    // saleEvent.watch((err, result) => {
    //   console.log('event err: ', err)
    //   console.log('event result: ', result)
    // });
  }

  render() {
    return <div>TestEvents {this.watch()}</div>
  }
}
