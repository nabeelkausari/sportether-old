import React from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from '../server/routes';

export default () => <Menu style={{ marginTop: 10 }}>
  <Link route="/">
    <a className="item">SportEther</a>
  </Link>
  <Menu.Menu position="right">
    <Link route="/">
      <a className="item">Team Tokens</a>
    </Link>
    {/*<Link route="/campaigns/new">*/}
      {/*<a className="item">+</a>*/}
    {/*</Link>*/}
  </Menu.Menu>
</Menu>
