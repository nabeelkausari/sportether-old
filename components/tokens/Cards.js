import React from 'react';
import { Card } from 'semantic-ui-react';
import BuyTokens from './BuyTokens';

export default tokens => <Card.Group>
  { tokens.map(({ name, symbol, balance, teamSale }) => <Card key={symbol}>
    <Card.Content>
      <Card.Header>
        {symbol}
      </Card.Header>
      <Card.Meta>
        {name}
      </Card.Meta>
      <Card.Description>
        Available Tokens <strong>{balance}</strong>
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      {teamSale && <div>
        <BuyTokens symbol={symbol} address={teamSale.testNet} />
      </div>}
    </Card.Content>
  </Card>) }
</Card.Group>
