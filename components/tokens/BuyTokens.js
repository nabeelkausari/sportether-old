import React, { Component } from 'react';
import { Button, Modal, Icon, Input } from 'semantic-ui-react';

class BuyTokens extends Component {
  state = {
    open: false,
    rate: 0.002,
    noOfTokens: 10
  }

  show = () => this.setState({ open: true })
  close = () => this.setState({ open: false, noOfTokens: 10 })
  tokensHandler = e => {
    let noOfTokens = e.target.value;
    this.setState({ noOfTokens: noOfTokens >= 0 ? noOfTokens : 0 })
  }

  render() {
    const { symbol, address } = this.props;
    const { rate, noOfTokens } = this.state;
    return (
      <div>
        <Button onClick={this.show}>Buy {symbol}</Button>
        <Modal size='small' open={this.state.open} onClose={this.close}>
          <Modal.Header>
            Buy {symbol}
          </Modal.Header>
          <Modal.Content>
            <Input type="number" value={noOfTokens} onChange={this.tokensHandler} />
            <p>Send {noOfTokens * rate} ethers to this address</p>
            <pre>{address}</pre>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.close} color='red' inverted>
              <Icon name='remove' /> Cancel
            </Button>
            <Button onClick={this.close} color='green' inverted>
              <Icon name='checkmark' /> Done
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    )
  }
}

export default BuyTokens;
