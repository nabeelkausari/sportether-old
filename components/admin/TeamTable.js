import React from 'react'
import { Icon, Menu, Table } from 'semantic-ui-react'

import LogoUpload from './LogoUpload';

const TeamTable = ({ headers, rows, showFooter, editButton }) => (
  <Table celled compact size='small'>
    <Table.Header>
      <Table.Row>
        {headers.map(header => <Table.HeaderCell key={header}>{header}</Table.HeaderCell>)}
        <Table.HeaderCell />
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {rows.map(row => (
        <Table.Row key={row[0]}>
          <Table.Cell>
            <LogoUpload img={row[2]} teamName={row[1][0]} teamId={row[0]} />
          </Table.Cell>
          {row[1].map((cell, c) => <Table.Cell key={c}>{cell}</Table.Cell>)}
          <Table.Cell>
            {editButton(row[0])}
          </Table.Cell>
        </Table.Row>
      ))}
    </Table.Body>

    {showFooter && <Table.Footer>
      <Table.Row>
        <Table.HeaderCell colSpan='3'>
          <Menu floated='right' pagination>
            <Menu.Item as='a' icon>
              <Icon name='chevron left' />
            </Menu.Item>
            <Menu.Item as='a'>1</Menu.Item>
            <Menu.Item as='a'>2</Menu.Item>
            <Menu.Item as='a'>3</Menu.Item>
            <Menu.Item as='a'>4</Menu.Item>
            <Menu.Item as='a' icon>
              <Icon name='chevron right' />
            </Menu.Item>
          </Menu>
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>}
  </Table>
)

export default TeamTable
