import React, { Component } from 'react';
import axios from 'axios';

export default class LogoUpload extends Component {
  state = { img: {}, imgUrl: '', showEdit: false }

  componentWillMount() {
    this.setState({ imgUrl: this.props.img })
  }

  handleLogo = e => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        img: {
          data: reader.result,
          contentType: file.type
        }
      });
    }

    reader.readAsDataURL(file)

  }

  handleLogoUpload = async e => {
    e.preventDefault();
    let { img } = this.state;
    let { teamId } = this.props;
    let newLogo = await axios.post('/api/logos', { img, teamId });
    this.setState({ imgUrl: newLogo.data.logo})
  }

  toggleEdit = () => this.setState({ showEdit: !this.state.showEdit })

  render() {
    let { teamName } = this.props;
    let { imgUrl, showEdit } = this.state;
    let src = imgUrl ? `/api/logos/${imgUrl}` : 'http://www.hncompass.com/resources/img/portfolio/photo.png';
    let alt = imgUrl ? `${teamName} Logo` : 'Upload Logo';
    return (
      <div style={{ display: "flex", alignItems: "center", flexDirection: "row" }}>
        <img onClick={this.toggleEdit} style={{ cursor: "pointer" }} height="70" src={src} alt={alt}/>
        {showEdit && <div style={{ marginLeft: 10 }}>
          <input type="file" onChange={this.handleLogo} />
          <button onClick={this.handleLogoUpload}>Upload</button>
        </div>}
      </div>
    )
  }
}
