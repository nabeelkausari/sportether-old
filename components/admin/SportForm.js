import React, { Component } from 'react';
import { Form } from 'semantic-ui-react'
import axios from 'axios';

import {Router} from '../../server/routes';

export default class SportForm extends Component {
  state = {
    name: '',
    fanFollowing: '',
    tokenSupply: '',
    description: ''
  }

  componentWillMount() {
    if (this.props.sport) {
      let { name, fanFollowing, tokenSupply, description } = this.props.sport;
      this.setState({ name, fanFollowing, tokenSupply, description });
    }
  }

  handleSubmit = async e => {
    e.preventDefault();
    if (this.props.sport) {
      await axios.put(`/api/sport/${this.props.sport._id}`, {...this.state})
    } else {
      await axios.post('/api/sports', {...this.state})
    }
    Router.pushRoute('/admin/sports/list')
  }

  handleName = e => this.setState({name: e.target.value});
  handleFanCount = e => this.setState({fanFollowing: e.target.value});
  handleTokenSupply = e => this.setState({tokenSupply: e.target.value});
  handleDescription = e => this.setState({description: e.target.value});

  render() {
    let { name, fanFollowing, tokenSupply, description } = this.state;
    return <Form onSubmit={this.handleSubmit}>
      <Form.Group widths='equal'>
        <Form.Input
          onChange={this.handleName}
          value={name}
          fluid label='Sport name'
          placeholder='Sport name'
        />
        <Form.Input
          onChange={this.handleFanCount}
          value={fanFollowing}
          fluid label='Fan following count'
          placeholder='Fan following count'
        />
        <Form.Input
          onChange={this.handleTokenSupply}
          value={tokenSupply}
          fluid label='Token supply'
          placeholder='Token supply'
        />
      </Form.Group>
      <Form.TextArea
        onChange={this.handleDescription}
        value={description}
        label='Description'
        placeholder='Write more about this sport...'
      />
      <Form.Button secondary>Submit</Form.Button>
    </Form>
  }
}
