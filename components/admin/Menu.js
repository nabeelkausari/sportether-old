import React from 'react';
import { Container, Divider, Dropdown, Grid, Header, Image, List, Menu, Segment } from 'semantic-ui-react'

import { Link } from '../../server/routes';

const link = (route, label, color) => <Link route={route}><a style={{ color: color || '#000000de' }}>{label}</a></Link>

export default () => <Menu fixed='top' inverted>
  <Container>
    <Menu.Item header>
      {link('/admin', 'SportEther Admin', '#ffffff')}
    </Menu.Item>

    <Dropdown item simple text='Sport'>
      <Dropdown.Menu>
        <Dropdown.Item>{link('/admin/sports/list', 'List sports')}</Dropdown.Item>
        <Dropdown.Item>{link('/admin/sports/new', 'Add New Sport')}</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
    <Dropdown item simple text='Teams'>
      <Dropdown.Menu>
        <Dropdown.Item>
          <i className='dropdown icon' />
          <span className='text'>List teams</span>
          <Dropdown.Menu>
            <Dropdown.Item>{link('/admin/teams/5aa666461a04c64fb318e9a2', 'Football')}</Dropdown.Item>
            <Dropdown.Item>{link('/admin/teams/5aa6661a1a04c64fb318e9a1', 'Cricket')}</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown.Item>
        <Dropdown.Item>{link('/admin/teams/new', 'Add New Team')}</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
    <Dropdown item simple text='Matches'>
      <Dropdown.Menu>
        <Dropdown.Item disabled>List Matches</Dropdown.Item>
        <Dropdown.Item disabled>Add New Match</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
    <Dropdown item simple text='Grounds'>
      <Dropdown.Menu>
        <Dropdown.Item disabled>List Active Grounds</Dropdown.Item>
        <Dropdown.Item disabled>Add New Ground</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
    <Dropdown item simple text='Users'>
      <Dropdown.Menu>
        <Dropdown.Item disabled>List Users</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  </Container>
</Menu>
