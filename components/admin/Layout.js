import React from 'react'
import Head from 'next/head';
import { Container } from 'semantic-ui-react'

import Menu from './Menu';

const Layout = props => (
  <div>
    <Head>
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.12/semantic.min.css"></link>
    </Head>
    <Menu />
    <Container text style={{ marginTop: '7em' }}>
      {props.children}
    </Container>
  </div>
)

export default Layout
