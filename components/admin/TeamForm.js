import React, { Component } from 'react';
import { Form } from 'semantic-ui-react'
import axios from 'axios';

import {Router} from '../../server/routes';

export default class TeamForm extends Component {
  state = {
    name: '',
    symbol: '',
    balance: '',
    description: '',
    address: {},
    teamSale: {},
    sportId: '',
    sports: [],
    loading: false
  }

  async componentWillMount() {
    const sports = await axios.get('/api/sports');
    this.setState({ sports: sports.data });

    if (this.props.team) {
      let { name, symbol, logo, description, balance, address, teamSale, sportId } = this.props.team;
      this.setState({ name, symbol, logo, description, balance, address, teamSale, sportId });
    }
  }

  handleSubmit = async e => {
    e.preventDefault();
    this.setState({ loading: true })
    let { name, symbol, logo, description, sportId, teamSale, address } = this.state;
    // Prevent admin to modify once it is published to mainnet
    if (teamSale && teamSale.mainNet || address && address.mainNet) return

    if (this.props.team) {
      await axios.put(`/api/team/${this.props.team._id}`, {name, symbol, logo, description, sportId})
    } else {
      await axios.post('/api/teams', {name, symbol, logo, description, sportId})
    }

    if (sportId) Router.pushRoute(`/admin/teams/${sportId}`)
    else Router.pushRoute('/admin')

    this.setState({ loading: false })
  }

  handleName = e => this.setState({name: e.target.value});
  handleSymbol = e => this.setState({symbol: e.target.value});
  handleDescription = e => this.setState({description: e.target.value});
  handleSport = e => this.setState({sportId: e.target.value });

  render() {
    let { name, symbol, description, sportId, sports, loading } = this.state;
    return <Form loading={loading} onSubmit={this.handleSubmit}>
      <Form.Group widths='equal'>
        <Form.Input
          onChange={this.handleName}
          value={name}
          fluid label='Team name'
          placeholder='Team name'
        />
        <Form.Input
          onChange={this.handleSymbol}
          value={symbol}
          fluid label='Team symbol'
          placeholder='Team symbol'
        />
        <Form.Field style={{height: 37}} label='Select Sport' value={sportId} onChange={this.handleSport} control='select'>
          {sports.map(sport => <option
            key={sport._id}
            value={sport._id}
          >{sport.name}</option>)}
        </Form.Field>
      </Form.Group>

      <Form.TextArea
        onChange={this.handleDescription}
        value={description}
        label='Description'
        placeholder='Write more about this team...'
      />

      <Form.Button secondary>Submit</Form.Button>
    </Form>
  }
}
