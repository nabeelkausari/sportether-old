const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledTeam = require('../ethereum/build/Team.json');
const assertRevert = require('./helpers/assertRevert');

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';
const tokenName = "RCB Token";
const tokenSymbol = "RCB";
const gas = '1000000';

let team;
let owner;
let recipient;
let anotherRecipient;

beforeEach(async () => {
  [owner, recipient, anotherRecipient] = await web3.eth.getAccounts();

  team = await new web3.eth.Contract(JSON.parse(compiledTeam.interface))
    .deploy({ data: compiledTeam.bytecode, arguments: [tokenName, tokenSymbol]})
    .send({ from: owner, gas: '1000000'});
});

describe('after deployment', () => {
  it('has deployed a token', () => {
    assert.ok(team.options.address);
  });

  it('has a name', async function () {
    const name = await team.methods.name().call();
    assert.equal(name, tokenName)
  });

  it('has a symbol', async function () {
    const symbol = await team.methods.symbol().call();
    assert.equal(symbol, tokenSymbol)
  });

  it('has an amount of decimals', async function () {
    const decimals = await team.methods.decimals().call();
    assert.equal(decimals, 0)
  });

  it('assigns the balance to the token creator', async () => {
    assert.equal(await team.methods.balanceOf(owner).call(), 1000);
  });
});

describe('total supply', function () {
  it('returns the total amount of tokens', async function () {
    const totalSupply = await team.methods.totalSupply().call();

    assert.equal(totalSupply, 1000);
  });
});

describe('balanceOf', function () {
  describe('when the requested account has no tokens', function () {
    it('returns zero', async function () {
      const balance = await team.methods.balanceOf(recipient).call();

      assert.equal(balance, 0);
    });
  });

  describe('when the requested account has some tokens', function () {
    it('returns the total amount of tokens', async function () {
      const balance = await team.methods.balanceOf(owner).call();

      assert.equal(balance, 1000);
    });
  });
});

describe('transfer', function () {
  describe('when the recipient is not the zero address', function () {

    describe('when the sender does not have enough balance', function () {
      it('reverts', async function () {
        await assertRevert(team.methods.transfer(recipient, 1001).send({ from: owner, gas }));
      });
    });

    describe('when the sender has enough balance', function () {
      const amount = 1000;

      it('transfers the requested amount', async function () {
        await team.methods.transfer(recipient, amount).send({ from: owner, gas });

        assert.equal(await team.methods.balanceOf(owner).call(), 0);
        assert.equal(await team.methods.balanceOf(recipient).call(), amount);

        await team.methods.transfer(anotherRecipient, amount).send({ from: recipient, gas });

        assert.equal(await team.methods.balanceOf(recipient).call(), 0);
        assert.equal(await team.methods.balanceOf(anotherRecipient).call(), amount);
      });

      it('transfers the whole token (1) not the decimals (1.5)', async () => {
        await team.methods.transfer(recipient, 1.5).send({ from: owner, gas });

        assert.equal(await team.methods.balanceOf(owner).call(), 999);
        assert.equal(await team.methods.balanceOf(recipient).call(), 1);
      });

      it('emits a transfer event', async function () {
        const { events: { Transfer }} = await team.methods.transfer(recipient, amount).send({ from: owner, gas });

        assert.equal(Transfer.event, 'Transfer');
        assert.equal(Transfer.returnValues.from, owner);
        assert.equal(Transfer.returnValues.to, recipient);
        assert.equal(Transfer.returnValues.value, amount);
      });
    });
  });

  describe('when the recipient is the zero address', function () {
    it('reverts', async function () {
      await assertRevert(team.methods.transfer(ZERO_ADDRESS, 1000).send({ from: owner, gas }));
    });
  });
});
