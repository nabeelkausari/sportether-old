import React, {Component} from 'react';
import { Button } from 'semantic-ui-react'
import Error from 'next/error'
import fetch from 'isomorphic-fetch';

import Layout from "../../../components/admin/Layout";
import Table from "../../../components/admin/Table";
import { Router } from '../../../server/routes';

export default class ListSports extends Component {
  static async getInitialProps({ req }) {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
    let res = await fetch(baseUrl + '/api/sports');
    const statusCode = res.statusCode > 200 ? res.statusCode : false
    const sports = await res.json();
    return { statusCode, sports }
  }

  render() {
    if(this.props.statusCode) {
      return <Error statusCode={this.props.statusCode} />
    }

    let headers = ['Name', 'Fan following', 'Token supply', 'Description'];
    return <Layout>
      <h1>List of available sports</h1>
      {Table({ headers, rows: getRows(this.props.sports), editButton })}
    </Layout>
  }
}

const getRows = data => {
  let output = [];
  for (let item of data) {
    let row = [];
    row.push([item._id]);
    row.push([item.name, item.fanFollowing, item.tokenSupply, item.description]);
    output.push(row);
  }
  return output;
}

const editButton = sportId => {
  const handleClick = () => Router.pushRoute('/admin/sports/edit/'+ sportId);
  return <Button secondary onClick={handleClick}>Edit</Button>
}
