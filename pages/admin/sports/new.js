import React from 'react';

import Layout from "../../../components/admin/Layout";
import SportForm from "../../../components/admin/SportForm";

export default () => <Layout>
  <h1>Add a new sport</h1>
  <SportForm />
</Layout>
