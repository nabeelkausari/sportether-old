import React, {Component} from 'react';
import Error from 'next/error'
import fetch from 'isomorphic-fetch';

import Layout from "../../../components/admin/Layout";
import SportForm from "../../../components/admin/SportForm";

export default class EditSport extends Component {
  static async getInitialProps({ req, query }) {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
    let res = await fetch(baseUrl + '/api/sport/' + query.sportId);
    const statusCode = res.statusCode > 200 ? res.statusCode : false
    const sport = await res.json();
    return { statusCode, sport }
  }

  render() {
    if(this.props.statusCode) {
      return <Error statusCode={this.props.statusCode} />
    }

    return <Layout>
      <h1>Edit sport</h1>
      <SportForm sport={this.props.sport} />
    </Layout>
  }
}
