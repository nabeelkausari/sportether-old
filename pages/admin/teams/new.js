import React from 'react';

import Layout from "../../../components/admin/Layout";
import TeamForm from "../../../components/admin/TeamForm";

export default () => <Layout>
  <h1>Add a new team</h1>
  <TeamForm />
</Layout>
