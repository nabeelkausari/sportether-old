import React, {Component} from 'react';
import Error from 'next/error'
import fetch from 'isomorphic-fetch';

import Layout from "../../../components/admin/Layout";
import TeamForm from "../../../components/admin/TeamForm";

export default class EditTeam extends Component {
  static async getInitialProps({ req, query }) {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';

    let res = await fetch(baseUrl + '/api/team/' + query.teamId);
    const statusCode = res.statusCode > 200 ? res.statusCode : false
    const team = await res.json();
    return { statusCode, team }
  }

  render() {
    if(this.props.statusCode) {
      return <Error statusCode={this.props.statusCode} />
    }

    return <Layout>
      <h1>Edit team</h1>
      <TeamForm team={this.props.team} />
    </Layout>
  }
}
