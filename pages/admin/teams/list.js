import React, {Component} from 'react';
import { Button } from 'semantic-ui-react'
import Error from 'next/error'
import fetch from 'isomorphic-fetch';

import Layout from "../../../components/admin/Layout";
import TeamTable from "../../../components/admin/TeamTable";
import { Router } from '../../../server/routes';

export default class ListTeams extends Component {
  static async getInitialProps({ req, query }) {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
    let res = await fetch(baseUrl + '/api/teams/' + query.sportFilterId);
    const statusCode = res.statusCode > 200 ? res.statusCode : false
    const teams = await res.json();
    return { statusCode, teams }
  }


  render() {
    if(this.props.statusCode) {
      return <Error statusCode={this.props.statusCode} />
    }
    if (!this.props.teams.length) {
      return <Layout>
        <h1>No teams yet</h1>
        <Button onClick={() => Router.pushRoute('/admin/teams/new')} secondary>Create One</Button>
      </Layout>
    }
    let headers = ['Logo', 'Name', 'Symbol', 'Balance'];
    return <Layout>
      <h1>List of available { this.props.teams[0].sportId.name } teams</h1>
      {TeamTable({ headers, rows: getRows(this.props.teams), editButton })}
    </Layout>
  }
}

const getRows = data => {
  let output = [];
  for (let item of data) {
    let row = [];
    row.push(item._id);
    row.push([item.name, item.symbol, item.balance]);
    row.push(item.logo);
    output.push(row);
  }
  return output;
}

const editButton = teamId => {
  const handleClick = () => Router.pushRoute('/admin/teams/edit/'+ teamId);
  return <div>
    <Button secondary onClick={handleClick}>Edit</Button>
    <Button primary >Deploy</Button>
  </div>
}
