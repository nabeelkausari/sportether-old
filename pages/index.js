import React, { Component } from 'react';
import axios from 'axios';

import Layout from '../components/Layout';
import Cards from '../components/tokens/Cards';
import { Link } from '../server/routes';


export default class SportEther extends Component {
  static async getInitialProps({ req }) {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
    const { data } = await axios.get(baseUrl + '/api/deployedTokens');
    return { tokens: data };
  }

  state = { loading: false }

  async handleTransfer (tokenAddress) {
    this.setState({ loading: true })
    let transfer = await axios.post('/api/transferTokens', {tokenAddress});
    this.setState({ loading: false })
    console.log('transfer: ', transfer);
  }

  render() {
    return <Layout>
      {this.state.loading && <h2>Loading.....</h2>}
      <h3>Available Team Tokens</h3>
      { Cards(this.props.tokens) }
    </Layout>
  }
}
